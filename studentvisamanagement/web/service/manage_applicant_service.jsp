<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR DATA TABLE -->
        <link href="<%= request.getContextPath()%>/css/dataTables.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            .input-inline {
                display: inline-block;
                width: auto;
                vertical-align: middle;
            }

            .blue.btn {
                color: #fff;
                background-color: #4b8df8;
                color: #FFF;
            }

            .btn {
                border-width: 0;
                padding: 7px 14px;
                font-size: 12px;
                outline: none;
                background-image: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                text-shadow: none;
            }

            .yellow.btn {
                color: #fff;
                background-color: #ffb848;
            }

            .red.btn {
                color: #fff;
                background-color: #d84a38;
            }

            /*            
            .table-scrollable {
                width: 100%;
                max-height: 630px;
                overflow-x: auto;
                overflow-y: auto;
                margin: 10px 0 !important;
             }
            */

            /*            .
            margin-bottom-5, .btn-editable {
                margin-bottom: 5px;
            }
            */

        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Applicant Service Allocate History</h3>
                                </div> 

                                <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="12%">Applicant Name</th>
                                                <th width="12%">Agent Name</th>
                                                <th width="11%">Tuition Basic</th>
                                                <th width="9%">Tuition Agree</th>
                                                <th width="10%">Others Basic</th>
                                                <th width="11%">Others Agree</th>
                                                <th width="9%">Actions</th>
                                            </tr>
                                            <tr style="text-align: center;" class="filter">
                                                <td><input type="text" id="applicantName" name="applicantName" class="form-control form-filter input-sm"></td>
                                                <td><input type="text" id="agentName" name="agentName" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="width: 90px;">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom" data-toggle="tooltip" data-placement="top" title="Search Button"><i class="icon-magnifier"></i></button>
                                                    <button class="btn btn-sm red filter-cancel" data-toggle="tooltip" data-placement="top" title="Reset Button"><i class="icon-close"></i></button>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_app_service">
                                            <s:if test="applicantServiceInfoList !=null">
                                                <s:if test="applicantServiceInfoList.size() !=0">
                                                    <s:iterator value="applicantServiceInfoList">
                                                        <tr style="text-align: center;">
                                                            <td align="center"><s:property value="applicantName"/></td>
                                                            <td align="center"><s:property value="userInfo.userFullName"/></td>
                                                            <td align="center"><s:property value="asi.tutionBasic"/></td>
                                                            <td align="center"><s:property value="asi.tutionAgree"/></td>
                                                            <td align="center"><s:property value="asi.othersBasic"/></td>
                                                            <td align="center"><s:property value="asi.othersAgree"/></td>
                                                            <td>
                                                                <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View Button">
                                                                    <i class="icon-pencil"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="5" style="text-align: right;">Total</th>
                                                <th colspan="2" style="text-align: left;"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- Modal -->
            <div id="service-modal" class="modal fade" tabindex="-1">

            </div>

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- JQUERY FOR DATA TABLE -->
        <script src="<%= request.getContextPath()%>/js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/dataTables.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-modal.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip();
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(5)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(5, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(5).footer()).html(
                                '' + pageTotal + ' (Grand Total : ' + total + ')'
                                );
                    }
                });

                $('.btn-view').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    $('body').modalmanager('loading');
                    setTimeout(function () {
                        $('#service-modal').load('ViewApplicantService', {applicantID: id}, function () {
                            $('#service-modal').modal();
                        });
                    }, 2000, id);
                });

                var fName = '';
                var fValue = '';
                var tbody = $("#tbody_app_service");
                var search = $(".filter-submit");
                search.on('click', function (e) {
                    e.preventDefault();
                    if ($("#applicantName").val() != '') {
                        fName = $("#applicantName").attr("name");
                        fValue = $("#applicantName").val();
                    } else if ($("#agentName").val() != '') {
                        fName = $("#agentName").attr("name");
                        fValue = $("#agentName").val();
                    }
                    var dataString = 'fieldName=' + fName;
                    dataString += '&fieldValue=' + fValue;
                    $.post('SearchAppService', dataString).done(function (data) {
                        tbody.html(data);
                    });
                });

                $('.filter-cancel').on('click', function (e) {
                    e.preventDefault();
                    $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                        $(this).val("");
                    });
                    setTimeout(location.reload(), 0);
                });
            });
        </script>
    </body>
</html>
