<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="applicantServiceInfoList !=null">
    <s:if test="applicantServiceInfoList.size() !=0">
        <s:iterator value="applicantServiceInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Applicant ID <s:property value="applicantID"/></h4>
            </div>
            <form id="form" action="" method="">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN ALERTS PORTLET-->
                            <div class="portlet green box">
                                <div class="portlet-body">
                                    <h4>Details of <s:property value="applicantName"/></h4>
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Applicant Name</td>
                                                    <td><s:property value="applicantName"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Agent Name</td>
                                                    <td><s:property value="userInfo.userFullName"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Tuition Basic</td>
                                                    <td><s:property value="asi.tutionBasic"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Tuition Agree</td>
                                                    <td><input type="text" id="userFax" name="userFax" value="<s:property value="asi.tutionAgree"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Others Basic</td>
                                                    <td><s:property value="asi.othersBasic"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Others Agree</td>
                                                    <td><input type="text" id="userFax" name="userFax" value="<s:property value="asi.othersAgree"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Tuition Status</td>
                                                    <td>
                                                        <select id="tuitionStatus" name="tuitionStatus" class="">
                                                            <s:if test="asi.tuitionStatus=='Y'">
                                                                <option selected value="Y">Active</option>
                                                                <option value="N">Inactive</option>
                                                            </s:if>
                                                            <s:else>
                                                                <option value="Y">Active</option>
                                                                <option selected value="N">Inactive</option>
                                                            </s:else>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Others Status</td>
                                                    <td>
                                                        <select id="othersStatus" name="othersStatus" class="">
                                                            <s:if test="asi.othersStatus=='Y'">
                                                                <option selected value="Y">Active</option>
                                                                <option value="N">Inactive</option>
                                                            </s:if>
                                                            <s:else>
                                                                <option value="Y">Active</option>
                                                                <option selected value="N">Inactive</option>
                                                            </s:else>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
<!--                                            <tfoot>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <button style="font-size: 14px;" type="reset" class="btn red">Reset</button>
                                                        <button style="margin-right: 5px; font-size: 14px;" type="submit" class="btn blue">Save Change</button>
                                                    </td>
                                                </tr>
                                            </tfoot>-->
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END ALERTS PORTLET-->
                        </div>
                    </div>
                </div>
            </form>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>
