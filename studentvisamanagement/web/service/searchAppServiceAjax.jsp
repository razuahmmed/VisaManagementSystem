<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="applicantServiceInfoList !=null">
    <s:if test="applicantServiceInfoList.size() !=0">
        <s:iterator value="applicantServiceInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="applicantName"/></td>
                <td align="center"><s:property value="userInfo.userFullName"/></td>
                <td align="center"><s:property value="asi.tutionBasic"/></td>
                <td align="center"><s:property value="asi.tutionAgree"/></td>
                <td align="center"><s:property value="asi.othersBasic"/></td>
                <td align="center"><s:property value="asi.othersAgree"/></td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View Button">
                        <i class="icon-pencil"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>