<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="curAppName !=null">
    <s:iterator value="curAppName">
        <div class="form-group">
            <label class="col-sm-2 control-label">Applicant Name</label>
            <div class="col-sm-3">
                <input type="text" disabled value="<s:property value="curAppName"/>" class="form-control">
            </div>
        </div>
    </s:iterator>
</s:if>
