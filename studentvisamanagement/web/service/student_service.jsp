<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            table th, td {
                padding: 5px;
                margin: 0px;
                border: #eee 1px solid;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Applicant Service Allocate</h3>
                                </div> 
                                <form action="AllocateApplicantService" method="POST" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="demo-is-inputsmall">Agent *</label>
                                            <div class="col-sm-3"> 
                                                <select id="agentId" name="agentId" data-placeholder="Select Agent" required class="form-control drop-select">
                                                    <option selected value=""></option>
                                                    <s:if test="agentInfoList !=null">
                                                        <s:if test="agentInfoList.size() !=0">
                                                            <s:iterator value="agentInfoList">
                                                                <option value="<s:property value="userID"/>"><s:property value="userName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Applicant ID *</label>
                                            <div class="col-sm-3">
                                                <select id="applicantID" name="applicantID" required data-placeholder="Select Applicant ID" class="form-control drop-select">
                                                    <option selected value=""></option>

                                                </select>
                                                <img id="createLoadingImage" src="" alt="" />
                                            </div>

                                            <div class="" id="applicant-name">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-7 control-label">Select Service *</label><br><br>
                                            <div id="" class="col-sm-7">  
                                                <table style="width: 100%">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">Service Name</th>
                                                            <th scope="col">Amount</th>
                                                            <th scope="col">Currency</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="">
                                                        <s:if test="serviceInfoList !=null">
                                                            <s:if test="serviceInfoList.size() !=0">
                                                                <s:iterator value="serviceInfoList">
                                                                    <tr>
                                                                        <td>
                                                                            <input type="checkbox" id="serviceID" name="serviceID" class="chkbCSName" value="<s:property value="serviceID"/>">
                                                                            <label style="margin-left: 10px;" for=""><s:property value="serviceName"/></label>
                                                                        </td>
                                                                        <td>
                                                                            <input type="hidden" id="basicAmount" name="basicAmount" value="<s:property value="serviceAmount"/>"/>
                                                                            <input type="text" id="agreeAmount" pattern="[0-9]{1,10}" name="agreeAmount" value="<s:property value="serviceAmount"/>" class="amount"/>
                                                                        </td>
                                                                        <td>MYR</td>
                                                                    </tr>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td style="text-align: right;"><label>Total</label></td>
                                                            <td style="text-align: left;" colspan="2"><label><input type="hidden" id="agreeAmountTotal" name="agreeAmountTotal"/><div id="total">0</div></label></td>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.chkbCSName').change(function () {
                    var total = 0;
                    $('.chkbCSName:checked').each(function () {
                        total += parseFloat($(this).parent().next('td').find('.amount').val());
                    });
                    $('#total').text(total);
                    $('#agreeAmountTotal').val(total);

                    if ($(this).is(':checked')) {
                        var row = $(this).closest('tr');
                        var inputs = $('.amount', row);
                        inputs.attr('readonly', true);
                    } else {
                        var row1 = $(this).closest('tr');
                        var inputs = $('.amount', row1);
                        inputs.attr('readonly', false);
                    }
                });

                $('#agentId').select2().on("change", function (e) {
                    getApplicant();
                });

                $('#applicantID').select2().on("change", function (e) {
                    currentAppName();
                });
            });

            function  currentAppName() {

                var appid = $('#applicantID').val();

                if (appid !== "") {
                    document.getElementById('createLoadingImage').src = 'images/load.gif';

                    $.post("CurrentAppName", {applicantID: appid}, function (data) {
                        document.getElementById('createLoadingImage').src = '';
                        $("#applicant-name").html(data);
                    });
                }
            }

            function  getApplicant() {
                var agid = $('#agentId').val();
                if (agid !== "") {
                    $.post("GetApplicantByAgent", {agentId: agid}, function (data) {
                        $("#applicantID").html(data);
                    });
                }
            }
        </script>
    </body>
</html>
