<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <!-- THIS CSS FOR DATA TABLE -->
        <link href="<%= request.getContextPath()%>/css/dataTables.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <link href="<%= request.getContextPath()%>/css/jquery-confirm.css" rel="stylesheet" type="text/css">

        <style type="text/css">

            .input-inline {
                display: inline-block;
                width: auto;
                vertical-align: middle;
            }

            .blue.btn {
                color: #fff;
                background-color: #4b8df8;
                color: #FFF;
            }

            .btn {
                border-width: 0;
                padding: 7px 14px;
                font-size: 12px;
                outline: none;
                background-image: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                text-shadow: none;
            }

            .yellow.btn {
                color: #fff;
                background-color: #ffb848;
            }

            .red.btn {
                color: #fff;
                background-color: #d84a38;
            }

            /*            
            .table-scrollable {
                width: 100%;
                max-height: 630px;
                overflow-x: auto;
                overflow-y: auto;
                margin: 10px 0 !important;
            }
            */

            /*            .
            margin-bottom-5, .btn-editable {
                margin-bottom: 5px;
            }
            */

        </style>

        <%
            String gid = (request.getSession().getAttribute("GROUP_ID") != null ? request.getSession().getAttribute("GROUP_ID").toString() : "");
        %>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Worker History</h3>
                                </div> 

                                <div id="message">

                                </div>

                                <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover table-responsive" id="datatable_orders">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="14%">Full Name</th>
                                                <th width="12%">User Name</th>
                                                <th width="8%">Phone</th>
                                                <th width="8%">Fax</th>
                                                <th width="10%">Email</th>
                                                <th width="9%">Country</th>
                                                <th width="9%">Company</th>
                                                <th width="10%">User Type</th>
                                                <th width="12%">Status</th>
                                                <th width="8%">Actions</th>
                                            </tr>
                                            <tr style="text-align: center;" class="filter">
                                                <td><input type="text" id="userFulName" name="userFulName" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td><input type="text" id="userPhone" name="userPhone" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><input type="text" id="userCompany" name="userCompany" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td></td>
                                                <td style="width: 100px;">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom" data-toggle="tooltip" data-placement="top" title="Search Button"><i class="icon-magnifier"></i></button>
                                                    <button class="btn btn-sm red filter-cancel" data-toggle="tooltip" data-placement="top" title="Reset Button"><i class="icon-close"></i></button>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_user_history">
                                            <s:if test="agentInfoList !=null">
                                                <s:if test="agentInfoList.size() !=0">
                                                    <s:iterator value="agentInfoList">
                                                        <tr style="text-align: center;">
                                                            <td align="center"><s:property value="userFullName"/></td>
                                                            <td><s:property value="userName"/></td>
                                                            <td><s:property value="userPhone"/></td>
                                                            <td><s:property value="userFax"/></td>
                                                            <td><s:property value="userEmail"/></td>
                                                            <td><s:property value="countryInfo.countryName"/></td>
                                                            <td><s:property value="company"/></td>
                                                            <td id="grname"><s:property value="userGroupInfo.groupName"/></td>
                                                            <td>
                                                                <s:if test="userStatus=='Y'">
                                                                    Active
                                                                </s:if>
                                                                <s:else>
                                                                    <div style="color: red;">
                                                                        Inactive
                                                                    </div>
                                                                </s:else>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" id="<s:property value="userID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                                                                    <i class="icon-pencil"></i>
                                                                </a>
                                                                <a href="javascript:void(0);" id="<s:property value="userID"/>" class="btn btn-xs purple btn-danger btn-pass-change" data-toggle="tooltip" data-placement="top" title="Change Password">
                                                                    <i class="icon-key"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- Modal -->
            <div id="user-modal" class="modal fade" tabindex="-1">

            </div>

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- JQUERY FOR DATA TABLE -->
        <script src="<%= request.getContextPath()%>/js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/dataTables.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-modal.js" type="text/javascript"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

        <script src="<%= request.getContextPath()%>/js/jquery-confirm.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js/user-search.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers"
                });

                $('.btn-view').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    $('body').modalmanager('loading');
                    setTimeout(function () {
                        $('#user-modal').load('ViewUserInfo', {userId: id}, function () {
                            $('#user-modal').modal();
                        });
                    }, 2000, id);
                });

                $(".btn-pass-change").on('click', function (e) {
                    e.preventDefault();
                    var userId = this.id;
                    $.confirm({
                        columnClass: 'col-md-5 col-md-offset-3',
                        title: 'Change Password',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        content: '' +
                                '<form action="" class="formName">' +
                                '<div class="form-group">' +
                                '<label>New Password</label>' +
                                '<input type="password" id="newPassword" name="newPassword" pattern="[a-zA-Z0-9]{3,15}" required placeholder="New Password" class="form-control">' +
                                '</div>' +
                                '<div class="form-group">' +
                                '<label>Retype NewPassword</label>' +
                                '<input type="password" id="retypeNewPassword" name="retypeNewPassword" pattern="[a-zA-Z0-9]{3,15}" required placeholder="Retype New Password" class="form-control">' +
                                '</div>' +
                                '</form>',
                        buttons: {
                            formSubmit: {
                                text: 'Submit',
                                btnClass: 'btn-blue',
                                action: function () {

                                    var newPassword = this.$content.find('#newPassword').val();
                                    var retypeNewPassword = this.$content.find('#retypeNewPassword').val();

                                    if (newPassword === '') {
                                        conf("New Password empty !");
                                        return false;
                                    } else if (retypeNewPassword === '') {
                                        conf("Retype Password empty !");
                                        return false;
                                    } else if (newPassword !== retypeNewPassword) {
                                        conf("New Password and Retype Password does not match !");
                                        return false;
                                    } else if (!length(newPassword, 3, 15, "New Password")) {
                                        return false;
                                    } else if (!format(newPassword, "Invalid password Format !")) {
                                        return false;
                                    } else {
                                        $.post("ChangePassByAdmin", {userId: userId, newPassword: newPassword}, function (data, status) {
                                            $('#message').html(data);
                                        });
                                    }
                                }
                            },
                            cancel: function () {
                                //close
                            }
                        }
                    });
                });

                $('.filter-cancel').on('click', function (e) {
                    e.preventDefault();
                    $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                        $(this).val("");
                    });
                    setTimeout(location.reload(), 0);
                });
            });

            function conf(msg) {
                $.confirm({
                    columnClass: 'col-md-5 col-md-offset-3',
                    title: 'Warning!',
                    content: '' + msg + '',
                    backgroundDismiss: true,
                    closeIcon: true,
                    typeAnimated: true,
                    buttons: {
                        tryAgain: {
                            text: 'Ok'
                        }
                    }
                });
            }

            function length(id, min, max, msg) {
                var uInput = id;
                if (uInput.length >= min && uInput.length <= max) {
                    return true;
                } else {
                    $.confirm({
                        columnClass: 'col-md-5 col-md-offset-3',
                        title: 'Warning!',
                        content: 'Please enter ' + msg + ' between ' + min + ' to ' + max + ' characters',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok'
                            }
                        }
                    });
                }
            }

            function format(id, msg) {
                var alphaExp = /^[0-9a-zA-Z]+$/;
                if (id.match(alphaExp)) {
                    return true;
                } else {
                    $.confirm({
                        columnClass: 'col-md-5 col-md-offset-3',
                        title: 'Warning!',
                        content: '' + msg + '',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Ok'
                            }
                        }
                    });
                }
            }
        </script>
    </body>
</html>
