<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <style type="text/css">
            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Perspective Applicant</h3>
                                </div>

                                <form action="AddNewWorker" method="POST" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Applicant Type *</label>
                                            <div class="col-sm-4">
                                                <select id="appType" name="appType" required class="form-control">
                                                    <option selected value="2">Worker</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="demo-is-inputsmall">Agent *</label>
                                            <div class="col-sm-4"> 
                                                <select id="agentId" name="agentId" data-placeholder="Select Agent" required class="form-control drop-select">
                                                    <option selected value=""></option>
                                                    <s:if test="agentInfoList !=null">
                                                        <s:if test="agentInfoList.size() !=0">
                                                            <s:iterator value="agentInfoList">
                                                                <option value="<s:property value="userID"/>"><s:property value="userFullName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Applicant Name *</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="fullName" name="fullName" pattern="[A-Za-z-0-9-_ ]{3,20}" minlength="3" maxlength="20" placeholder="Full Name" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="demo-is-inputlarge" class="col-sm-3 control-label">Nationality *</label>
                                            <div class="col-sm-4">
                                                <select id="nationality" name="nationality" required class="form-control">
                                                    <option selected value="">Select Country</option>
                                                    <s:if test="countryInfoList !=null">
                                                        <s:if test="countryInfoList.size() !=0">
                                                            <s:iterator value="countryInfoList">
                                                                <option value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><strong>Passport Number *</strong></label>
                                            <div class="col-sm-4">
                                                <input type="text" id="passportNumber" name="passportNumber" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Passport Number" class="form-control">
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <label for="demo-is-inputlarge" class="col-sm-3 control-label">Sector *</label>
                                            <div class="col-sm-4">
                                                <select id="sector" name="sector" required class="form-control">
                                                    <option selected value="">Select Sector</option>
                                                    <s:if test="countryInfoList !=null">
                                                        <s:if test="countryInfoList.size() !=0">
                                                            <s:iterator value="countryInfoList">
                                                                <option value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Date Of Birth *</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <input type="date" id="dob" name="dob" placeholder="YYYY-MM-DD" required class="form-control">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input type="submit" id="btn_submit" name="btn_submit" value="Submit" class="btn btn-primary"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var date_dob = $('input[name="dob"]');
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_dob.datepicker(options);

                $('#agentId').select2().on("change", function (e) {
                });
            });
        </script>

    </body>
</html>
