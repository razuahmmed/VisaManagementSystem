
var fName = '';
var fValue = '';

var tbody = $("#tbody_user_history");

var search = $(".filter-submit");

search.on('click', function (e) {
    e.preventDefault();

    if ($("#userFulName").val() != '') {
        fName = $("#userFulName").attr("name");
        fValue = $("#userFulName").val();
    } else if ($("#userPhone").val() != '') {
        fName = $("#userPhone").attr("name");
        fValue = $("#userPhone").val();
    } else if ($("#userCompany").val() != '') {
        fName = $("#userCompany").attr("name");
        fValue = $("#userCompany").val();
    }

    var dataString = 'fieldName=' + fName;
    dataString += '&fieldValue=' + fValue;

    $.post('SearchUser', dataString).done(function (data) {
        tbody.html(data);
    });
});