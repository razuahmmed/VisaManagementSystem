<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Student Visa Management</title>
<link href="<%= request.getContextPath()%>/images/icon.ico" rel="shortcut icon" type="image/x-icon"/>
<link href="<%= request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/bootstrap-theme.min.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>
<link href="<%= request.getContextPath()%>/css/font-awesome.min.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/pogo-slider.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/style.css" rel="stylesheet">
<link href="<%= request.getContextPath()%>/css/responsive.css" rel="stylesheet">