<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
    </head>
    <body>
        <div class="site-wrapper">
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel panel-default loginbox">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Login You Account</h3>
                                </div> 
                                <div class="panel-body">
                                    <form id="login_form">
                                        <div style="padding-bottom:10px;" align="center" >
                                            <div id="errorMess">

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="username">User Name</label>
                                            <input type="text" id="userName" name="userName" required="required" maxlength="20" placeholder="User Name" class="form-control text">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="password" id="userPassword" name="userPassword" placeholder="Password" required="required" maxlength="20" class="form-control text">
                                        </div>  
                                        <div class="form-group submit">
                                            <input type="submit" id="btn_login" name="btn_login" value="Login" class="btn btn-primary ok"/>
                                            <img id="submit_image" style="margin-top: 10px; height: 20px; width: 20px;" src="" alt="">
                                            <!--<img id="submit_image" style="display: none; margin-top: 10px; height: 20px; width: 20px;" src="<%= request.getContextPath()%>/images/load.gif" alt=""/>-->
                                        </div>
                                    </form>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <script src="<%= request.getContextPath()%>/js/jqueryLibrary.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.pogo-slider.js"></script>
        <script src="<%= request.getContextPath()%>/js/scripts.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#btn_login').on('click', function (e) {
                    e.preventDefault();
                    document.getElementById('submit_image').src = 'images/load.gif';
                    $.post("Login", $("#login_form").serialize()).done(function (data) {
                        if (data.trim() == 1) {
                            document.getElementById('submit_image').src = '';
                            window.location = 'Dashboard';
                        } else {
                            $('#errorMess').html(data);
                            document.getElementById('submit_image').src = '';
                        }
                    });
                });


//                $("#errorMess").hide();
//                $('#login_form').submit(function () {
//                    $('#submit_image').show();
//                    return true;
//                });
            });
        </script>
    </body>
</html>
