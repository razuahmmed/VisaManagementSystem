<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<%
    // Menu Start 
    // Dashboard
    boolean PM01 = (request.getSession().getAttribute("1") != null && request.getSession().getAttribute("1").toString().equalsIgnoreCase("T")) ? true : false;
    // Agent   
    boolean PM02 = (request.getSession().getAttribute("2") != null && request.getSession().getAttribute("2").toString().equalsIgnoreCase("T")) ? true : false;
    // Agent Child
    boolean PM02CM01 = (request.getSession().getAttribute("3") != null && request.getSession().getAttribute("3").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM02CM02 = (request.getSession().getAttribute("4") != null && request.getSession().getAttribute("4").toString().equalsIgnoreCase("T")) ? true : false;
    // Applicant Service  
    boolean PM03 = (request.getSession().getAttribute("5") != null && request.getSession().getAttribute("5").toString().equalsIgnoreCase("T")) ? true : false;
    // Applicant Service Child
    boolean PM03CM01 = (request.getSession().getAttribute("6") != null && request.getSession().getAttribute("6").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM03CM02 = (request.getSession().getAttribute("7") != null && request.getSession().getAttribute("7").toString().equalsIgnoreCase("T")) ? true : false;
    // Student Visa
    boolean PM04 = (request.getSession().getAttribute("8") != null && request.getSession().getAttribute("8").toString().equalsIgnoreCase("T")) ? true : false;
    // Student Visa Child
    boolean PM04CM01 = (request.getSession().getAttribute("9") != null && request.getSession().getAttribute("9").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM02 = (request.getSession().getAttribute("10") != null && request.getSession().getAttribute("10").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM03 = (request.getSession().getAttribute("11") != null && request.getSession().getAttribute("11").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM04 = (request.getSession().getAttribute("12") != null && request.getSession().getAttribute("12").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM05 = (request.getSession().getAttribute("13") != null && request.getSession().getAttribute("13").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM04CM06 = (request.getSession().getAttribute("14") != null && request.getSession().getAttribute("14").toString().equalsIgnoreCase("T")) ? true : false;
    // Admin Module
    boolean PM05 = (request.getSession().getAttribute("15") != null && request.getSession().getAttribute("15").toString().equalsIgnoreCase("T")) ? true : false;
    // Admin Module Child
    boolean PM05CM01 = (request.getSession().getAttribute("16") != null && request.getSession().getAttribute("16").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM05CM02 = (request.getSession().getAttribute("17") != null && request.getSession().getAttribute("17").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM05CM03 = (request.getSession().getAttribute("18") != null && request.getSession().getAttribute("18").toString().equalsIgnoreCase("T")) ? true : false;
    // Accounts Module
    boolean PM06 = (request.getSession().getAttribute("19") != null && request.getSession().getAttribute("19").toString().equalsIgnoreCase("T")) ? true : false;
    // Report
    boolean PM07 = (request.getSession().getAttribute("20") != null && request.getSession().getAttribute("20").toString().equalsIgnoreCase("T")) ? true : false;
    // Report Child   
    boolean PM07CM01 = (request.getSession().getAttribute("21") != null && request.getSession().getAttribute("21").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM07CM02 = (request.getSession().getAttribute("22") != null && request.getSession().getAttribute("22").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM07CM03 = (request.getSession().getAttribute("23") != null && request.getSession().getAttribute("23").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM07CM04 = (request.getSession().getAttribute("24") != null && request.getSession().getAttribute("24").toString().equalsIgnoreCase("T")) ? true : false;
    // Setup
    boolean PM08 = (request.getSession().getAttribute("25") != null && request.getSession().getAttribute("25").toString().equalsIgnoreCase("T")) ? true : false;
    // Setup Child
    boolean PM08CM01 = (request.getSession().getAttribute("26") != null && request.getSession().getAttribute("26").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM08CM02 = (request.getSession().getAttribute("27") != null && request.getSession().getAttribute("27").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM08CM03 = (request.getSession().getAttribute("28") != null && request.getSession().getAttribute("28").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM08CM04 = (request.getSession().getAttribute("29") != null && request.getSession().getAttribute("29").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM08CM05 = (request.getSession().getAttribute("30") != null && request.getSession().getAttribute("30").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM08CM06 = (request.getSession().getAttribute("31") != null && request.getSession().getAttribute("31").toString().equalsIgnoreCase("T")) ? true : false;
    // Settings
    boolean PM09 = (request.getSession().getAttribute("32") != null && request.getSession().getAttribute("32").toString().equalsIgnoreCase("T")) ? true : false;
    // Settings Child
    boolean PM09CM01 = (request.getSession().getAttribute("33") != null && request.getSession().getAttribute("33").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM09CM02 = (request.getSession().getAttribute("34") != null && request.getSession().getAttribute("34").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM09CM03 = (request.getSession().getAttribute("35") != null && request.getSession().getAttribute("35").toString().equalsIgnoreCase("T")) ? true : false;
    boolean PM09CM04 = (request.getSession().getAttribute("36") != null && request.getSession().getAttribute("36").toString().equalsIgnoreCase("T")) ? true : false;
%>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container header-top-nav">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
            <ul class="nav navbar-nav">
                <% if (PM01) {%>
                <li class="">
                    <a href="Dashboard">
                        Dashboard
                    </a>
                </li>
                <% }%>
                <% if (PM02) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Agent
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <% if (PM02CM01) {%>
                        <li>
                            <a href="AddAgent">Add Agent</a>
                        </li>
                        <% }%>
                        <% if (PM02CM02) {%>
                        <li>
                            <a href="ManageAgent">Manage Agent</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
                <% if (PM03) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Allocate Service
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%  if (PM03CM01) {%>
                        <li>
                            <a href="StudentService">Allocate Applicant Service</a>
                        </li>
                        <% }%>
                        <%  if (PM03CM02) {%>
                        <li>
                            <a href="ApplicantServiceHistory">Manage Allocate Applicant Service</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
                <% if (PM04) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Visa Management
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%  if (PM04CM01) {%>
                        <li>
                            <a href="AddStudent">Student Visa</a>
                        </li>
                        <% }%>

                        <li>
                            <a href="AddWorker">Worker Visa</a>
                        </li>

                        <%  if (PM04CM02) {%>
                        <li>
                            <a href="StudentHistory">Student History</a>
                        </li>
                        <% }%>

                        <li>
                            <a href="WorkerHistory">Worker History</a>
                        </li>

                        <%  if (PM04CM03) {%>
                        <li>
                            <a href="Payment">Payment Receive</a>
                        </li>
                        <% }%>
                        <%  if (PM04CM04) {%>
                        <li>
                            <a href="PaymentHistory">Payment History</a>
                        </li>
                        <% }%>
                        <%  if (PM04CM06) {%>
                        <li>
                            <a href="DuePayment">Payment Due</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>

                <% if (PM05) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Admin Module 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%  if (PM05CM01) {%>
                        <li>
                            <a href="AddAdminUser">Add Admin User</a>
                        </li>
                        <% }%>
                        <%  if (PM05CM02) {%>
                        <li>
                            <a href="ManageAdminUser">Manage Admin User</a>
                        </li>
                        <% }%>
                        <%  if (PM05CM03) {%>
                        <li>
                            <a href="MenuSettings">Menu Settings</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
                <% if (PM07) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Report 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <% if (PM07CM01) {%>
                        <li>
                            <a href="GenerateReport">Agent Report</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
                <% if (PM08) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Setup 
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%  if (PM08CM01) {%>
                        <li>
                            <a href="UniversitySetup">Varsity Courses</a>
                        </li>
                        <li>
                            <a href="ManageVarsityCourses">Manage Varsity Courses</a>
                        </li>
                        <% }%>
                        <%  if (PM08CM02) {%>
                        <li>
                            <a href="ServiceSettings">Create Service</a>
                        </li>
                        <% }%>
                        <%  if (PM08CM03) {%>
                        <li>
                            <a href="ManageService">Manage Service</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
                <% if (PM09) {%>
                <li class="dropdown">
                    <a href="javascript:;" aria-expanded="false" aria-haspopup="true" role="button" data-toggle="dropdown" class="dropdown-toggle">
                        Profile
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <%  if (PM09CM01) {%>
                        <li>
                            <a href="MyProfile">My Profile</a>
                        </li>
                        <% }%>
                        <%  if (PM09CM02) {%>
                        <li>
                            <a href="EditProfile">Edit Profile</a>
                        </li>
                        <% }%>

                        <%  if (PM09CM03) {%>
                        <li>
                            <a href="ChangePassword">Change Password</a>
                        </li>
                        <% }%>
                        <%  if (PM09CM04) {%>
                        <div role="separator" class="divider"></div>
                        <li>
                            <a href="Logout">Log Out</a>
                        </li>
                        <% }%>
                    </ul>
                </li>
                <% }%>
            </ul>
        </div>
    </div>
</nav>