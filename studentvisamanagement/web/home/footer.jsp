<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-8">
                <div class="copyright">
                    <span class="copy">� 2017 RIT. All Rights Reserved.</span>
                </div>
            </div>
            <div class="col-md-6 col-sm-4">
                <div class="footer-right">
                    <div class="sociles">
                        <a href="" class="social"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="" class="social"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="" class="social"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        <a href="" class="social"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>