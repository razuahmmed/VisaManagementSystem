<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <!-- Ionicons -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/dashboardUi.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/datepicker3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/bootstrap3-wysihtml5.min.css">
        <%@include file="/home/resource-css.jsp" %>
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/bootstrap.min.css">

        <%
            String gID = (request.getSession().getAttribute("GROUP_ID") != null ? request.getSession().getAttribute("GROUP_ID").toString() : "");
        %>

        <style>
            .container.header-top-nav {
                font-family: "Montserrat-Light";
                font-size: 13px;
                padding-left: 0px;
                padding-right: 0px;
            }

            .navbar-collapse.collapse {
                margin: 0 0 0 0;
                padding: 0 0 0 0;
                text-align: center;
            }

            .navbar-nav {
                float: none; 
                display: inline-block;
            }
        </style>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">

                                <section class="content-header">
                                    <h1>
                                        Dashboard
                                        <small>Control panel</small>
                                    </h1>
                                </section>

                                <section class="content">

                                    <div class="row">
                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-teal-active">
                                                <div class="inner">
                                                    <h3>
                                                        <s:property value="totalApplicant"/>
                                                    </h3>
                                                    <p>Total Applicant</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-person-add"></i>
                                                </div>
                                                <a href="ApplicantHistory" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-yellow">
                                                <div class="inner">
                                                    <h3>
                                                        <s:if test="totalAgree!=null">
                                                            <s:property value="totalAgree"/>
                                                        </s:if>
                                                        <s:else>
                                                            0
                                                        </s:else>
                                                    </h3>
                                                    <p>Total Payment Agree</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-cash"></i>
                                                </div>
                                                <a href="DuePayment" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-green">
                                                <div class="inner">
                                                    <h3>
                                                        <s:if test="totalPayment!=null">
                                                            <s:property value="totalPayment"/>
                                                        </s:if>
                                                        <s:else>
                                                            0
                                                        </s:else>
                                                    </h3>
                                                    <p>Total Payment</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-briefcase"></i>
                                                </div>
                                                <a href="DuePayment" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <div class="small-box bg-red">
                                                <div class="inner">
                                                    <h3>
                                                        <s:if test="totalDue!=null">
                                                            <s:property value="totalDue"/>
                                                        </s:if>
                                                        <s:else>
                                                            0
                                                        </s:else>
                                                    </h3>
                                                    <p>Total Payment Due</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-pie-graph"></i>
                                                </div>
                                                <a href="DuePayment" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-xs-6">
                                            <!-- small box -->
                                            <%if (gID.equals("0")) {%>
                                            <div class="small-box bg-aqua">
                                                <div class="inner">
                                                    <h3>
                                                        <s:property value="totalAgent"/>
                                                    </h3>
                                                    <p>Total Agent</p>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-person"></i>
                                                </div>
                                                <a href="ManageAgent" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <%}%>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <section class="col-lg-7 connectedSortable">

                                            <div class="box box-info">

                                                <div class="box-header">
                                                    <i class="fa fa-envelope"></i>
                                                    <h3 class="box-title">Write Formatting</h3>
                                                    <!-- tools box -->
                                                    <div class="pull-right box-tools">
                                                        <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="box-body">
                                                    <form action="#" method="post">
                                                        <div>
                                                            <textarea class="textarea" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </section>

                                        <section class="col-lg-5 connectedSortable">
                                            <div class="box box-solid bg-green-gradient">

                                                <div class="box-header">
                                                    <i class="fa fa-calendar"></i>
                                                    <h3 class="box-title">Calendar</h3>
                                                    <div class="pull-right box-tools">
                                                        <button type="button" class="btn btn-success btn-sm" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                        <button type="button" class="btn btn-success btn-sm" data-widget="remove">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="box-body no-padding">
                                                    <!--The calendar -->
                                                    <div id="calendar" style="width: 100%"></div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </section>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <!-- jQuery 2.2.3 -->
        <script src="<%= request.getContextPath()%>/js/jquery-2.2.3.min.js"></script>

        <!-- jQuery UI 1.11.4 -->
        <script src="<%= request.getContextPath()%>/js/jquery-ui.min.js"></script>

        <!-- Bootstrap 3.3.6 -->
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>

        <!-- Sparkline -->
        <script src="<%= request.getContextPath()%>/js/jquery.sparkline.min.js"></script>

        <!-- jvectormap -->
        <script src="<%= request.getContextPath()%>/js/jquery-jvectormap-1.2.2.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/jquery-jvectormap-world-mill-en.js"></script>

        <!-- jQuery Knob Chart -->
        <script src="<%= request.getContextPath()%>/js/jquery.knob.js"></script>

        <!-- daterangepicker -->
        <script src="<%= request.getContextPath()%>/js/moment.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/daterangepicker.js"></script>

        <!-- datepicker -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker2.js"></script>

        <!-- Bootstrap WYSIHTML5 -->
        <script src="<%= request.getContextPath()%>/js/bootstrap3-wysihtml5.all.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/app.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/dashboard.js"></script>
    </body>
</html>
