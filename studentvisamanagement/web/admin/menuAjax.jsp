<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<s:if test="userLevelMenuInfoList.size() != 0 ">
    <% int pid = 0;%>
    <s:iterator value="userLevelMenuInfoList">
        <% pid++;%>
        <% int cid = 0;%>
        <tr class="tbl-tr">
            <td class="mas-chk">
                <s:if test="levelMenuStatus == 'Y' ">
                    <input type="checkbox" value="<s:property value="menuMaster.masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="masterMenuID" checked>
                </s:if>
                <s:else>
                    <input type="checkbox" value="<s:property value="menuMaster.masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=pid%>CH<%=cid%>', '<%=pid%>');" id="PR<%=pid%>CH<%=cid%>" name="masterMenuID">
                </s:else>
            </td>
            <td class="cld-name">
                <s:property value="menuMaster.masterMenuName"/>
            </td>
            <s:if test="userLevelMenuChildList != null">
                <s:if test="userLevelMenuChildList.size()!=0">
                    <td onclick="menuShowHide('collapseMenu<%=pid%>')" class="td-icon">[-]</td>
                </s:if>
                <s:else>
                    <td></td>
                </s:else>
            </s:if>
            <s:else>
                <td></td>
            </s:else>
        </tr>
        <s:iterator value="userLevelMenuChildList">
            <% cid++;%>
            <tr class="chm tbl-tr collapseMenu<%=pid%>">
                <td class="chk-child">
                    <s:if test="userLevelMenu.levelMenuStatus == 'Y' ">
                        <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenuID" checked>
                    </s:if>
                    <s:else>
                        <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=pid%>CH<%=cid%>', 'PR<%=pid%>CH0', '<%=pid%>');" class="chkbox<%=pid%>" id="PR<%=pid%>CH<%=cid%>" name="childMenuID">
                    </s:else>
                </td>
                <td class="cld-name">
                    <s:property value="childMenuName"/>
                </td>
            </tr>
        </s:iterator>
    </s:iterator>
</s:if>
<s:if test="menuMasterInfoList.size() != 0 ">
    <% int mpid = 0;%>
    <s:iterator value="menuMasterInfoList">
        <% mpid++;%>
        <% int mcid = 0;%>
        <tr class="tbl-tr">
            <td class="mas-chk">
                <s:if test="masterMenuStatus == 'Y' ">
                    <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID" checked>
                </s:if>
                <s:else>
                    <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID">
                </s:else>
            </td>
            <td class="cld-name">
                <s:property value="masterMenuName"/>
            </td>
            <s:if test="menuChildList != null">
                <s:if test="menuChildList.size()!=0">
                    <td onclick="menuShowHide('collapseMenu<%=mpid%>')" class="td-icon">[-]</td>
                </s:if>
                <s:else>
                    <td></td>
                </s:else>
            </s:if>
            <s:else>
                <td></td>
            </s:else>
        </tr>
        <s:iterator value="menuChildList">
            <% mcid++;%>
            <tr class="chm tbl-tr collapseMenu<%=mpid%>">
                <td class="chk-child">
                    <s:if test="childMenuStatus == 'Y' ">
                        <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID" checked>
                    </s:if>
                    <s:else>
                        <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID">
                    </s:else>
                </td>
                <td class="cld-name">
                    <s:property value="childMenuName"/>
                </td>
            </tr>
        </s:iterator>
    </s:iterator>
</s:if>
<script type="text/javascript">
    $(document).ready(function () {
        $('.chm').hide();
    });
</script>