<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <script type="text/javascript">
            function parentMenuCheckBox(chkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    $('.chkbox' + id).each(function () {

                        this.checked = true;
                    });
                } else {
                    $('.chkbox' + id).each(function () {

                        this.checked = false;
                    });

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function subMenuCheckBox(chkBxId, pchkBxId, id) {

                var chckbox = document.getElementById(chkBxId);

                if (chckbox.checked) {

                    document.getElementsByName("parentMenu").checked = true;

                    document.getElementById(pchkBxId).checked = true;

                } else {

                    if ($(".chkbox" + id + ":checked").length == 0) {

                        document.getElementById(pchkBxId).checked = false;
                    }

                    if ($("input:checkbox:checked").length == 1) {

                        document.getElementsByName("parentMenu").checked = false;
                    }
                }
            }

            function menuShowHide(collapseSubMenu) {

                for (var i = 1; i < collapseSubMenu.length; i++) {
                    $("." + collapseSubMenu + i).hide();
                }
                $("." + collapseSubMenu).slideToggle("slow");
            }
        </script>

        <style type="text/css">

            table {
                width: 100%
            }

            .chk-child {
                padding-left: 20px;
                text-align: center;
            }

            .cld-name {
                text-align: left; 
                padding-left: 3px;
            }

            .tbl-tr {
                background-color: #336ea5; 
                font-size: 12px;
                color: #ffffff;
            }

            .td-icon {
                cursor:pointer; 
                text-align: center;
            }

            .mas-chk {
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Menu Settings</h3>
                                </div> 

                                <form action="SaveMenuPermission" method="POST" class="form-horizontal">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Select User Group</label>
                                            <div class="col-sm-4">
                                                <select id="userGroupID" name="userGroupID" required class="form-control">
                                                    <option selected value="">Select User Group</option>
                                                    <s:if test="userGroupInfoList.size() != 0 ">
                                                        <s:iterator value="userGroupInfoList">
                                                            <option value="<s:property value="userGroupID"/>"><s:property value="groupName"/></option>
                                                        </s:iterator>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Menu</label>
                                            <div class="col-sm-4">
                                                <table id="groupFormTbl">
                                                    <tbody id="tbl-menu">
                                                        <% int mpid = 0;%>
                                                        <s:if test="menuMasterInfoList.size() != 0 ">
                                                            <s:iterator value="menuMasterInfoList">
                                                                <% mpid++;%>
                                                                <% int mcid = 0;%>
                                                                <tr class="tbl-tr">
                                                                    <td class="mas-chk">
                                                                        <s:if test="masterMenuStatus == 'Y' ">
                                                                            <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID" checked>
                                                                        </s:if>
                                                                        <s:else>
                                                                            <input type="checkbox" value="<s:property value="masterMenuID"/>" onclick="parentMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', '<%=mpid%>');" id="PR<%=mpid%>CH<%=mcid%>" name="masterMenuID">
                                                                        </s:else>
                                                                    </td>

                                                                    <td class="cld-name">
                                                                        <s:property value="masterMenuName"/>
                                                                    </td>

                                                                    <s:if test="menuChildList != null">
                                                                        <s:if test="menuChildList.size()!=0">
                                                                            <td onclick="menuShowHide('collapseMenu<%=mpid%>')" class="td-icon">[-]</td>
                                                                        </s:if>
                                                                        <s:else>
                                                                            <td></td>
                                                                        </s:else>
                                                                    </s:if>
                                                                    <s:else>
                                                                        <td></td>
                                                                    </s:else>
                                                                </tr>

                                                                <s:iterator value="menuChildList">
                                                                    <% mcid++;%>
                                                                    <tr class="chm tbl-tr collapseMenu<%=mpid%>">
                                                                        <td class="chk-child">
                                                                            <s:if test="childMenuStatus == 'Y' ">
                                                                                <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID" checked>
                                                                            </s:if>
                                                                            <s:else>
                                                                                <input type="checkbox" value="<s:property value="childMenuID"/>" onclick="subMenuCheckBox('PR<%=mpid%>CH<%=mcid%>', 'PR<%=mpid%>CH0', '<%=mpid%>');" class="chkbox<%=mpid%>" id="PR<%=mpid%>CH<%=mcid%>" name="childMenuID">
                                                                            </s:else>
                                                                        </td>
                                                                        <td class="cld-name">
                                                                            <s:property value="childMenuName"/>
                                                                        </td>
                                                                    </tr>
                                                                </s:iterator>
                                                            </s:iterator>
                                                        </s:if>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input type="hidden" id="formMenuIdStatus" name="formMenuIdStatus"/>
                                                <button type="submit" name="btn-menu" class="btn btn-primary">Save</button>
                                                <button type="reset" name="btn-cancel" class="btn btn-primary">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.chm').hide();

                $('form').submit(function () {
                    var formMenuIdStatus = '';
                    $(this).find('input[type="checkbox"]').each(function () {
                        var checkbox = $(this);
                        if (checkbox.is(':checked')) {
                            formMenuIdStatus += checkbox.val() + '/mid/Y/mstatus/';
                        } else {
                            formMenuIdStatus += checkbox.val() + '/mid/N/mstatus/';
                        }
                    });

                    $('#formMenuIdStatus').val(formMenuIdStatus);
                });

                $("#userGroupID").change(function () {
                    var ugId = $(this).find(":selected").val();
                    $.post("MenuBySelectedGroup", {userGroupID: ugId}, function (result) {
                        $("#tbl-menu").html(result);
                    });
                });
            });
        </script>
    </body>
</html>
