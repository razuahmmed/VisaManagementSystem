<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/jquery-confirm.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Change Password</h3>
                                </div> 
                                <form action="ChangeUserPassword" method="post" id="pass-form" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="currentPassword">Current Password</label>
                                            <div class="col-sm-4">
                                                <input type="password" id="currentPassword" name="currentPassword" required placeholder="Current Password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="newPassword">New Password</label>
                                            <div class="col-sm-4">
                                                <input type="password" id="newPassword" name="newPassword" pattern="[a-zA-Z0-9]{3,15}" required placeholder="New Password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="retypeNewPassword">Retype New Password</label>
                                            <div class="col-sm-4">
                                                <input type="password" id="retypeNewPassword" name="retypeNewPassword" pattern="[a-zA-Z0-9]{3,15}" required placeholder="Retype New Password" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input class="btn btn-primary" type="submit" value="Change Password"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script src="<%= request.getContextPath()%>/js/jquery-confirm.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#pass-form").submit(function (e) {
                    var newPassword = $("#newPassword").val();
                    var retypeNewPassword = $("#retypeNewPassword").val();
                    if (newPassword !== retypeNewPassword) {
                        e.preventDefault();
                        $.confirm({
                            columnClass: 'col-md-5 col-md-offset-3',
                            title: 'Warning!',
                            content: 'New Password and Retype Password does not match !',
                            backgroundDismiss: true,
                            closeIcon: true,
                            typeAnimated: true,
                            buttons: {
                                tryAgain: {
                                    text: 'Ok'
                                }
                            }
                        });
                    }
                });
            });
        </script>
    </body>
</html>
