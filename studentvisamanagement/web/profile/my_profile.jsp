<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">My Profile</h3>
                                </div> 
                                <div class="form-horizontal">
                                    <div class="panel-body">
                                        <s:if test="singleAgentInfoList !=null">
                                            <s:if test="singleAgentInfoList.size() !=0">
                                                <s:iterator value="singleAgentInfoList">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">User Type</label>
                                                        <div class="col-md-4">
                                                            <s:property value="userGroupInfo.groupName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Full Name</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userFullName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">User Name</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Company Name</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="company"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <a href="ChangePassword">Change Password</a>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Phone Number</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userPhone"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Fax Number</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userFax"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userEmail"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <a href="EditProfile">Edit Profile</a>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Date Of Birth</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userDob"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Country</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="countryInfo.countryName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Address</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="userAddress"/>
                                                        </div>
                                                    </div>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>
    </body>
</html>
