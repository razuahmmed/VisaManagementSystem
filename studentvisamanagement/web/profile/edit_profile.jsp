<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Edit Profile</h3>
                                </div> 
                                <form action="EditUserProfile" method="post" class="form-horizontal">
                                    <div class="panel-body">
                                        <s:if test="singleAgentInfoList !=null">
                                            <s:if test="singleAgentInfoList.size() !=0">
                                                <s:iterator value="singleAgentInfoList">

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" for="userGroup">User Type</label>
                                                        <div class="col-md-4">
                                                            <input type="text" id="userGroup" name="" value="<s:property value="userGroupInfo.groupName"/>" disabled class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Full Name *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="fullName" name="fullName" value="<s:property value="userFullName"/>" required placeholder="full name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">User Name</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="userName" name="" value="<s:property value="userName"/>" disabled placeholder="User Name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Company Name</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="company" name="company" value="<s:property value="company"/>" pattern="[A-Za-z-0-9 ]{3,20}" minlength="3" maxlength="20" placeholder="Company Name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Phone Number *</label>
                                                        <div class="col-sm-4">
                                                            <input type="tel" id="phoneNumber" name="phoneNumber" placeholder="Phone Number" required value="<s:property value="userPhone"/>" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Fax Number</label>
                                                        <div class="col-sm-4">
                                                            <input type="tel" id="faxNumber" name="faxNumber" value="<s:property value="userFax"/>" pattern="[0-9]{8,15}" minlength="8" maxlength="15" placeholder="Fax Number" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email *</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" id="email" name="email" value="<s:property value="userEmail"/>" required placeholder="Like : abc@gmail.com" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Date Of Birth *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="dob" name="dob" value="<s:property value="userDob"/>" required placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="demo-is-inputlarge" class="col-sm-3 control-label">Country *</label>
                                                        <div class="col-sm-4">
                                                            <select id="country" name="country" required class="form-control">
                                                                <option selected value="">Select Country</option>
                                                                <s:if test="countryInfoList !=null">
                                                                    <s:if test="countryInfoList.size() !=0">
                                                                        <s:iterator value="countryInfoList">
                                                                            <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Address *</label>
                                                        <div class="col-sm-4">
                                                            <textarea id="address" name="address" placeholder="address" required class="form-control"><s:property value="userAddress"/></textarea>
                                                        </div>
                                                    </div>
                                                </s:iterator>
                                            </s:if>
                                        </s:if>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input class="btn btn-primary" type="submit" name="submit" value="Submit"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                var date_dob = $('input[name="dob"]');

                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };

                date_dob.datepicker(options);
            });
        </script>
    </body>
</html>
