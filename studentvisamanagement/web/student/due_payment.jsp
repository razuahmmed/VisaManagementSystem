<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR DATA TABLE -->
        <link href="<%= request.getContextPath()%>/css/dataTables.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">

            .input-inline {
                display: inline-block;
                width: auto;
                vertical-align: middle;
            }

            .blue.btn {
                color: #fff;
                background-color: #4b8df8;
                color: #FFF;
            }

            .btn {
                border-width: 0;
                padding: 7px 14px;
                font-size: 12px;
                outline: none;
                background-image: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                text-shadow: none;
            }

            .yellow.btn {
                color: #fff;
                background-color: #ffb848;
            }

            .red.btn {
                color: #fff;
                background-color: #d84a38;
            }
            
            hr {
                margin: 0 0 0 0;
            }

            /*            
            .table-scrollable {
                width: 100%;
                max-height: 630px;
                overflow-x: auto;
                overflow-y: auto;
                margin: 10px 0 !important;
             }
            */

            /*            .
            margin-bottom-5, .btn-editable {
                margin-bottom: 5px;
            }
            */

        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Applicant Payment Due History</h3>
                                </div> 

                                <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="12%">Applicant</th>
                                                <th width="12%">Agent</th>
                                                <th width="8%">Pay Date</th>
                                                <th width="11%">Tuition Agree<hr>Tuition Pay<hr>Tuition Due</th>
                                                <th width="9%">Others Agree<hr>Others Pay<hr>Others Due</th>
                                                <th width="10%">Worker Agree<hr>Worker Pay<hr>Worker Due</th>
                                                <th width="9%">Actions</th>
                                            </tr>
                                            <tr style="text-align: center;" class="filter">
                                                <td><input type="text" id="applicantName" name="applicantName" class="form-control form-filter input-sm"></td>
                                                <td><input type="text" id="agentName" name="agentName" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td style="width: 90px;">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom" data-toggle="tooltip" data-placement="top" title="Search Button"><i class="icon-magnifier"></i></button>
                                                    <button class="btn btn-sm red filter-cancel" data-toggle="tooltip" data-placement="top" title="Reset Button"><i class="icon-close"></i></button>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_due_pay">
                                            <s:if test="payDueInfoList !=null">
                                                <s:if test="payDueInfoList.size() !=0">
                                                    <s:iterator value="payDueInfoList">
                                                        <tr style="text-align: center;">
                                                            <td align="center"><s:property value="applicantInfo.applicantName"/></td>
                                                            <td align="center"><s:property value="userInfo.userFullName"/></td>
                                                            <td><s:property value="paymentDate"/></td>
                                                            <td><s:property value="tuitionAgree"/><hr><s:property value="tuitionFee"/><hr><s:property value="tuitionFeeDue"/></td>
                                                            <td><s:property value="othersAgree"/><hr><s:property value="othersAmount"/><hr><s:property value="othersAmountDue"/></td>
                                                            <td><s:property value="workerAgree"/><hr><s:property value="workerAmount"/><hr><s:property value="workerAmountDue"/></td>
                                                            <td>
                                                                <a href="javascript:void(0);" id="<s:property value="paymentID"/>" class="btn btn-xs blue btn-editable" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                                                                    <i class="icon-pencil"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="5" style="text-align: right;">Total</th>
                                                <th colspan="5" style="text-align: left;"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- JQUERY FOR DATA TABLE -->
        <script src="<%= request.getContextPath()%>/js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/dataTables.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(5)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(5, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(5).footer()).html(
                                '' + pageTotal + ' (Grand Total : ' + total + ')'
                                );
                    }
                });

                var fName = '';
                var fValue = '';
                var tbody = $("#tbody_due_pay");
                var search = $(".filter-submit");
                search.on('click', function (e) {
                    e.preventDefault();
                    if ($("#applicantName").val() != '') {
                        fName = $("#applicantName").attr("name");
                        fValue = $("#applicantName").val();
                    } else if ($("#agentName").val() != '') {
                        fName = $("#agentName").attr("name");
                        fValue = $("#agentName").val();
                    }
                    var dataString = 'fieldName=' + fName;
                    dataString += '&fieldValue=' + fValue;
                    $.post('SearchDuePayment', dataString).done(function (data) {
                        tbody.html(data);
                    });
                });

                $('.filter-cancel').on('click', function (e) {
                    e.preventDefault();
                    $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                        $(this).val("");
                    });
                    setTimeout(location.reload(), 0);
                });
            });
        </script>
    </body>
</html>
