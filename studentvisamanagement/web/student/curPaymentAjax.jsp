<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="paymentInfoList.size() !=0">
    <div class="form-group">
        <div>  
            <table style="width: 100%">
                <thead>
                    <tr>
                        <th style="text-align: center;" colspan="4" scope="col">Payment Received Summary of : 
                            <s:iterator value="paymentInfoList">
                                <i><s:property value="applicantInfo.applicantName"/></i>
                            </s:iterator>
                        </th>
                    </tr>
                    <tr>
                        <th scope="col">Last Pay Date</th>
                        <th scope="col">Agree</th>
                        <th scope="col">Pay</th>
                        <th scope="col">Due</th>
                    </tr>
                </thead>
                <tbody id="">
                    <s:iterator value="paymentInfoList">
                        <tr>
                            <td>
                                <label for=""><s:property value="paymentDate"/></label>
                            </td>
                            <td>
                                <label for=""><s:property value="tuitionAgree"/></label>
                            </td>
                            <td>
                                <label for=""><s:property value="amount"/></label>
                            </td>
                            <td>
                                <label for=""><s:property value="tuitionFeeDue"/></label>
                            </td>
                        </tr>
                    </s:iterator>
                </tbody>
            </table>
        </div>
    </div>
</s:if>
<s:else>
    <table style="width: 100%">
        <tbody id="">
            <tr>
                <td style="text-align: center;" colspan="4">
                    <label for="">Payment Received Summary Not Found</label>
                </td>
            </tr>
        </tbody>
    </table>
</s:else>