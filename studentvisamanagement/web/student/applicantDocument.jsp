<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <style type="text/css">
            .same_above{
                margin-left: 0px;
                width: 57%;
            }

            #table-items>thead>tr>th, #table-items>tbody>tr>td {
                text-align: center;
            }

            .table {
                counter-reset:section;
            }

            .count:before {
                counter-increment:section;
                content:counter(section);
            }


            #divProName th, td {
                padding: 5px;
                margin: 0px;
                border-bottom: #eee 1px solid;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Applicant Documents</h3>
                                </div> 
                                <form id="from-student" action="AddDocuments" method="POST" class="form-horizontal" enctype="multipart/form-data">
                                    <s:if test="singleAppInfoList !=null">
                                        <s:if test="singleAppInfoList.size() !=0">
                                            <s:iterator value="singleAppInfoList">
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="demo-is-inputsmall">Agent Name</label>
                                                        <div class="col-sm-4"> 
                                                            <s:property value="userInfo.userFullName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant Name</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="applicantName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant ID</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="applicantID"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Document Name</label>
                                                        <div class="col-sm-4">
                                                            <label>File</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant Photo</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document1" name="document1">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Full</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document2" name="document2">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Page2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document3" name="document3">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Last Page1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document4" name="document4">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical All documents</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document5" name="document5">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical Form Page1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document6" name="document16">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical Form Page2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document7" name="document7">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical Form Page3</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document8" name="document8">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical Form Page4</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document9" name="document9">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Medical Form Page5</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document10" name="document10">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Full documents</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document11" name="document11">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document12" name="document12">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate3</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document13" name="document13">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate4</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document14" name="document14">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate5</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document15" name="document15">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate6</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document16" name="document16">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Academic Certificate7</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document17" name="document17">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Attestation Form</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document18" name="document18">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Lab Report1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document19" name="document19">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Lab Report2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document20" name="document20">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Lab Report3</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document21" name="document21">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Lab Report4</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document22" name="document22">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Lab Report5</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document23" name="document23">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Other Supporting Doc1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document24" name="document24">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Other Supporting Doc2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document25" name="document25">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Visa Approval 1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document26" name="document26">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Visa Approval 2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document27" name="document27">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Visa Approval 3</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document28" name="document28">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Money Receipts 1</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document29" name="document29">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Money Receipts 2</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="document30" name="document30">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <label>Other Documents</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Document Name</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="documentName" name="documentName">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Document</label>
                                                        <div class="col-sm-4">
                                                            <input type="file" id="documentOthers" name="documentOthers">
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-sm-9 col-sm-offset-3">
                                                            <input type="hidden" id="applicantID" name="applicantID" value="<s:property value="applicantID"/>">
                                                            <input type="hidden" id="agentId" name="agentId" value="<s:property value="userInfo.userID"/>">
                                                            <button type="submit" id="btn_change" name="btn_change" class="btn btn-primary btn_change">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/jquery-confirm.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery-confirm.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".btn_change").on('click', function () {
                    $.confirm({
                        icon: 'fa fa-warning',
                        title: 'Confirmation !',
                        content: 'Do you want to add new applicant ?',
                        type: 'green',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Sure',
                                btnClass: 'btn-green',
                                action: function () {
                                }
                            },
                            close: {
                                text: 'No',
                                btnClass: 'btn-red',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                var date_dob = $('input[name="dob"]');
                var date_passport_issue = $('input[name="passportIssueDate"]');
                var date_passport_expiry = $('input[name="passportExpiryDate"]');
                var date_visa_expiry = $('input[name="visaExpiryDate"]');
                var date_arrival = $('input[name="arrivalDate"]');

                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };

                date_dob.datepicker(options);
                date_passport_issue.datepicker(options);
                date_passport_expiry.datepicker(options);
                date_visa_expiry.datepicker(options);
                date_arrival.datepicker(options);

                $("#chkbSame").click(function () {
                    if ($(this).is(":checked")) {
                        $("#corAddress").val($("#permanentAddress").val());
                        $("#corCity").val($("#city").val());
                        $("#corPostalCode").val($("#postalCode").val());
                        $("#corState").val($("#state").val());
                        $("#corCountry").val($("#country").val());
                        $("#corMobileNumber").val($("#mobileNumber").val());
                        $("#corResidentNumber").val($("#residentNumber").val());
                    } else {
                        $("#corAddress").val('');
                        $("#corCity").val('');
                        $("#corPostalCode").val('');
                        $("#corState").val('');
                        $("#corCountry").val('');
                        $("#corMobileNumber").val('');
                        $("#corResidentNumber").val('');
                    }
                });

                var remove_row = $('#remove-row');
                remove_row.hide();

                remove_row.on('click', function () {
                    $("#table-items tr.info").fadeOut(300, function () {
                        $(this).remove();
                    });
                });

                $('#add-row').on('click', function () {
                    $("#table-items").find('tbody')
                            .append('<tr>\n\
                                        <td class="count"></td>\n\
                                        <td><input type="text" id="degreeName" name="degreeName" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Name of Degree" class="form-control item"></td>\n\
                                        <td><input type="text" id="result" name="result" pattern="[0-9-.]{4,4}" required placeholder="Result" class="form-control item"></td>\n\
                                        <td><input type="text" id="passingYear" name="passingYear" pattern="[0-9]{4,4}" required placeholder="Passing Year" class="form-control item"></td>\n\
                                        <td><input type="text" id="institution" name="institution" pattern="[A-Za-z-0-9]{3,30}" required placeholder="Institute name" class="form-control item"></td>\n\
                                    </tr>');
                });

                $('#table-items').on('click', '.item', function () {
                    $("tr").removeClass("info");
                    $(this).closest('tr').addClass("info");
                    remove_row.show();
                });

                $(".chkbVarId").click(function () {

                    var varID = this.id;

                    if ($(this).is(":checked")) {
                        document.getElementById('createLoadingImage').src = 'images/load.gif';
                        $.post('GetVarsityCourses', {
                            universityID: varID
                        }).done(function (data) {
                            document.getElementById('createLoadingImage').src = '';
                            $("#selected-courses").append(data);
                        });
                    } else if ($(this).not(":checked")) {
                        var id = this.id;
                        $('#program > tbody').each(function () {
                            $("#program tr." + id + "").fadeOut(300, function () {
                                $(this).remove();
                            });
                        });
                    }
                });

                $('#agentId').select2().on("change", function (e) {
                });
            });
        </script>

    </body>
</html>
