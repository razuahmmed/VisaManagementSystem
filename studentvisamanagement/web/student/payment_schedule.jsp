<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Payment Schedule</h3>
                                </div> 
                                <form class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Full Name*</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="fullName" name="fullName" placeholder="full name" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">User Name*</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="userName" name="userName" placeholder="user name" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><strong>Phone Number*</strong></label>
                                            <div class="col-sm-4">
                                                <input type="tel" placeholder="Like : +8801555555555" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Email*</label>
                                            <div class="col-sm-4">
                                                <input type="email" id="email" name="email" placeholder="Like : abc@gmail.com" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Password</label>
                                            <div class="col-sm-4">
                                                <input type="password" id="password" name="password" placeholder="password" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Date Of Birth*</label>
                                            <div class="col-sm-4">
                                                <input type="date" id="dob" name="dob" placeholder="Like : 12-12-2017" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Gender*</label>
                                            <div class="col-md-4">
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        <input type="radio" id="male" name="gender" checked>
                                                        Male
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="female" name="gender">
                                                        Female
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Country*</label>
                                            <div class="col-sm-4">
                                                <select id="country" name="country" class="form-control">
                                                    <option value="0">Select A Country</option>
                                                    <option value="bd">Bangladesh</option>
                                                    <option value="ml">Malaysia</option> 
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Address*</label>
                                            <div class="col-sm-4">
                                                <textarea id="address" name="address" placeholder="address" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Note</label>
                                            <div class="col-sm-4">
                                                <textarea id="note" name="note" placeholder="note" class="form-control"></textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input class="btn btn-primary" type="submit" name="submit" value="Submit"> 
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>
    </body>
</html>
