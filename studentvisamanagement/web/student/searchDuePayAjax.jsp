<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="payDueInfoList !=null">
    <s:if test="payDueInfoList.size() !=0">
        <s:iterator value="payDueInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="applicantInfo.applicantName"/></td>
                <td align="center"><s:property value="userInfo.userFullName"/></td>
                <td><s:property value="paymentDate"/></td>
                <td><s:property value="tuitionAgree"/></td>
                <td><s:property value="tuitionFee"/></td>
                <td><s:property value="tuitionFeeDue"/></td>
                <td><s:property value="othersAgree"/></td>
                <td><s:property value="othersAmount"/></td>
                <td><s:property value="othersAmountDue"/></td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="paymentID"/>" class="btn btn-xs blue btn-editable" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                        <i class="icon-pencil"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>