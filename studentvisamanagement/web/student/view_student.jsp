<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <style type="text/css">
            .same_above{
                margin-left: 0px;
                width: 57%;
            }

            #table-items>thead>tr>th, #table-items>tbody>tr>td {
                text-align: center;
            }

            .table {
                counter-reset:section;
            }

            .count:before {
                counter-increment:section;
                content:counter(section);
            }


            #divProName th, td {
                padding: 5px;
                margin: 0px;
                border-bottom: #eee 1px solid;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">View/Edit Applicant Information</h3>
                                </div> 
                                <form id="from-student" action="ChangeApplicantInfo" method="POST" class="form-horizontal">
                                    <s:if test="singleAppInfoList !=null">
                                        <s:if test="singleAppInfoList.size() !=0">
                                            <s:iterator value="singleAppInfoList">
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="demo-is-inputsmall">Agent *</label>
                                                        <div class="col-sm-4"> 
                                                            <select id="agentId" name="agentId" data-placeholder="" required class="form-control drop-select">
                                                                <s:if test="agentInfoList !=null">
                                                                    <s:if test="agentInfoList.size() !=0">
                                                                        <s:iterator value="agentInfoList">
                                                                            <option <s:if test="userID==userInfo.userID">selected</s:if> value="<s:property value="userID"/>"><s:property value="userFullName"/>&nbsp;&nbsp;<s:if test="userStatus!='Y'">(Inactive Agent)</s:if></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="demo-is-inputlarge" class="col-sm-3 control-label">Nationality *</label>
                                                        <div class="col-sm-4">
                                                            <select id="nationality" name="nationality" required class="form-control">
                                                                <option selected value="">Select Country</option>
                                                                <s:if test="countryInfoList !=null">
                                                                    <s:if test="countryInfoList.size() !=0">
                                                                        <s:iterator value="countryInfoList">
                                                                            <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><strong>Passport Number *</strong></label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="passportNumber" name="passportNumber" value="<s:property value="passportNumber"/>" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Passport Number" class="form-control">
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Alternate Passport No</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="alternatePassportNo" name="alternatePassportNo" value="<s:property value="alternatePassportNo"/>" pattern="[A-Za-z-0-9]{3,20}" placeholder="Alternate Passport No" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant Name *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="applicantName" name="applicantName" value="<s:property value="applicantName"/>" pattern="[A-Za-z-0-9-. ]{3,20}" required placeholder="Applicant Name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Date Of Birth *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="dob" name="dob" value="<s:property value="dob"/>" required placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Issued Place *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="passportIssuedPlace" name="passportIssuedPlace" value="<s:property value="passportIssuedPlace"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Passport Issued Place" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Issue Date *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="passportIssueDate" name="passportIssueDate" value="<s:property value="passportIssueDate"/>" required placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Passport Expiry Date *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="passportExpiryDate" name="passportExpiryDate" value="<s:property value="passportExpiryDate"/>" required placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Gender *</label>
                                                        <div class="col-md-4">
                                                            <div class="radio-list">
                                                                <s:if test='gender=="0"'>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="male" name="gender" value="0" checked>
                                                                        Male
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="female" name="gender" value="1">
                                                                        Female
                                                                    </label>
                                                                </s:if>
                                                                <s:elseif test='gender=="1"'>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="male" name="gender" value="0">
                                                                        Male
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="female" name="gender" value="1" checked>
                                                                        Female
                                                                    </label>
                                                                </s:elseif>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Email *</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" id="applicantEmail" name="applicantEmail" value="<s:property value="applicantEmail"/>" required min="5" max="25" placeholder="Email" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Alternate Email</label>
                                                        <div class="col-sm-4">
                                                            <input type="email" id="applicantAlternateEmail" name="applicantAlternateEmail" value="<s:property value="applicantAlternateEmail"/>" min="5" max="25" placeholder="Alternate Email" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Marital Status *</label>
                                                        <div class="col-sm-4">
                                                            <select id="maritalStatus" name="maritalStatus" required class="form-control">
                                                                <s:if test='maritalStatus=="0"'>
                                                                    <option selected value="0">Single</option>
                                                                    <option value="1">Married</option>
                                                                </s:if>
                                                                <s:elseif test='maritalStatus=="1"'>
                                                                    <option value="0">Single</option>
                                                                    <option selected value="1">Married</option>
                                                                </s:elseif>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Mobile Number *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="mobileNumber" name="mobileNumber" value="<s:property value="mobileNumber"/>" pattern="[0-9]{10,15}" required placeholder="Mobile Number" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">City</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="city" name="city" value="<s:property value="city"/>" pattern="[A-Za-z-0-9 ]{3,20}" placeholder="City" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Postal Code</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="postalCode" name="postalCode" value="<s:property value="postalCode"/>" pattern="[A-Za-z-0-9]{3,6}" placeholder="Postal Code" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">State *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="state" name="state" value="<s:property value="state"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="State" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Country *</label>
                                                        <div class="col-sm-4">
                                                            <select id="country" name="country" required class="form-control">
                                                                <option selected value="">Select Country</option>
                                                                <s:if test="countryInfoList !=null">
                                                                    <s:if test="countryInfoList.size() !=0">
                                                                        <s:iterator value="countryInfoList">
                                                                            <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Resident Number</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="residentNumber" name="residentNumber" value="<s:property value="residentNumber"/>" pattern="[A-Za-z-0-9]{3,20}" placeholder="Resident Number" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Permanent Address *</label>
                                                        <div class="col-sm-4">
                                                            <textarea id="permanentAddress" name="permanentAddress" value="<s:property value="permanentAddress"/>" pattern="[A-Za-z-0-9-.-_ ]{20,200}" required placeholder="Permanent Address" class="form-control"><s:property value="permanentAddress"/></textarea>
                                                        </div>
                                                    </div>

                                                    <!-- ***************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <input type="checkbox" id="chkbSame">&nbsp;&nbsp;Check if Same as Above
                                                        </div>
                                                    </div>

                                                    <hr class="same_above"/>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><strong>Correspondence Address *</strong></label>
                                                        <div class="col-sm-4">
                                                            <textarea id="corAddress" name="corAddress" value="<s:property value="corAddress"/>" pattern="[A-Za-z-0-9-.-_ ]{20,200}" required placeholder="Correspondence Address" class="form-control"><s:property value="corAddress"/></textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Mobile Number *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="corMobileNumber" name="corMobileNumber" value="<s:property value="corMobileNumber"/>" pattern="[0-9]{10,15}" required placeholder="Mobile Number" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">City *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="corCity" name="corCity" value="<s:property value="corCity"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="City" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Postal Code *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="corPostalCode" name="corPostalCode" value="<s:property value="corPostalCode"/>" pattern="[A-Za-z-0-9]{3,6}" required placeholder="Postal Code" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">State *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="corState" name="corState" value="<s:property value="corState"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="State" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Country *</label>
                                                        <div class="col-sm-4">
                                                            <select id="corCountry" name="corCountry" required class="form-control">
                                                                <option selected value="">Select Country</option>
                                                                <s:if test="countryInfoList !=null">
                                                                    <s:if test="countryInfoList.size() !=0">
                                                                        <s:iterator value="countryInfoList">
                                                                            <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Resident Number *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="corResidentNumber" name="corResidentNumber" value="<s:property value="corResidentNumber"/>" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Resident Number" class="form-control">
                                                        </div>
                                                    </div>

                                                    <hr class="same_above"/>

                                                    <!-- *************************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><strong>Father's Name *</strong> </label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="fathersName" name="fathersName" value="<s:property value="fathersName"/>" pattern="[A-Za-z-0-9-. ]{3,20}" required placeholder="Father's Name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Father's Occupation *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="fathersOccupation" name="fathersOccupation" value="<s:property value="fathersOccupation"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Father's Occupation" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Mother's Name *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="mothersName" name="mothersName" value="<s:property value="mothersName"/>" pattern="[A-Za-z-0-9-. ]{3,20}" required placeholder="Mother's Name" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Mother's Occupation *</label>
                                                        <div class="col-sm-4">
                                                            <input  type="text" id="mothersOccupation" name="mothersOccupation" value="<s:property value="mothersOccupation"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Mother's Occupation" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Highest Qualification *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="highestQualification" name="highestQualification" value="<s:property value="highestQualification"/>" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Highest Qualification" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Year Of Passing *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="hqPassYear" name="hqPassYear" value="<s:property value="hqPassYear"/>" pattern="[0-9]{4,4}" required placeholder="Year Of Passing" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Overall Grade *</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="overAllGrade" name="overAllGrade" value="<s:property value="overAllGrade"/>" pattern="[0-9-.]{4,4}" required placeholder="Overall Grade" class="form-control">
                                                        </div>
                                                    </div>

                                                    <!-- ******************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"></label>
                                                        <div class="col-sm-4">
                                                            <input type="checkbox" id="chkbox_active_visa">&nbsp;Do you have Current Visa and Passport Active ?
                                                        </div>
                                                    </div>

                                                    <hr class="same_above"/>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Country *</label>
                                                        <div class="col-sm-4">
                                                            <select id="visaCountry" name="visaCountry" required class="form-control">
                                                                <option selected value="">Select Country</option>
                                                                <s:if test="countryInfoList !=null">
                                                                    <s:if test="countryInfoList.size() !=0">
                                                                        <s:iterator value="countryInfoList">
                                                                            <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </s:if>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Visa Expiry Date *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="visaExpiryDate" name="visaExpiryDate" required value="<s:property value="visaExpiryDate"/>" placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Release Letter Issued</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="visaReleaseLetter" name="visaReleaseLetter" value="<s:property value="visaReleaseLetter"/>" pattern="[A-Za-z-0-9 ]{5,30}" placeholder="Release Letter Issued" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Name Of College or University</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="visaColName" name="visaColName" value="<s:property value="visaColName"/>" pattern="[A-Za-z-0-9 ]{3,20}" placeholder="Name Of College or University" class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Visa Type 8</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" id="visaType" name="visaType" required value="<s:property value="visaType"/>" pattern="[A-Za-z-0-9 ]{3,20}" placeholder="Visa Type" class="form-control">
                                                        </div>
                                                    </div>

                                                    <hr class="same_above"/>

                                                    <!-- ***************************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label"><strong>Academic Qualification *</strong>  </label>
                                                        <div class="col-sm-4"> 
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">  
                                                        <div class="col-sm-7"> 
                                                            <table class="table table-bordered" id="table-items"> 
                                                                <thead> 
                                                                    <tr> 
                                                                        <th>Serial</th> 
                                                                        <th>Degree</th> 
                                                                        <th>Result</th> 
                                                                        <th>Passing Year</th> 
                                                                        <th>Institution</th> 
                                                                    </tr> 
                                                                </thead> 
                                                                <tbody> 
                                                                    <s:iterator value="eduInfoList">
                                                                        <tr class="disable"> 
                                                                            <td class="count"></td>
                                                                            <td><input type="text" id="degreeName" name="degreeName" value="<s:property value="degreeName"/>" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Degree" class="form-control"></td> 
                                                                            <td><input type="text" id="result" name="result" value="<s:property value="result"/>" pattern="[0-9-.]{4,4}" required placeholder="Result" class="form-control"></td> 
                                                                            <td><input type="text" id="passingYear" name="passingYear" value="<s:property value="passingYear"/>" pattern="[0-9]{4,4}" required placeholder="Passing Year" class="form-control"></td> 
                                                                            <td><input type="text" id="institution" name="institution" value="<s:property value="instituteName"/>" pattern="[A-Za-z-0-9]{3,30}" required placeholder="Institute name" class="form-control"></td> 
                                                                        </tr> 
                                                                    </s:iterator>
                                                                </tbody> 
                                                            </table>
                                                            <button type="button" class="btn green" id="add-row">
                                                                <i class="fa fa-plus-circle"></i>
                                                                Add Row
                                                            </button>
                                                            <button type="button" style="margin-left: 5px;" class="btn btn-danger" id="remove-row">
                                                                <i class="fa fa-minus-circle"></i>
                                                                Remove Row
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <!-- ********************************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Program Of Study *</label>
                                                        <div class="col-sm-4">  

                                                        </div>
                                                    </div>

                                                    <div class="form-group">  
                                                        <div class="col-sm-7"> 
                                                            <table class="table table-bordered table-responsive" id=""> 
                                                                <thead> 
                                                                    <tr> 
                                                                        <th style="text-align: center;" colspan="2">
                                                                            University Name
                                                                            <img src="" style="width: 20px; height: 20px;" id="createLoadingImage" alt=""/>
                                                                        </th> 
                                                                    </tr> 
                                                                </thead> 
                                                                <tbody> 
                                                                    <s:if test="varsityInfoList !=null">
                                                                        <s:if test="varsityInfoList.size() !=0">
                                                                            <s:iterator value="varsityInfoList">
                                                                                <tr> 
                                                                                    <td style="text-align: center;" class="count"></td>
                                                                                    <td>
                                                                                        <s:iterator value="appVarsityInfoList">
                                                                                            <s:if test="varsityID==varsity.varsityID">
                                                                                                <input checked type="checkbox" id="<s:property value="varsityID"/>" name="varsityID" value="<s:property value="varsityID"/>" class="chkbVarId">
                                                                                                <label style="margin-left: 10px;" for="varsityID"><s:property value="varsityName"/>&nbsp;&nbsp;<s:if test="varsityStatus!='Y'">(Inactive)</s:if></label>
                                                                                            </s:if>
                                                                                            <s:else>
                                                                                                <input type="checkbox" id="<s:property value="varsityID"/>" name="varsityID" value="<s:property value="varsityID"/>" class="chkbVarId">
                                                                                                <label style="margin-left: 10px;" for="varsityID"><s:property value="varsityName"/>&nbsp;&nbsp;<s:if test="varsityStatus!='Y'">(Inactive)</s:if></label>
                                                                                            </s:else>
                                                                                        </s:iterator>
                                                                                    </td>
                                                                                </tr> 
                                                                            </s:iterator>
                                                                        </s:if>
                                                                    </s:if>                                            
                                                                </tbody> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- ****************************************** -->
                                                    <div class="form-group">
                                                        <div id="divProName" class="col-sm-7" style="">  
                                                            <table class="table table-bordered table-responsive" id="program"> 
                                                                <thead>
                                                                    <tr >
                                                                        <th style="text-align: center;" colspan="3">Selected Program Name *</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th style="text-align: center;" colspan="2" scope="col">Course Name</th>
                                                                        <th style="text-align: center;" scope="col">University Name</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="selected-courses">
                                                                    <s:if test="appCoursesInfoList !=null">
                                                                        <s:if test="appCoursesInfoList.size() !=0">
                                                                            <s:iterator value="appCoursesInfoList">
                                                                                <tr class="<s:property value="varsity.varsityID"/>">
                                                                                    <td style="text-align: center;" class="count"></td>
                                                                                    <td>
                                                                                        <span title="">
                                                                                            <input type="hidden" id="varid" name="varid" value="<s:property value="varsity.varsityID"/>">
                                                                                            <input type="checkbox" id="courseID" name="courseID" checked value="<s:property value="varsity.courseID"/>">
                                                                                            <label style="margin-left: 10px;" for="">
                                                                                                <s:property value="varsity.courseName"/>
                                                                                                &nbsp;&nbsp;
                                                                                                <s:if test="varsity.courseStatus!='Y'">
                                                                                                    (Inactive)
                                                                                                </s:if>
                                                                                            </label>
                                                                                        </span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <label style="margin-left: 10px;" for=""><s:property value="varsity.varsityName"/></label>
                                                                                    </td>
                                                                                </tr>
                                                                            </s:iterator>
                                                                        </s:if>
                                                                    </s:if>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!-- ********************************************************** -->
                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Arrival Date *</label>
                                                        <div class="col-sm-4">
                                                            <div class="input-group">
                                                                <input type="text" id="arrivalDate" name="arrivalDate" value="<s:property value="arrivalDate"/>" required placeholder="YYYY-MM-DD" class="form-control">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="panel-footer">
                                                    <div class="row">
                                                        <div class="col-sm-9 col-sm-offset-3">
                                                            <button type="submit" id="btn_change" name="btn_change" class="btn btn-primary btn_change">
                                                                Save Change
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/jquery-confirm.css">
        <script type="text/javascript" src="<%= request.getContextPath()%>/js/jquery-confirm.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".btn_change").on('click', function () {
                    $.confirm({
                        icon: 'fa fa-warning',
                        title: 'Confirmation !',
                        content: 'Do you want to add new applicant ?',
                        type: 'green',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Sure',
                                btnClass: 'btn-green',
                                action: function () {
                                }
                            },
                            close: {
                                text: 'No',
                                btnClass: 'btn-red',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                var date_dob = $('input[name="dob"]');
                var date_passport_issue = $('input[name="passportIssueDate"]');
                var date_passport_expiry = $('input[name="passportExpiryDate"]');
                var date_visa_expiry = $('input[name="visaExpiryDate"]');
                var date_arrival = $('input[name="arrivalDate"]');

                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };

                date_dob.datepicker(options);
                date_passport_issue.datepicker(options);
                date_passport_expiry.datepicker(options);
                date_visa_expiry.datepicker(options);
                date_arrival.datepicker(options);

                $("#chkbSame").click(function () {
                    if ($(this).is(":checked")) {
                        $("#corAddress").val($("#permanentAddress").val());
                        $("#corCity").val($("#city").val());
                        $("#corPostalCode").val($("#postalCode").val());
                        $("#corState").val($("#state").val());
                        $("#corCountry").val($("#country").val());
                        $("#corMobileNumber").val($("#mobileNumber").val());
                        $("#corResidentNumber").val($("#residentNumber").val());
                    } else {
                        $("#corAddress").val('');
                        $("#corCity").val('');
                        $("#corPostalCode").val('');
                        $("#corState").val('');
                        $("#corCountry").val('');
                        $("#corMobileNumber").val('');
                        $("#corResidentNumber").val('');
                    }
                });

                var remove_row = $('#remove-row');
                remove_row.hide();

                remove_row.on('click', function () {
                    $("#table-items tr.info").fadeOut(300, function () {
                        $(this).remove();
                    });
                });

                $('#add-row').on('click', function () {
                    $("#table-items").find('tbody')
                            .append('<tr>\n\
                                        <td class="count"></td>\n\
                                        <td><input type="text" id="degreeName" name="degreeName" pattern="[A-Za-z-0-9]{3,20}" required placeholder="Name of Degree" class="form-control item"></td>\n\
                                        <td><input type="text" id="result" name="result" pattern="[0-9-.]{4,4}" required placeholder="Result" class="form-control item"></td>\n\
                                        <td><input type="text" id="passingYear" name="passingYear" pattern="[0-9]{4,4}" required placeholder="Passing Year" class="form-control item"></td>\n\
                                        <td><input type="text" id="institution" name="institution" pattern="[A-Za-z-0-9]{3,30}" required placeholder="Institute name" class="form-control item"></td>\n\
                                    </tr>');
                });

                $('#table-items').on('click', '.item', function () {
                    $("tr").removeClass("info");
                    $(this).closest('tr').addClass("info");
                    remove_row.show();
                });

                $(".chkbVarId").click(function () {

                    var varID = this.id;

                    if ($(this).is(":checked")) {
                        document.getElementById('createLoadingImage').src = 'images/load.gif';
                        $.post('GetVarsityCourses', {
                            universityID: varID
                        }).done(function (data) {
                            document.getElementById('createLoadingImage').src = '';
                            $("#selected-courses").append(data);
                        });
                    } else if ($(this).not(":checked")) {
                        var id = this.id;
                        $('#program > tbody').each(function () {
                            $("#program tr." + id + "").fadeOut(300, function () {
                                $(this).remove();
                            });
                        });
                    }
                });

                $('#agentId').select2().on("change", function (e) {
                });
            });
        </script>

    </body>
</html>
