<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            #issueTable  {
                margin-top: 5px;
                margin-bottom: 5px;
                border-top:1px solid #eee;
                border-right:1px solid #eee;
                border-collapse:collapse;
            }

            #issueTable  td {
                padding: 7px;
                border-bottom:1px solid #eee;
                border-left:1px solid #eee;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>

        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<%= request.getContextPath()%>/css/bootstrap3-wysihtml5.min.css">
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Issue History</h3>
                                </div> 

                                <s:if test="issuesInfosList.size() !=0">
                                    <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                        <table cellpadding="10" id="issueTable">
                                            <thead>
                                                <tr>
                                                    <th>Issue ID</th>
                                                    <th>Issue Title</th>
                                                    <th>Issue Details</th>
                                                    <th>Issued By</th>
                                                    <th>Issued Date</th>
                                                    <th>Issued Status</th>
                                                    <th>Comment</th>
                                                    <th>Comment By</th>
                                                    <th>Comment Date</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <s:iterator value="issuesInfosList">
                                                    <tr>
                                                        <td><s:property value="issuesId"/></td>
                                                        <td><s:property value="issuesTitle"/> </td>
                                                        <td><s:property value="issuesDetails"/> </td>
                                                        <td><s:property value="userInfo.userFullName"/></td>
                                                        <td><s:property value="issuesInsertDate"/></td>
                                                        <td>
                                                            <s:if test="issuesStatus =='Y'">
                                                                Resolved
                                                            </s:if>
                                                            <s:else>
                                                                Pending
                                                            </s:else>
                                                        </td>
                                                        <td>
                                                            <s:if test="commentDetails !=null">
                                                                <s:property value="commentDetails"/>
                                                            </s:if>
                                                            <s:else>
                                                                No Comment
                                                            </s:else>
                                                        </td>
                                                        <td><s:property value="userInfo.userFullName"/></td>
                                                        <td><s:property value="commentDate"/></td>
                                                    </tr>
                                                </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr style="margin: 0;">
                                </s:if>

                                <div class="form-horizontal">
                                    <s:if test="singleAppInfoList !=null">
                                        <s:if test="singleAppInfoList.size() !=0">
                                            <s:iterator value="singleAppInfoList">
                                                <div class="panel-body">

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label" for="demo-is-inputsmall">Agent Name</label>
                                                        <div class="col-sm-4"> 
                                                            <s:property value="userInfo.userFullName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant Name</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="applicantName"/>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">Applicant ID</label>
                                                        <div class="col-sm-4">
                                                            <s:property value="applicantID"/>
                                                        </div>
                                                    </div>

                                                    <form id="from-issues" action="AddIssues" method="POST" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label"></label>
                                                            <div class="col-sm-4">
                                                                <label>Add Issue</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Issue Title</label>
                                                            <div class="col-sm-6">
                                                                <input type="text" id="issueTitle" name="issueTitle" pattern="[A-Za-z-0-9-.-_ ]{3,60}" required class="form-control">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Issue Details</label>
                                                            <div class="col-sm-9">
                                                                <textarea required id="issueDetails" name="issueDetails" class="textarea" placeholder="Message" style="width: 91%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="panel-footer">
                                                            <div class="row">
                                                                <div class="col-sm-9 col-sm-offset-3">
                                                                    <input type="hidden" id="applicantID" name="applicantID" value="<s:property value="applicantID"/>">
                                                                    <input type="hidden" id="agentId" name="agentId" value="<s:property value="userInfo.userID"/>">
                                                                    <button type="submit" id="btn_issue" name="btn_issue" class="btn btn-primary btn_issue">
                                                                        Add Issue
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                    <form id="from-comment" action="CommentOnIssues" method="POST" class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label"></label>
                                                            <div class="col-sm-4">
                                                                <label>Add Comment</label>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Issue</label>
                                                            <div class="col-sm-4">
                                                                <select id="issueId" name="issueId" data-placeholder="Select Issue" required class="form-control drop-select">
                                                                    <option value=""></option>
                                                                    <s:if test="issuesInfosList.size() !=0">
                                                                        <s:iterator value="issuesInfosList">
                                                                            <option value="<s:property value="issuesId"/>"><s:property value="issuesTitle"/></option>
                                                                        </s:iterator>
                                                                    </s:if>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">Comment</label>
                                                            <div class="col-sm-9">
                                                                <textarea required id="commentDetails" name="commentDetails" class="textarea" placeholder="Comment" style="width: 91%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="panel-footer">
                                                            <div class="row">
                                                                <div class="col-sm-9 col-sm-offset-3">
                                                                    <button type="submit" id="btn_comment" name="btn_comment" class="btn btn-primary btn_comment">
                                                                        Add Comment
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </s:iterator>
                                        </s:if>
                                    </s:if>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <!-- jQuery 2.2.3 -->
        <script src="<%= request.getContextPath()%>/js/jquery-2.2.3.min.js"></script>

        <!-- jQuery UI 1.11.4 -->
        <script src="<%= request.getContextPath()%>/js/jquery-ui.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $(".btn_change").on('click', function () {
                    $.confirm({
                        icon: 'fa fa-warning',
                        title: 'Confirmation !',
                        content: 'Do you want to add new applicant ?',
                        type: 'green',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Sure',
                                btnClass: 'btn-green',
                                action: function () {
                                }
                            },
                            close: {
                                text: 'No',
                                btnClass: 'btn-red',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                $('#commentOnIssue').select2().on("change", function (e) {
                });
            });
        </script>

        <!-- Bootstrap 3.3.6 -->
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>

        <!-- Bootstrap WYSIHTML5 -->
        <script src="<%= request.getContextPath()%>/js/bootstrap3-wysihtml5.all.min.js"></script>

        <script src="<%= request.getContextPath()%>/js/dashboard.js"></script>

    </body>
</html>
