<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="applicantInfoList.size() !=0">
    <label class="col-md-4 control-label">Applicant ID</label>
    <div class="col-md-6">
        <select id="applicantID" name="applicantID" required data-placeholder="Select Applicant ID" class="form-control drop-select">
            <option selected value=""></option>
            <s:iterator value="applicantInfoList">
                <option value="<s:property value="applicantID"/>"><s:property value="applicantID"/>&nbsp;<s:if test='applicantType=="1"'>Student</s:if><s:else>Worker</s:else></option>
            </s:iterator>
        </select>
    </div>
</s:if>
<s:else>
    <label class="col-md-4 control-label">Applicant ID</label>
    <div class="col-md-6">
        <select id="applicantID" name="applicantID" required data-placeholder="Select Applicant ID" class="form-control drop-select">
            <option selected value=""></option>
            <option value="" selected="selected">You have no applicant</option>
        </select>
    </div>
</s:else>
<script type="text/javascript">
    $(document).ready(function () {
        $('#applicantID').select2().on("change", function (e) {
            getServiceByApplicant();
//                    updateCurrentPay();
        });
    });
</script>