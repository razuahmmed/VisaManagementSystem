<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <!-- THIS CSS FOR DATA TABLE -->
        <link href="<%= request.getContextPath()%>/css/dataTables.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/css/jquery-confirm.css" rel="stylesheet" type="text/css">

        <style type="text/css">

            .input-inline {
                display: inline-block;
                width: auto;
                vertical-align: middle;
            }

            .blue.btn {
                color: #fff;
                background-color: #4b8df8;
                color: #FFF;
            }

            .btn {
                border-width: 0;
                padding: 7px 14px;
                font-size: 12px;
                outline: none;
                background-image: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                text-shadow: none;
            }

            .yellow.btn {
                color: #fff;
                background-color: #ffb848;
            }

            .red.btn {
                color: #fff;
                background-color: #d84a38;
            }

            .table {
                counter-reset:section;
            }

            .count:before {
                counter-increment:section;
                content:counter(section);
            }

            hr {
                margin-top: 0;
                margin-bottom: 0;
            }

            /*            
            .table-scrollable {
                width: 100%;
                max-height: 630px;
                overflow-x: auto;
                overflow-y: auto;
                margin: 10px 0 !important;
             }
            */

            /*            .
            margin-bottom-5, .btn-editable {
                margin-bottom: 5px;
            }
            */

        </style>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Student History</h3>
                                </div> 

                                <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="3%">SL</th>
                                                <th width="12%">Applicant ID<hr>Applicant Name</th>
                                        <th width="10%">Date of Birth<hr>Phone</th>
                                        <th width="7%">Email<hr>Passport</th>
                                        <th width="12%">Agent<hr>Visa Expiry Date</th>
                                        <th width="30%">Applied Institution<hr>Status</th>
                                        <th width="12%">Actions</th>
                                        </tr>
                                        <tr style="text-align: center;" class="filter">
                                            <td></td>
                                            <td><input type="text" id="applicantId" name="applicantId" class="form-control form-filter input-sm"></td>
                                            <td><input type="text" id="applicantName" name="applicantName" class="form-control form-filter input-sm"></td>
                                            <td><input type="text" id="applicantPhone" name="applicantPhone" class="form-control form-filter input-sm"></td>
                                            <td><input type="text" id="agentName" name="agentName" class="form-control form-filter input-sm"></td>
                                            <td></td>
                                            <td style="width: 90px;">
                                                <button class="btn btn-sm yellow filter-submit margin-bottom" data-toggle="tooltip" data-placement="top" title="Search Button"><i class="fa fa-search"></i></button>
                                                <button class="btn btn-sm red filter-cancel" data-toggle="tooltip" data-placement="top" title="Reset Button"><i class="icon-close"></i></button>
                                            </td>
                                        </tr>
                                        </thead>
                                        <tbody id="tbody_applicant">
                                            <s:if test="applicantInfoList !=null">
                                                <s:if test="applicantInfoList.size() !=0">
                                                    <s:iterator value="applicantInfoList">
                                                        <tr style="text-align: center;">
                                                            <td class="count"></td>
                                                            <td style="text-align: left;"><s:property value="applicantID"/><hr><s:property value="applicantName"/></td>
                                                            <td style="text-align: left;"><s:property value="dob"/><hr><s:property value="mobileNumber"/></td>
                                                            <td style="text-align: left;"><s:property value="applicantEmail"/><hr><s:property value="passportNumber"/></td>
                                                            <td style="text-align: left;"><s:property value="userInfo.userFullName"/><hr><s:property value="visaExpiryDate"/></td>
                                                            <td style="text-align: left;">
                                                                <s:iterator value="appVarsityInfoList">
                                                                    <s:property value="varsity.varsityName"/> |
                                                                </s:iterator>
                                                                (<s:iterator value="appCoursesInfoList">
                                                                    <s:property value="varsity.courseName"/> |
                                                                </s:iterator>)
                                                                <hr>
                                                                Proved : 
                                                                <hr>
                                                                Departure Date : 
                                                                <hr>
                                                                Arrival Date : 
                                                                <hr>
                                                                Airlines Name : 
                                                                <hr>
                                                                Flight No : 
                                                                <hr>
                                                                Due :  <s:iterator value="paymentInfo">Tuition : <s:property value="tuitionFeeDue"/> &AMP; Others : <s:property value="othersAmountDue"/></s:iterator>
                                                                </td>
                                                                <td>
                                                                    <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View Details">
                                                                    <i class="fa fa-list"></i>
                                                                </a>
                                                                <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs purple btn-danger btn-document" data-toggle="tooltip" data-placement="top" title="Document">
                                                                    <i class="fa fa-book"></i>
                                                                </a>
                                                                <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs purple btn-danger btn-issues" data-toggle="tooltip" data-placement="top" title="Issues">
                                                                    <i class="fa fa-info"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- JQUERY FOR DATA TABLE -->
        <script src="<%= request.getContextPath()%>/js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/dataTables.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js/jquery-confirm.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip();
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers"
                });

                $('.btn-view').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    if (id != undefined && id != null) {
                        window.location = 'ViewStudent?applicantID=' + id;
                    }
                });

                $('.btn-document').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    if (id != undefined && id != null) {
                        window.location = 'ApplicantDocument?applicantID=' + id;
                    }
                });

                $('.btn-issues').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    if (id != undefined && id != null) {
                        window.location = 'ApplicantIssues?applicantID=' + id;
                    }
                });

                $(".btn-delete").on('click', function (e) {
                    e.preventDefault();
                    var appId = this.id;
                    $.confirm({
                        icon: 'fa fa-question',
                        title: 'Confirmation !',
                        content: 'Do you want to delete this applicant ?',
                        type: 'green',
                        backgroundDismiss: true,
                        closeIcon: true,
                        typeAnimated: true,
                        buttons: {
                            tryAgain: {
                                text: 'Yes',
                                btnClass: 'btn-green',
                                action: function () {
                                    window.location = 'DeleteApplicant?applicantID=' + appId;
                                }
                            },
                            close: {
                                text: 'No',
                                btnClass: 'btn-red',
                                action: function () {
                                }
                            }
                        }
                    });
                });

                var fName = '';
                var fValue = '';
                var tbody = $("#tbody_applicant");
                var search = $(".filter-submit");
                search.on('click', function (e) {
                    e.preventDefault();
                    if ($("#applicantId").val() != '') {
                        fName = $("#applicantId").attr("name");
                        fValue = $("#applicantId").val();
                    } else if ($("#applicantName").val() != '') {
                        fName = $("#applicantName").attr("name");
                        fValue = $("#applicantName").val();
                    } else if ($("#applicantPhone").val() != '') {
                        fName = $("#applicantPhone").attr("name");
                        fValue = $("#applicantPhone").val();
                    } else if ($("#applicantCountry").val() != '') {
                        fName = $("#applicantCountry").attr("name");
                        fValue = $("#applicantCountry").val();
                    } else if ($("#agentName").val() != '') {
                        fName = $("#agentName").attr("name");
                        fValue = $("#agentName").val();
                    }
                    var dataString = 'fieldName=' + fName;
                    dataString += '&fieldValue=' + fValue;
                    $.post('SearchApplicant', dataString).done(function (data) {
                        tbody.html(data);
                    });
                });

                $('.filter-cancel').on('click', function (e) {
                    e.preventDefault();
                    $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                        $(this).val("");
                    });
                    setTimeout(location.reload(), 0);
                });
            });
        </script>
    </body>
</html>
