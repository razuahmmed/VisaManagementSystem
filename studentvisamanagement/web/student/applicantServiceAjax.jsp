<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="applicantInfoList.size() !=0">
    <label class="col-md-4 control-label" for="demo-is-inputsmall">Service Type</label>
    <div class="col-md-6"> 
        <select id="serviceId" name="serviceId" required data-placeholder="Select Service Type" class="form-control drop-select">
            <option selected value=""></option>
            <s:iterator value="applicantInfoList">
                <option value="<s:property value="serviceInfo.serviceID"/>"><s:property value="serviceInfo.serviceName"/></option>
            </s:iterator>
        </select>
        <img id="createLoadingImage" src="" alt="" />
    </div>
</s:if>
<s:else>
    <label class="col-md-4 control-label" for="demo-is-inputsmall">Service Type</label>
    <div class="col-md-6"> 
        <select id="serviceId" name="serviceId" required data-placeholder="Select Service Type" class="form-control drop-select">
            <option selected value=""></option>
            <option value="" selected="selected">You have no service</option>
        </select>
    </div>
</s:else>
<script type="text/javascript">
    $(document).ready(function () {
        $('#serviceId').select2().on("click", function (e) {
            updateCurrentPay();
        });
    });
</script>