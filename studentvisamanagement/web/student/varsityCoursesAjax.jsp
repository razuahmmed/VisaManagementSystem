<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="varsityCoursesInfoList !=null">
    <s:if test="varsityCoursesInfoList.size() !=0">
        <s:iterator value="varsityCoursesInfoList">
            <s:iterator value="coursesList">
                <tr class="<s:property value="varsityID"/>">
                    <td style="text-align: center;" class="count"></td>
                    <td>
                        <span title="">
                            <input type="hidden" id="varid" name="varid" value="<s:property value="varsityID"/>">
                            <input type="checkbox" id="courseID" name="courseID" checked value="<s:property value="courseID"/>">
                            <label style="margin-left: 10px;" for=""><s:property value="courseName"/></label>
                        </span>
                    </td>
                    <td>
                        <label style="margin-left: 10px;" for=""><s:property value="varsityName"/></label>
                    </td>
                </tr>
            </s:iterator>
        </s:iterator>
    </s:if>
</s:if>