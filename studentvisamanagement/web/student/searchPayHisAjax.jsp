<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="payHisInfoList !=null">
    <s:if test="payHisInfoList.size() !=0">
        <s:iterator value="payHisInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="applicantInfo.applicantName"/></td>
                <td align="center"><s:property value="userInfo.userFullName"/></td>
                <td><s:property value="paymentDate"/></td>
                <td><s:property value="paymentMode"/></td>
                <td>
                    <s:if test='currency=="1"'>
                        MYR
                    </s:if>
                    <s:elseif test='currency=="2"'>
                        BDT
                    </s:elseif>
                </td>
                <td><s:property value="amount"/></td>
                <td><s:property value="bankName"/></td>
                <td><s:property value="chequeDate"/></td>
                <td><s:property value="chequeReference"/></td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="paymentHistoryId"/>" class="btn btn-xs blue btn-editable" data-toggle="tooltip" data-placement="top" title="View Button">
                        <i class="icon-list"></i>
                    </a>
                    <a href="javascript:void(0);" id="<s:property value="paymentHistoryId"/>" class="btn btn-xs purple btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Button">
                        <i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>