<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <style type="text/css">
            table th, tr, td {
                padding: 5px;
                margin: 0px;
                border: #eee 1px solid;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Applicant Payment Receive</h3>
                                </div> 

                                <form action="ApplicantPaymentReceive" method="POST" class="form-horizontal">
                                    <div class="panel-body">
                                        <div class="col-md-6">

                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="demo-is-inputsmall">Agent</label>
                                                <div class="col-md-6"> 
                                                    <select id="agentId" name="agentId" required data-placeholder="Select Agent" class="form-control drop-select">
                                                        <option selected value=""></option>
                                                        <s:if test="agentInfoList !=null">
                                                            <s:if test="agentInfoList.size() !=0">
                                                                <s:iterator value="agentInfoList">
                                                                    <option value="<s:property value="userID"/>"><s:property value="userFullName"/>&nbsp;&nbsp;<s:if test="userStatus!='Y'">(Inactive Agent)</s:if></option>
                                                                </s:iterator>
                                                            </s:if>
                                                        </s:if>
                                                    </select>
                                                </div>
                                            </div>

                                            <div id="app_div" class="form-group">
                                                <label class="col-md-4 control-label">Applicant ID</label>
                                                <div class="col-md-6">
                                                    <select id="applicantID" name="applicantID" required data-placeholder="Select Applicant ID" class="form-control drop-select">
                                                        <option selected value=""></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div id="serv_div" class="form-group">
                                                <label class="col-md-4 control-label" for="demo-is-inputsmall">Service Type</label>
                                                <div class="col-md-6"> 
                                                    <select id="serviceId" name="serviceId" required data-placeholder="Select Service Type" class="form-control drop-select">
                                                        <option selected value=""></option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Payment Mode</label>
                                                <div class="col-md-6">
                                                    <div class="radio-list">
                                                        <label class="radio-inline">
                                                            <input type="radio" id="cash" name="paymentMode" value="Cash" checked>
                                                            Cash
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" id="bank" name="paymentMode" value="Bank">
                                                            Bank
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Payment Date</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="paymentDate" name="paymentDate" required placeholder="YYYY-MM-DD" class="form-control">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Amount</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="amount" name="amount" pattern="[0-9]{3,10}" required placeholder="Amount" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Currency</label>
                                                <div class="col-md-6">
                                                    <select id="currency" name="currency" required data-placeholder="Select Currency" class="form-control drop-select">
                                                        <option selected value=""></option>
                                                        <option value="1">MYR</option>
                                                        <option value="2">BDT</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div id="conv-div" class="form-group">
                                                <label class="col-md-4 control-label">Conversion Rate</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="conversionRate" name="conversionRate" pattern="[0-9]{1,3}" placeholder="Conversion Rate" class="form-control">
                                                </div>
                                            </div>

                                            <div class="bankMode">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Bank Name</label>
                                                    <div class="col-md-6">
                                                        <input type="text" id="bankName" name="bankName" pattern="[A-Za-z-0-9]{3,20}" placeholder="Bank Name" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Cheque Reference</label>
                                                    <div class="col-md-6">
                                                        <input type="text" id="chequeReference" name="chequeReference" pattern="[A-Za-z-0-9]{3,20}" placeholder="Cheque Reference" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Cheque Date</label>
                                                    <div class="col-md-6">
                                                        <div class="input-group">
                                                            <input type="text" id="chequeDate" name="chequeDate" placeholder="YYYY-MM-DD" class="form-control">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6" id="pay-div">

                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input type="submit" name="submit" value="Submit" class="btn btn-primary"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <script src="<%= request.getContextPath()%>/js/jqueryLibrary.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.pogo-slider.js"></script>
        <script src="<%= request.getContextPath()%>/js/scripts.js"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('.bankMode').hide();
                $("input[name=paymentMode]").change(function () {
                    $('.bankMode').toggle();
                    if ($("#bank").is(':checked')) {
                        $('#bankName').attr('required', true);
                        $('#chequeReference').attr('required', true);
                        $('#chequeDate').attr('required', true);
                    } else {
                        $('#bankName').attr('required', false);
                        $('#chequeReference').attr('required', false);
                        $('#chequeDate').attr('required', false);
                    }
                });

                $('#conv-div').hide();
                $("#currency").change(function () {
                    if ($("#currency").val() == 1) {
                        $('#conv-div').hide();
                        $('#conversionRate').attr('required', false);
                    } else {
                        $('#conv-div').show();
                        $('#conversionRate').attr('required', true);
                    }
                });

                var payment_date = $('input[name="paymentDate"]');
                var cheque_date = $('input[name="chequeDate"]');

                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };

                payment_date.datepicker(options);
                cheque_date.datepicker(options);

                updateCurrentPay();

                $('#agentId').select2().on("change", function (e) {
                    getApplicant();
//                    updateCurrentPay();
                });

                $('#serviceId').select2();

                $('#applicantID').select2();

                $('#currency').select2();
            });

            function  updateCurrentPay() {

                var srid = $('#serviceId').val();
                var agid = $('#agentId').val();
                var appid = $('#applicantID').val();

                if (srid != "" && agid != "" && appid != "") {
                    document.getElementById('createLoadingImage').src = 'images/load.gif';
                    $.post('GetCurrentPayment', {
                        serviceId: srid,
                        agentId: agid,
                        applicantID: appid
                    }).done(function (data) {
                        document.getElementById('createLoadingImage').src = '';
                        $("#pay-div").html(data);
                    });
                }
            }

            function  getApplicant() {
                var agid = $('#agentId').val();
                if (agid !== "") {
                    $.post("GetApplicantByAgent", {agentId: agid}, function (data) {
                        $("#app_div").html(data);
                    });
                }
            }

            function  getServiceByApplicant() {
                var appid = $('#applicantID').val();
                if (appid !== "") {
                    $.post("GetServiceByApplicant", {applicantID: appid}, function (data) {
                        $("#serv_div").html(data);
                    });
                }
            }
        </script>
    </body>
</html>
