<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<s:if test="applicantInfoList !=null">
    <s:if test="applicantInfoList.size() !=0">
        <s:iterator value="applicantInfoList">
            <tr style="text-align: center;">
                <td class="count"></td>
                <td style="text-align: left;"><s:property value="applicantID"/><hr><s:property value="applicantName"/></td>
                <td style="text-align: left;"><s:property value="dob"/><hr><s:property value="mobileNumber"/></td>
                <td style="text-align: left;"><s:property value="applicantEmail"/><hr><s:property value="passportNumber"/></td>
                <td style="text-align: left;"><s:property value="userInfo.userFullName"/><hr><s:property value="visaExpiryDate"/></td>
                <td style="text-align: left;">
                    <s:iterator value="appVarsityInfoList">
                        <s:property value="varsity.varsityName"/> |
                    </s:iterator>
                    (<s:iterator value="appCoursesInfoList">
                        <s:property value="varsity.courseName"/> |
                    </s:iterator>)
                    <hr>
                    Proved : 
                    <hr>
                    Departure Date : 
                    <hr>
                    Arrival Date : 
                    <hr>
                    Airlines Name : 
                    <hr>
                    Flight No : 
                    <hr>
                    Due :  <s:iterator value="paymentInfo">Tuition : <s:property value="tuitionFeeDue"/> &AMP; Others : <s:property value="othersAmountDue"/></s:iterator>
                    </td>
                    <td>
                        <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View Details">
                        <i class="fa fa-list"></i>
                    </a>
                    <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs purple btn-danger btn-document" data-toggle="tooltip" data-placement="top" title="Document">
                        <i class="fa fa-book"></i>
                    </a>
                    <a href="javascript:void(0);" id="<s:property value="applicantID"/>" class="btn btn-xs purple btn-danger btn-issues" data-toggle="tooltip" data-placement="top" title="Issues">
                        <i class="fa fa-info"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>