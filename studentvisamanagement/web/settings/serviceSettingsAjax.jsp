<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="serviceInfoList !=null">
    <s:if test="serviceInfoList.size() !=0">
        <s:iterator value="serviceInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Service Name - <s:property value="serviceName"/></h4>
            </div>
            <form id="form" action="EditService" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN ALERTS PORTLET-->
                            <div class="portlet green box">
                                <div class="portlet-body">
                                    <h4>Service Details of <s:property value="serviceName"/></h4>
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Service Name *</td>
                                                    <td>
                                                        <input type="hidden" id="serviceId" name="serviceId" value="<s:property value="serviceID"/>">
                                                        <input type="text" id="serviceName" name="serviceName" pattern="[A-Za-z-0-9 ]{3,20}" required value="<s:property value="serviceName"/>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Amount *</td>
                                                    <td><input type="text" id="serviceAmount" name="serviceAmount" pattern="[0-9]{2,7}" required value="<s:property value="serviceAmount"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Currency</td>
                                                    <td>MYR</td>
                                                </tr>
                                                <tr>
                                                    <td>Status *</td>
                                                    <td>
                                                        <select id="serviceStatus" name="serviceStatus" required class="">
                                                            <s:if test="serviceStatus=='Y'">
                                                                <option selected value="Y">Active</option>
                                                                <option value="N">Inactive</option>
                                                            </s:if>
                                                            <s:else>
                                                                <option value="Y">Active</option>
                                                                <option selected value="N">Inactive</option>
                                                            </s:else>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <button style="font-size: 14px;" type="reset" class="btn red">Reset</button>
                                                        <button style="margin-right: 5px; font-size: 14px;" type="submit" class="btn blue">Save Change</button>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END ALERTS PORTLET-->
                        </div>
                    </div>
                </div>
            </form>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>
