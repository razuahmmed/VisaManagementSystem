<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/simple-line-icons.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR DATA TABLE -->
        <link href="<%= request.getContextPath()%>/css/dataTables.css" rel="stylesheet" type="text/css"/>

        <link href="<%= request.getContextPath()%>/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link href="<%= request.getContextPath()%>/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">

            .input-inline {
                display: inline-block;
                width: auto;
                vertical-align: middle;
            }

            .blue.btn {
                color: #fff;
                background-color: #4b8df8;
                color: #FFF;
            }

            .btn {
                border-width: 0;
                padding: 7px 14px;
                font-size: 12px;
                outline: none;
                background-image: none;
                filter: none;
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
                text-shadow: none;
            }

            .yellow.btn {
                color: #fff;
                background-color: #ffb848;
            }

            .red.btn {
                color: #fff;
                background-color: #d84a38;
            }

            /*            
            .table-scrollable {
                width: 100%;
                max-height: 630px;
                overflow-x: auto;
                overflow-y: auto;
                margin: 10px 0 !important;
             }
            */

            /*            .
            margin-bottom-5, .btn-editable {
                margin-bottom: 5px;
            }
            */

        </style>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Student Service History</h3>
                                </div> 

                                <div style="border-top: 1px solid #ddd;" class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover" id="datatable_orders">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th width="14%">Service Name</th>
                                                <th width="9%">Service Amount</th>
                                                <th width="9%">Service Currency</th>
                                                <th width="8%">Service Status</th>
                                                <th width="9%">Actions</th>
                                            </tr>
                                            <tr style="text-align: center;" class="filter">
                                                <td><input type="text" id="serviceName" name="serviceName" class="form-control form-filter input-sm"></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <select id="serviceStatus" name="serviceStatus" class="form-control form-filter input-sm">
                                                        <option value="">Select Status</option>
                                                        <option value="Y">Active</option>
                                                        <option value="N">Inactive</option>
                                                    </select>
                                                </td>
                                                <td style="width: 90px;">
                                                    <button class="btn btn-sm yellow filter-submit margin-bottom" data-toggle="tooltip" data-placement="top" title="Search Button"><i class="icon-magnifier"></i></button>
                                                    <button class="btn btn-sm red filter-cancel" data-toggle="tooltip" data-placement="top" title="Reset Button"><i class="icon-close"></i></button>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_service_history">
                                            <s:if test="serviceInfoList !=null">
                                                <s:if test="serviceInfoList.size() !=0">
                                                    <s:iterator value="serviceInfoList">
                                                        <tr style="text-align: center;">
                                                            <td align="center"><s:property value="serviceName"/></td>
                                                            <td align="center"><s:property value="serviceAmount"/></td>
                                                            <td align="center">MYR</td>
                                                            <td>
                                                                <s:if test="serviceStatus == 'Y' ">
                                                                    Active
                                                                </s:if>
                                                                <s:else>
                                                                    <div style="color: red;">
                                                                        Inactive
                                                                    </div>
                                                                </s:else>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0);" id="<s:property value="serviceID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                                                                    <i class="icon-pencil"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </s:iterator>
                                                </s:if>
                                            </s:if>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th colspan="1" style="text-align: right;">Total</th>
                                                <th colspan="4" style="text-align: left;"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- Modal -->
            <div id="service-modal" class="modal fade" tabindex="-1">

            </div>

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- JQUERY FOR DATA TABLE -->
        <script src="<%= request.getContextPath()%>/js/jquery_002.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/dataTables.js" type="text/javascript"></script>

        <script src="<%= request.getContextPath()%>/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap-modal.js" type="text/javascript"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                $('[data-toggle="tooltip"]').tooltip();
                $('#datatable_orders').DataTable({
                    "order": [[1, "asc"]],
                    "orderCellsTop": true,
                    "searching": false,
                    "pagingType": "full_numbers",
                    "footerCallback": function (row, data, start, end, display) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function (i) {
                            return typeof i === 'string' ?
                                    i.replace(/[\$,]/g, '') * 1 :
                                    typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        total = api
                                .column(1)
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Total over this page
                        pageTotal = api
                                .column(1, {page: 'current'})
                                .data()
                                .reduce(function (a, b) {
                                    return intVal(a) + intVal(b);
                                }, 0);

                        // Update footer
                        $(api.column(1).footer()).html(
                                '' + pageTotal + ' (Grand Total : ' + total + ')'
                                );
                    }
                });

                $('.btn-view').on("click", function (e) {
                    e.preventDefault();
                    var id = this.id;
                    $('body').modalmanager('loading');
                    setTimeout(function () {
                        $('#service-modal').load('ViewServiceSettings', {serviceId: id}, function () {
                            $('#service-modal').modal();
                        });
                    }, 2000, id);
                });

                var fName = '';
                var fValue = '';
                var tbody = $("#tbody_service_history");
                var search = $(".filter-submit");
                search.on('click', function (e) {
                    e.preventDefault();
                    if ($("#serviceName").val() != '') {
                        fName = $("#serviceName").attr("name");
                        fValue = $("#serviceName").val();
                    } else if ($("#serviceStatus").val() != '') {
                        fName = $("#serviceStatus").attr("name");
                        fValue = $("#serviceStatus").val();
                    }
                    var dataString = 'fieldName=' + fName;
                    dataString += '&fieldValue=' + fValue;
                    $.post('SearchServiceHistory', dataString).done(function (data) {
                        tbody.html(data);
                    });
                });

                $('.filter-cancel').on('click', function (e) {
                    e.preventDefault();
                    $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                        $(this).val("");
                    });
                    setTimeout(location.reload(), 0);
                });
            });
        </script>
    </body>
</html>
