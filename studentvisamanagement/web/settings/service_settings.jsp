<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
        <style type="text/css">
            .table {
                counter-reset:section;
            }

            .count:before {
                counter-increment:section;
                content:counter(section);
            }
        </style>
    </head>
    <body>
        <div class="site-wrapper">


            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Create Service</h3>
                                </div> 
                                <div class="form-horizontal">
                                    <div class="panel-body">

                                        <form action="AddNewService" method="POST">
                                            <div class="form-group">  
                                                <div class="col-sm-7"> 
                                                    <table class="table table-bordered" id="table-items"> 
                                                        <thead> 
                                                            <tr> 
                                                                <th>Serial</th> 
                                                                <th>Service Name *</th> 
                                                                <th>Amount *</th> 
                                                                <th>Currency</th>  
                                                                <th>Action</th> 
                                                            </tr> 
                                                        </thead> 
                                                        <tbody> 
                                                            <tr style="text-align: center;">
                                                                <td class="count"></td>
                                                                <td><input type="text" id="serviceName" name="serviceName" value="" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Service Name" class="form-control"></td>
                                                                <td><input type="text" id="serviceAmount" name="serviceAmount" value="" pattern="[0-9]{2,7}" required placeholder="Amount" class="form-control"></td>
                                                                <td><input type="text" id="currency" name="currency" value="MYR" required readonly class="form-control"></td>
                                                                <td style="width: 105px;"></td>
                                                            </tr>
                                                        </tbody> 
                                                        <tfoot>
                                                            <tr>
                                                                <td style="text-align: center;" colspan="5">
                                                                    <button type="button" class="btn green" id="add-service">
                                                                        <i class="fa fa-plus-circle"></i>
                                                                        Add Row
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="panel-footer">
                                                <div class="row">
                                                    <div class="col-sm-9 col-sm-offset-3">
                                                        <input class="btn btn-primary" type="submit" name="submit" value="Submit"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#add-service').on('click', function () {
                    $("#table-items").find('tbody')
                            .append('<tr class="info" style="text-align: center;">\n\
                                        <td class="count"></td>\n\
                                        <td><input type="text" id="serviceName" name="serviceName" value="" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Service Name" class="form-control"></td>\n\
                                        <td><input type="text" id="serviceAmount" name="serviceAmount" value="" pattern="[0-9]{2,7}" required placeholder="Amount" class="form-control"></td>\n\
                                        <td><input type="text" id="currency" name="currency" value="MYR" required readonly class="form-control"></td>\n\
                                        <td style="width: 105px;">\n\
                                            <button type="button" class="btn btn-danger remove-row">\n\
                                                <i class="fa fa-trash"></i>\n\
                                            </button>\n\
                                        </td>\n\
                                    </tr>');
                });

                $(document).on('click', 'button.remove-row', function () {
                    $(this).closest('tr.info').remove();
                    return false;
                });
            });
        </script>
    </body>
</html>
