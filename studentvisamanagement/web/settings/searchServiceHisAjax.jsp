<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="serviceInfoList !=null">
    <s:if test="serviceInfoList.size() !=0">
        <s:iterator value="serviceInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="serviceName"/></td>
                <td align="center"><s:property value="serviceAmount"/></td>
                <td align="center">MYR</td>
                <td>
                    <s:if test="serviceStatus == 'Y' ">
                        Active
                    </s:if>
                    <s:else>
                        <div style="color: red;">
                            Inactive
                        </div>
                    </s:else>
                </td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="serviceID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                        <i class="icon-pencil"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>