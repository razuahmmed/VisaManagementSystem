<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">University Settings</h3>
                                </div> 
                                <form action="AddNewUniversity" method="POST" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Name of University *</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="varsityName" name="varsityName" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Name of University" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Contact</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="varsityContact" name="varsityContact" pattern="[A-Za-z-0-9 ]{3,20}" placeholder="Contact" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Email</label>
                                            <div class="col-sm-4">
                                                <input type="email" id="varsityEmail" name="varsityEmail" max="25" min="5" placeholder="Email" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Country *</label>
                                            <div class="col-sm-4">
                                                <select id="varsityCountry" name="varsityCountry" required class="form-control">
                                                    <option value="">Select Country</option>
                                                    <s:if test="countryInfoList !=null">
                                                        <s:if test="countryInfoList.size() !=0">
                                                            <s:iterator value="countryInfoList">
                                                                <option value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Address</label>
                                            <div class="col-sm-4">
                                                <textarea id="varsityAddress" name="varsityAddress" pattern="[A-Za-z-0-9 ]{3,150}" placeholder="Address" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Notes</label>
                                            <div class="col-sm-4">
                                                <textarea id="varsityNotes" name="varsityNotes" pattern="[A-Za-z-0-9 ]{3,100}" placeholder="Note" class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Course Name *</label>
                                            <div id="course-div" class="col-sm-4">
                                                <div>
                                                    <input type="text" id="courseName" name="courseName" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Name of Course" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-4">
                                                <button type="button" class="btn green" id="add-course">
                                                    <i class="fa fa-plus-circle"></i>
                                                    Add Course
                                                </button>
                                                <button type="button" style="margin-left: 5px;" class="btn btn-danger" id="remove-course">
                                                    <i class="fa fa-minus-circle"></i>
                                                    Remove Course
                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input class="btn btn-primary" type="submit" name="submit" value="Submit"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END  -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <script type="text/javascript">
            $(document).ready(function () {

                var remove_course = $('#remove-course');
                remove_course.hide();

                remove_course.on('click', function () {
                    $("#course-div div.item-course").fadeOut(300, function () {
                        $(this).remove();
                    });
                });

                $('#add-course').on('click', function () {
                    $("#course-div").append('<div><input type="text" id="courseName" name="courseName" style="margin-top: 15px;" pattern="[A-Za-z-0-9 ]{3,20}" required placeholder="Name of Course" class="form-control item-course"></div>');
                });

                $('#course-div').on('click', '.item-course', function () {
                    $("div").removeClass("item-course");
                    $(this).closest('div').addClass("item-course");
                    remove_course.show();
                });
            });
        </script>

    </body>
</html>
