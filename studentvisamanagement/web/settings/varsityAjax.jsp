<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleVarsityInfoList !=null">
    <s:if test="singleVarsityInfoList.size() !=0">
        <s:iterator value="singleVarsityInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">University Name - <s:property value="varsityName"/></h4>
            </div>
            <form id="form" action="EditUniversityCourses" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN ALERTS PORTLET-->
                            <div class="portlet green box">
                                <div class="portlet-body">
                                    <h4>Details of <s:property value="varsityName"/></h4>
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <tbody>
                                                <tr>
                                                    <td>Name *</td>
                                                    <td>
                                                        <input type="text" id="varsityName" name="varsityName" pattern="[A-Za-z-0-9 ]{3,20}" required value="<s:property value="varsityName"/>">
                                                        <input type="hidden" id="varsityId" name="varsityId" value="<s:property value="varsityID"/>">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Contact</td>
                                                    <td><input type="text" id="varsityContact" name="varsityContact" pattern="[A-Za-z-0-9 ]{3,20}" value="<s:property value="varsityContact"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td><input type="text" id="varsityEmail" name="varsityEmail" max="25" min="5" value="<s:property value="varsityEmail"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Country *</td>
                                                    <td>
                                                        <select id="varsityCountry" name="varsityCountry" required class="form-control">
                                                            <s:if test="countryInfoList !=null">
                                                                <s:if test="countryInfoList.size() !=0">
                                                                    <s:iterator value="countryInfoList">
                                                                        <option <s:if test="countryID==countryInfos.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                    </s:iterator>
                                                                </s:if>
                                                            </s:if>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Courses *</td>
                                                    <td>
                                                        <s:iterator value="coursesList">
                                                            <input type="hidden" id="courseId" name="courseId" value="<s:property value="courseID"/>">
                                                            <input type="text" style="margin-top: 5px;" id="courseName" name="courseName" pattern="[A-Za-z-0-9 ]{3,20}" required value="<s:property value="courseName"/>">
                                                            <select id="courseStatus" name="courseStatus" class="">
                                                                <s:if test="courseStatus=='Y'">
                                                                    <option selected value="Y">Active</option>
                                                                    <option value="N">Inactive</option>
                                                                </s:if>
                                                                <s:else>
                                                                    <option value="Y">Active</option>
                                                                    <option selected value="N">Inactive</option>
                                                                </s:else>
                                                            </select>
                                                            <br/>
                                                        </s:iterator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Status</td>
                                                    <td>
                                                        <select id="varsityStatus" name="varsityStatus" class="">
                                                            <s:if test="varsityStatus=='Y'">
                                                                <option selected value="Y">Active</option>
                                                                <option value="N">Inactive</option>
                                                            </s:if>
                                                            <s:else>
                                                                <option value="Y">Active</option>
                                                                <option selected value="N">Inactive</option>
                                                            </s:else>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td><textarea id="varsityAddress" name="varsityAddress"><s:property value="varsityAddress"/></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Notes</td>
                                                    <td><textarea id="varsityNotes" name="varsityNotes"><s:property value="varsityNotes"/></textarea></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <button style="font-size: 14px;" type="reset" class="btn red">Reset</button>
                                                        <button style="margin-right: 5px; font-size: 14px;" type="submit" class="btn blue">Save Change</button>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END ALERTS PORTLET-->
                        </div>
                    </div>
                </div>
            </form>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>
