<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="varsityInfoList !=null">
    <s:if test="varsityInfoList.size() !=0">
        <s:iterator value="varsityInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="varsityName"/></td>
                <td align="center"><s:property value="varsityContact"/></td>
                <td align="center"><s:property value="varsityEmail"/></td>
                <td align="center"><s:property value="countryInfos.countryName"/></td>
                <td align="center">
                    <s:if test="varsityStatus=='Y'">
                        Active
                    </s:if>
                    <s:else>
                        <div style="color: red;">
                            Inactive
                        </div>
                    </s:else>
                </td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="varsityID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                        <i class="icon-pencil"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>