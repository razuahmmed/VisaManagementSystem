<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>
    </head>
    <body>
        <div class="site-wrapper">
            <!-- HEADER -->
            <%@include file="/home/header.jsp" %>
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="form-group">
                                    <h3><s:property value="messageString"/></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- FOOTER -->
            <%@include file="/home/footer.jsp" %>
        </div>
        <script src="<%= request.getContextPath()%>/js/jqueryLibrary.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.pogo-slider.js"></script>
        <script src="<%= request.getContextPath()%>/js/scripts.js"></script>
    </body>
</html>