<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="singleAgentInfoList !=null">
    <s:if test="singleAgentInfoList.size() !=0">
        <s:iterator value="singleAgentInfoList">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"><s:property value="userGroupInfo.groupName"/> Full Name - <s:property value="userFullName"/></h4>
            </div>
            <form id="form" action="EditUser" method="post">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN ALERTS PORTLET-->
                            <div class="portlet green box">
                                <div class="portlet-body">
                                    <h4><s:property value="userGroupInfo.groupName"/> Details</h4>
                                    <div class="table-scrollable">
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th style="width: 30%">User Name</th>
                                                    <th><s:property value="userName"/></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Full Name *</td>
                                                    <td><input type="text" id="fullName" name="fullName" pattern="[A-Za-z-0-9-_ ]{3,20}" required value="<s:property value="userFullName"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>User Type</td>
                                                    <td><s:property value="userGroupInfo.groupName"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Fax</td>
                                                    <td><input type="text" id="faxNumber" name="faxNumber" pattern="[0-9]{8,15}" value="<s:property value="userFax"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Email *</td>
                                                    <td><input type="text" id="email" name="email" minlength="10" maxlength="30" required value="<s:property value="userEmail"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Phone *</td>
                                                    <td><input type="text" id="phoneNumber" name="phoneNumber" pattern="[0-9]{10,15}" required value="<s:property value="userPhone"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Date of Birth *</td>
                                                    <td>
                                                        <div class="input-group">
                                                            <input type="date" id="dob" name="dob" placeholder="YYYY-MM-DD" value="<s:property value="userDob"/>" required class="form-control">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Country *</td>
                                                    <td>
                                                        <select id="country" name="country" required class="form-control">
                                                            <option selected value="">Select Country</option>
                                                            <s:if test="countryInfoList !=null">
                                                                <s:if test="countryInfoList.size() !=0">
                                                                    <s:iterator value="countryInfoList">
                                                                        <option <s:if test="countryID==countryInfo.countryID">selected</s:if> value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                                    </s:iterator>
                                                                </s:if>
                                                            </s:if>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Company</td>
                                                    <td><input type="text" id="company" name="company" pattern="[A-Za-z-0-9 ]{3,20}" value="<s:property value="company"/>"></td>
                                                </tr>
                                                <tr>
                                                    <td>Status *</td>
                                                    <td>
                                                        <select id="userStatus" name="userStatus" required class="form-control">
                                                            <s:if test="userStatus=='Y'">
                                                                <option selected value="Y">Active</option>
                                                                <option value="N">Inactive</option>
                                                            </s:if>
                                                            <s:else>
                                                                <option value="Y">Active</option>
                                                                <option selected value="N">Inactive</option>
                                                            </s:else>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Address *</td>
                                                    <td><textarea id="address" name="address" minlength="10" maxlength="150" required><s:property value="userAddress"/></textarea></td>
                                                </tr>
                                                <tr>
                                                    <td>Notes</td>
                                                    <td><textarea id="notes" name="notes" minlength="10" maxlength="100"><s:property value="userNotes"/></textarea></td>
                                                </tr>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <input type="hidden" id="userId" name="userId" value="<s:property value="userID"/>">
                                                        <button style="font-size: 14px;" type="reset" class="btn red">Reset</button>
                                                        <button style="margin-right: 5px; font-size: 14px;" type="submit" class="btn blue btn-edit">Save Change</button>
                                                    </td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END ALERTS PORTLET-->
                        </div>
                    </div>
                </div>
            </form>
        </s:iterator>
        <div class="modal-footer">
            <button type="button" class="btn red" data-dismiss="modal">Close</button>
        </div>
    </s:if>
</s:if>
<script type="text/javascript">
    $(document).ready(function () {
        var date_dob = $('input[name="dob"]');
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        var options = {
            format: 'yyyy-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true
        };
        date_dob.datepicker(options);
    });
</script>
