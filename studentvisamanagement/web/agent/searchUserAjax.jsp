<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="s" uri="/struts-tags"%>
<s:if test="agentInfoList !=null">
    <s:if test="agentInfoList.size() !=0">
        <s:iterator value="agentInfoList">
            <tr style="text-align: center;">
                <td align="center"><s:property value="userFullName"/></td>
                <td><s:property value="userName"/></td>
                <td><s:property value="userPhone"/></td>
                <td><s:property value="userFax"/></td>
                <td><s:property value="userEmail"/></td>
                <td><s:property value="countryInfo.countryName"/></td>
                <td><s:property value="company"/></td>
                <td id="grname"><s:property value="userGroupInfo.groupName"/></td>
                <td>
                    <s:if test="userStatus=='Y'">
                        Active
                    </s:if>
                    <s:else>
                        <div style="color: red;">
                            Inactive
                        </div>
                    </s:else>
                </td>
                <td>
                    <a href="javascript:void(0);" id="<s:property value="userID"/>" class="btn btn-xs blue btn-editable btn-view" data-toggle="tooltip" data-placement="top" title="View / Edit Button">
                        <i class="icon-pencil"></i>
                    </a>
                    <a href="javascript:void(0);" id="<s:property value="userID"/>" class="btn btn-xs purple btn-danger btn-pass-change" data-toggle="tooltip" data-placement="top" title="Change Password">
                        <i class="icon-key"></i>
                    </a>
                </td>
            </tr>
        </s:iterator>
    </s:if>
</s:if>