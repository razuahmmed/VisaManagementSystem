<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">
    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER START -->
            <%@include file="/home/header.jsp" %>
            <!-- HEADER END -->

            <!-- PAGE CONTENT START -->
            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Create Agent Profile</h3>
                                </div> 
                                <form action="AddNewUser" method="POST" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">User Type *</label>
                                            <div class="col-sm-4">
                                                <select id="userGroup" name="userGroup" required class="form-control">
                                                    <option selected value="2">Agent</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Full Name *</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="fullName" name="fullName" pattern="[A-Za-z-0-9-_ ]{3,20}" minlength="3" maxlength="20" placeholder="Full Name" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">User Name *</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="userName" name="userName" pattern="[a-z-0-9]{3,15}" minlength="3" maxlength="15" placeholder="User Name" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Password *</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="password" name="password" pattern="[a-zA-Z0-9]{3,15}" minlength="3" maxlength="15" placeholder="Password" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Company Name</label>
                                            <div class="col-sm-4">
                                                <input type="text" id="company" name="company" pattern="[A-Za-z-0-9 ]{3,20}" minlength="3" maxlength="20" placeholder="Company Name" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><strong>Phone Number *</strong></label>
                                            <div class="col-sm-4">
                                                <input type="tel" id="phoneNumber" name="phoneNumber" pattern="[0-9]{10,15}" minlength="10" maxlength="15" placeholder="Phone Number" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><strong>Fax Number</strong></label>
                                            <div class="col-sm-4">
                                                <input type="tel" id="faxNumber" name="faxNumber" pattern="[0-9]{8,15}" minlength="8" maxlength="15" placeholder="Fax Number" class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Email *</label>
                                            <div class="col-sm-4">
                                                <input type="email" id="email" name="email" minlength="10" maxlength="30" placeholder="Email" required class="form-control">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Date Of Birth *</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <input type="date" id="dob" name="dob" placeholder="YYYY-MM-DD" required class="form-control">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Country *</label>
                                            <div class="col-sm-4">
                                                <select id="country" name="country" required class="form-control">
                                                    <option selected value="">Select Country</option>
                                                    <s:if test="countryInfoList !=null">
                                                        <s:if test="countryInfoList.size() !=0">
                                                            <s:iterator value="countryInfoList">
                                                                <option value="<s:property value="countryID"/>"><s:property value="countryName"/></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Address *</label>
                                            <div class="col-sm-4">
                                                <textarea id="address" name="address" minlength="10" maxlength="150" placeholder="Address" required class="form-control"></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Notes</label>
                                            <div class="col-sm-4">
                                                <textarea id="notes" name="notes" minlength="10" maxlength="100" placeholder="Notes" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input type="submit" id="btn_submit" name="btn_submit" value="Submit" class="btn btn-primary"> 
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 
            <!-- PAGE CONTENT END -->

            <!-- FOOTER START -->
            <%@include file="/home/footer.jsp" %>
            <!-- FOOTER END -->
        </div>

        <%@include file="/home/resource-js.jsp" %>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var date_dob = $('input[name="dob"]');
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };
                date_dob.datepicker(options);
            });
        </script>

    </body>
</html>
