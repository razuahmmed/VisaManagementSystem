<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="/home/resource-css.jsp" %>

        <link href="<%= request.getContextPath()%>/css/select2.css" rel="stylesheet" type="text/css"/>

        <!-- THIS CSS FOR BOOTSTRAP DATE PICKER -->
        <link href="<%= request.getContextPath()%>/css/bootstrap-datepicker.css" rel="stylesheet">

        <style type="text/css">
            table th, tr, td {
                padding: 5px;
                margin: 0px;
                border: #eee 1px solid;
            }

            .form-control.drop-select {
                border: none;
                padding: 0;
                background: rgba(0, 0, 0, 0) url("../images/select2.png") no-repeat scroll 0 1px;
            }

            .select2-container .select2-choice {
                border-radius: 0px;
            }
        </style>

    </head>
    <body>
        <div class="site-wrapper">

            <!-- HEADER -->
            <%@include file="/home/header.jsp" %>

            <section class="main-content">
                <div class="fature-box">
                    <div class="container">
                        <div class="row">  
                            <div class="panel">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Generate Report</h3>
                                </div> 
                                <form action="GenerateNewReport" method="post" target="_blank" class="form-horizontal">
                                    <div class="panel-body">

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Date From*</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <input type="text" id="dateFrom" name="dateFrom" required placeholder="YYYY-MM-DD" class="form-control">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Date To*</label>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    <input type="text" id="dateTo" name="dateTo" required placeholder="YYYY-MM-DD" class="form-control">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="demo-is-inputsmall">Agent</label>
                                            <div class="col-sm-4"> 
                                                <select id="agentId" name="agentId" required data-placeholder="Select Agent" class="form-control drop-select">
                                                    <option selected value=""></option>
                                                    <s:if test="agentInfoList !=null">
                                                        <s:if test="agentInfoList.size() !=0">
                                                            <s:iterator value="agentInfoList">
                                                                <option value="<s:property value="userID"/>"><s:property value="userFullName"/>&nbsp;&nbsp;<s:if test="userStatus!='Y'">(Inactive Agent)</s:if></option>
                                                            </s:iterator>
                                                        </s:if>
                                                    </s:if>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Report Type *</label>
                                            <div class="col-md-4">
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        <input type="radio" id="fileType" name="fileType" value="1" checked>
                                                        Student
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="fileType" name="fileType" value="2">
                                                        Worker
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Report Format*</label>
                                            <div class="col-md-4">
                                                <div class="radio-list">
                                                    <label class="radio-inline">
                                                        <input type="radio" id="rptFmt" name="rptFmt" value="pdf" checked>
                                                        PDF
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" id="rptFmt" name="rptFmt" value="xls">
                                                        Excel
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="panel-footer">
                                        <div class="row">
                                            <div class="col-sm-9 col-sm-offset-3">
                                                <input class="btn btn-primary" type="submit" name="submit" value="Generate Report"> 
                                            </div>
                                        </div>
                                    </div>

                                </form>
                            </div> 
                        </div>
                    </div>
                </div> 
            </section> 

            <!-- FOOTER -->
            <%@include file="/home/footer.jsp" %>
        </div>

        <script src="<%= request.getContextPath()%>/js/jqueryLibrary.js"></script>
        <script src="<%= request.getContextPath()%>/js/bootstrap.min.js"></script>
        <script src="<%= request.getContextPath()%>/js/jquery.pogo-slider.js"></script>
        <script src="<%= request.getContextPath()%>/js/scripts.js"></script>

        <script src="<%= request.getContextPath()%>/js/select2.js" type="text/javascript"></script>

        <!-- THIS JQUERY FOR BOOTSTRAP DATE PICKER -->
        <script src="<%= request.getContextPath()%>/js/bootstrap-datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {

                var date_from = $('input[name="dateFrom"]');
                var date_to = $('input[name="dateTo"]');

                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                var options = {
                    format: 'yyyy-mm-dd',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                };

                date_from.datepicker(options);
                date_to.datepicker(options);

                $('#agentId').select2().on("change", function (e) {
                });
            });
        </script>

    </body>
</html>
