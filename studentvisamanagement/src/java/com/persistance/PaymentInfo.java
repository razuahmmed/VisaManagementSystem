package com.persistance;

public class PaymentInfo {

    private Integer paymentID;
    private Integer paymentHistoryId;
    private String paymentMode;
    private String paymentDate;
    private Double tuitionFee;
    private Double othersAmount;
    private Double workerAmount;
    private String currency;
    private String conversionRate;
    private String bankName;
    private String chequeReference;
    private String chequeDate;
    private Double amount;
    private ApplicantInfo applicantInfo;
    private UserInfo userInfo;
    private ServiceInfo serviceInfo;
    private ApplicantServiceInfo applicantServiceInfo;

    private Double tuitionFeeDue;
    private Double othersAmountDue;
    private Double workerAmountDue;

    private Double tuitionAgree;
    private Double othersAgree;
    private Double workerAgree;

    /**
     * @return the paymentID
     */
    public Integer getPaymentID() {
        return paymentID;
    }

    /**
     * @param paymentID the paymentID to set
     */
    public void setPaymentID(Integer paymentID) {
        this.paymentID = paymentID;
    }

    /**
     * @return the paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * @param paymentMode the paymentMode to set
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the conversionRate
     */
    public String getConversionRate() {
        return conversionRate;
    }

    /**
     * @param conversionRate the conversionRate to set
     */
    public void setConversionRate(String conversionRate) {
        this.conversionRate = conversionRate;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the chequeReference
     */
    public String getChequeReference() {
        return chequeReference;
    }

    /**
     * @param chequeReference the chequeReference to set
     */
    public void setChequeReference(String chequeReference) {
        this.chequeReference = chequeReference;
    }

    /**
     * @return the chequeDate
     */
    public String getChequeDate() {
        return chequeDate;
    }

    /**
     * @param chequeDate the chequeDate to set
     */
    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    /**
     * @return the applicantInfo
     */
    public ApplicantInfo getApplicantInfo() {
        return applicantInfo;
    }

    /**
     * @param applicantInfo the applicantInfo to set
     */
    public void setApplicantInfo(ApplicantInfo applicantInfo) {
        this.applicantInfo = applicantInfo;
    }

    /**
     * @return the serviceInfo
     */
    public ServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    /**
     * @param serviceInfo the serviceInfo to set
     */
    public void setServiceInfo(ServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the tuitionFee
     */
    public Double getTuitionFee() {
        return tuitionFee;
    }

    /**
     * @param tuitionFee the tuitionFee to set
     */
    public void setTuitionFee(Double tuitionFee) {
        this.tuitionFee = tuitionFee;
    }

    /**
     * @return the othersAmount
     */
    public Double getOthersAmount() {
        return othersAmount;
    }

    /**
     * @param othersAmount the othersAmount to set
     */
    public void setOthersAmount(Double othersAmount) {
        this.othersAmount = othersAmount;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the paymentHistoryId
     */
    public Integer getPaymentHistoryId() {
        return paymentHistoryId;
    }

    /**
     * @param paymentHistoryId the paymentHistoryId to set
     */
    public void setPaymentHistoryId(Integer paymentHistoryId) {
        this.paymentHistoryId = paymentHistoryId;
    }

    /**
     * @return the applicantServiceInfo
     */
    public ApplicantServiceInfo getApplicantServiceInfo() {
        return applicantServiceInfo;
    }

    /**
     * @param applicantServiceInfo the applicantServiceInfo to set
     */
    public void setApplicantServiceInfo(ApplicantServiceInfo applicantServiceInfo) {
        this.applicantServiceInfo = applicantServiceInfo;
    }

    /**
     * @return the tuitionFeeDue
     */
    public Double getTuitionFeeDue() {
        return tuitionFeeDue;
    }

    /**
     * @param tuitionFeeDue the tuitionFeeDue to set
     */
    public void setTuitionFeeDue(Double tuitionFeeDue) {
        this.tuitionFeeDue = tuitionFeeDue;
    }

    /**
     * @return the othersAmountDue
     */
    public Double getOthersAmountDue() {
        return othersAmountDue;
    }

    /**
     * @param othersAmountDue the othersAmountDue to set
     */
    public void setOthersAmountDue(Double othersAmountDue) {
        this.othersAmountDue = othersAmountDue;
    }

    /**
     * @return the tuitionAgree
     */
    public Double getTuitionAgree() {
        return tuitionAgree;
    }

    /**
     * @param tuitionAgree the tuitionAgree to set
     */
    public void setTuitionAgree(Double tuitionAgree) {
        this.tuitionAgree = tuitionAgree;
    }

    /**
     * @return the othersAgree
     */
    public Double getOthersAgree() {
        return othersAgree;
    }

    /**
     * @param othersAgree the othersAgree to set
     */
    public void setOthersAgree(Double othersAgree) {
        this.othersAgree = othersAgree;
    }

    /**
     * @return the workerAmount
     */
    public Double getWorkerAmount() {
        return workerAmount;
    }

    /**
     * @param workerAmount the workerAmount to set
     */
    public void setWorkerAmount(Double workerAmount) {
        this.workerAmount = workerAmount;
    }

    /**
     * @return the workerAgree
     */
    public Double getWorkerAgree() {
        return workerAgree;
    }

    /**
     * @param workerAgree the workerAgree to set
     */
    public void setWorkerAgree(Double workerAgree) {
        this.workerAgree = workerAgree;
    }

    /**
     * @return the workerAmountDue
     */
    public Double getWorkerAmountDue() {
        return workerAmountDue;
    }

    /**
     * @param workerAmountDue the workerAmountDue to set
     */
    public void setWorkerAmountDue(Double workerAmountDue) {
        this.workerAmountDue = workerAmountDue;
    }
}
