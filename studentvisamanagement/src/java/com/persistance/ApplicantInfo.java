package com.persistance;

import java.util.List;

public class ApplicantInfo {

    private Integer appTableID;
    private String applicantID;
    private String applicantName;
    private String passportNumber;
    private String alternatePassportNo;
    private String dob;
    private String passportIssuedPlace;
    private String passportIssueDate;
    private String passportExpiryDate;
    private String gender;
    private String applicantEmail;
    private String applicantAlternateEmail;
    private String maritalStatus;
    private String mobileNumber;
    private String postalCode;
    private String permanentAddress;
    private String residentNumber;
    private String nationality;
    private String state;
    private String city;
    private String fathersName;
    private String fathersOccupation;
    private String mothersName;
    private String mothersOccupation;
    private String highestQualification;
    private String hqPassYear;
    private String overAllGrade;
    private String visaCountry;
    private String visaExpiryDate;
    private String visaReleaseLetter;
    private String visaColName;
    private String visaType;
    private String corMobileNumber;
    private String corPostalCode;
    private String corAddress;
    private String corResidentNumber;
    private String corState;
    private String corCountry;
    private String corCity;
    private String arrivalDate;
    private Character appStatus;
    private String appInsertBy;
    private String appInsertDate;
    private String appUpdateBy;
    private String appUpdateDate;
    private UserInfo userInfo;
    private ApplicantServiceInfo asi;
    private ServiceInfo serviceInfo;
    private List<ApplicantEducationInfo> eduInfoList;
    private List<ApplicantCoursesInfo> appVarsityInfoList;
    private List<ApplicantCoursesInfo> appCoursesInfoList;
    private CountryInfo countryInfo;
    private List<PaymentInfo> paymentInfo;

    private String applicantType;
    private String applicantSector;

    /**
     * @return the appTableID
     */
    public Integer getAppTableID() {
        return appTableID;
    }

    /**
     * @param appTableID the appTableID to set
     */
    public void setAppTableID(Integer appTableID) {
        this.appTableID = appTableID;
    }

    /**
     * @return the applicantID
     */
    public String getApplicantID() {
        return applicantID;
    }

    /**
     * @param applicantID the applicantID to set
     */
    public void setApplicantID(String applicantID) {
        this.applicantID = applicantID;
    }

    /**
     * @return the applicantName
     */
    public String getApplicantName() {
        return applicantName;
    }

    /**
     * @param applicantName the applicantName to set
     */
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    /**
     * @return the passportNumber
     */
    public String getPassportNumber() {
        return passportNumber;
    }

    /**
     * @param passportNumber the passportNumber to set
     */
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * @return the alternatePassportNo
     */
    public String getAlternatePassportNo() {
        return alternatePassportNo;
    }

    /**
     * @param alternatePassportNo the alternatePassportNo to set
     */
    public void setAlternatePassportNo(String alternatePassportNo) {
        this.alternatePassportNo = alternatePassportNo;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the passportIssuedPlace
     */
    public String getPassportIssuedPlace() {
        return passportIssuedPlace;
    }

    /**
     * @param passportIssuedPlace the passportIssuedPlace to set
     */
    public void setPassportIssuedPlace(String passportIssuedPlace) {
        this.passportIssuedPlace = passportIssuedPlace;
    }

    /**
     * @return the passportIssueDate
     */
    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    /**
     * @param passportIssueDate the passportIssueDate to set
     */
    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    /**
     * @return the passportExpiryDate
     */
    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    /**
     * @param passportExpiryDate the passportExpiryDate to set
     */
    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the applicantEmail
     */
    public String getApplicantEmail() {
        return applicantEmail;
    }

    /**
     * @param applicantEmail the applicantEmail to set
     */
    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    /**
     * @return the applicantAlternateEmail
     */
    public String getApplicantAlternateEmail() {
        return applicantAlternateEmail;
    }

    /**
     * @param applicantAlternateEmail the applicantAlternateEmail to set
     */
    public void setApplicantAlternateEmail(String applicantAlternateEmail) {
        this.applicantAlternateEmail = applicantAlternateEmail;
    }

    /**
     * @return the maritalStatus
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the permanentAddress
     */
    public String getPermanentAddress() {
        return permanentAddress;
    }

    /**
     * @param permanentAddress the permanentAddress to set
     */
    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    /**
     * @return the corMobileNumber
     */
    public String getCorMobileNumber() {
        return corMobileNumber;
    }

    /**
     * @param corMobileNumber the corMobileNumber to set
     */
    public void setCorMobileNumber(String corMobileNumber) {
        this.corMobileNumber = corMobileNumber;
    }

    /**
     * @return the corPostalCode
     */
    public String getCorPostalCode() {
        return corPostalCode;
    }

    /**
     * @param corPostalCode the corPostalCode to set
     */
    public void setCorPostalCode(String corPostalCode) {
        this.corPostalCode = corPostalCode;
    }

    /**
     * @return the corAddress
     */
    public String getCorAddress() {
        return corAddress;
    }

    /**
     * @param corAddress the corAddress to set
     */
    public void setCorAddress(String corAddress) {
        this.corAddress = corAddress;
    }

    /**
     * @return the fathersName
     */
    public String getFathersName() {
        return fathersName;
    }

    /**
     * @param fathersName the fathersName to set
     */
    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    /**
     * @return the fathersOccupation
     */
    public String getFathersOccupation() {
        return fathersOccupation;
    }

    /**
     * @param fathersOccupation the fathersOccupation to set
     */
    public void setFathersOccupation(String fathersOccupation) {
        this.fathersOccupation = fathersOccupation;
    }

    /**
     * @return the mothersName
     */
    public String getMothersName() {
        return mothersName;
    }

    /**
     * @param mothersName the mothersName to set
     */
    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    /**
     * @return the mothersOccupation
     */
    public String getMothersOccupation() {
        return mothersOccupation;
    }

    /**
     * @param mothersOccupation the mothersOccupation to set
     */
    public void setMothersOccupation(String mothersOccupation) {
        this.mothersOccupation = mothersOccupation;
    }

    /**
     * @return the highestQualification
     */
    public String getHighestQualification() {
        return highestQualification;
    }

    /**
     * @param highestQualification the highestQualification to set
     */
    public void setHighestQualification(String highestQualification) {
        this.highestQualification = highestQualification;
    }

    /**
     * @return the hqPassYear
     */
    public String getHqPassYear() {
        return hqPassYear;
    }

    /**
     * @param hqPassYear the hqPassYear to set
     */
    public void setHqPassYear(String hqPassYear) {
        this.hqPassYear = hqPassYear;
    }

    /**
     * @return the overAllGrade
     */
    public String getOverAllGrade() {
        return overAllGrade;
    }

    /**
     * @param overAllGrade the overAllGrade to set
     */
    public void setOverAllGrade(String overAllGrade) {
        this.overAllGrade = overAllGrade;
    }

    /**
     * @return the visaCountry
     */
    public String getVisaCountry() {
        return visaCountry;
    }

    /**
     * @param visaCountry the visaCountry to set
     */
    public void setVisaCountry(String visaCountry) {
        this.visaCountry = visaCountry;
    }

    /**
     * @return the visaExpiryDate
     */
    public String getVisaExpiryDate() {
        return visaExpiryDate;
    }

    /**
     * @param visaExpiryDate the visaExpiryDate to set
     */
    public void setVisaExpiryDate(String visaExpiryDate) {
        this.visaExpiryDate = visaExpiryDate;
    }

    /**
     * @return the visaReleaseLetter
     */
    public String getVisaReleaseLetter() {
        return visaReleaseLetter;
    }

    /**
     * @param visaReleaseLetter the visaReleaseLetter to set
     */
    public void setVisaReleaseLetter(String visaReleaseLetter) {
        this.visaReleaseLetter = visaReleaseLetter;
    }

    /**
     * @return the visaColName
     */
    public String getVisaColName() {
        return visaColName;
    }

    /**
     * @param visaColName the visaColName to set
     */
    public void setVisaColName(String visaColName) {
        this.visaColName = visaColName;
    }

    /**
     * @return the visaType
     */
    public String getVisaType() {
        return visaType;
    }

    /**
     * @param visaType the visaType to set
     */
    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    /**
     * @return the corResidentNumber
     */
    public String getCorResidentNumber() {
        return corResidentNumber;
    }

    /**
     * @param corResidentNumber the corResidentNumber to set
     */
    public void setCorResidentNumber(String corResidentNumber) {
        this.corResidentNumber = corResidentNumber;
    }

    /**
     * @return the corState
     */
    public String getCorState() {
        return corState;
    }

    /**
     * @param corState the corState to set
     */
    public void setCorState(String corState) {
        this.corState = corState;
    }

    /**
     * @return the corCountry
     */
    public String getCorCountry() {
        return corCountry;
    }

    /**
     * @param corCountry the corCountry to set
     */
    public void setCorCountry(String corCountry) {
        this.corCountry = corCountry;
    }

    /**
     * @return the corCity
     */
    public String getCorCity() {
        return corCity;
    }

    /**
     * @param corCity the corCity to set
     */
    public void setCorCity(String corCity) {
        this.corCity = corCity;
    }

    /**
     * @return the residentNumber
     */
    public String getResidentNumber() {
        return residentNumber;
    }

    /**
     * @param residentNumber the residentNumber to set
     */
    public void setResidentNumber(String residentNumber) {
        this.residentNumber = residentNumber;
    }

    /**
     * @return the nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * @param nationality the nationality to set
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the appStatus
     */
    public Character getAppStatus() {
        return appStatus;
    }

    /**
     * @param appStatus the appStatus to set
     */
    public void setAppStatus(Character appStatus) {
        this.appStatus = appStatus;
    }

    /**
     * @return the appInsertBy
     */
    public String getAppInsertBy() {
        return appInsertBy;
    }

    /**
     * @param appInsertBy the appInsertBy to set
     */
    public void setAppInsertBy(String appInsertBy) {
        this.appInsertBy = appInsertBy;
    }

    /**
     * @return the appInsertDate
     */
    public String getAppInsertDate() {
        return appInsertDate;
    }

    /**
     * @param appInsertDate the appInsertDate to set
     */
    public void setAppInsertDate(String appInsertDate) {
        this.appInsertDate = appInsertDate;
    }

    /**
     * @return the appUpdateBy
     */
    public String getAppUpdateBy() {
        return appUpdateBy;
    }

    /**
     * @param appUpdateBy the appUpdateBy to set
     */
    public void setAppUpdateBy(String appUpdateBy) {
        this.appUpdateBy = appUpdateBy;
    }

    /**
     * @return the appUpdateDate
     */
    public String getAppUpdateDate() {
        return appUpdateDate;
    }

    /**
     * @param appUpdateDate the appUpdateDate to set
     */
    public void setAppUpdateDate(String appUpdateDate) {
        this.appUpdateDate = appUpdateDate;
    }

    /**
     * @return the countryInfo
     */
    public CountryInfo getCountryInfo() {
        return countryInfo;
    }

    /**
     * @param countryInfo the countryInfo to set
     */
    public void setCountryInfo(CountryInfo countryInfo) {
        this.countryInfo = countryInfo;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the arrivalDate
     */
    public String getArrivalDate() {
        return arrivalDate;
    }

    /**
     * @param arrivalDate the arrivalDate to set
     */
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    /**
     * @return the eduInfoList
     */
    public List<ApplicantEducationInfo> getEduInfoList() {
        return eduInfoList;
    }

    /**
     * @param eduInfoList the eduInfoList to set
     */
    public void setEduInfoList(List<ApplicantEducationInfo> eduInfoList) {
        this.eduInfoList = eduInfoList;
    }

    /**
     * @return the asi
     */
    public ApplicantServiceInfo getAsi() {
        return asi;
    }

    /**
     * @param asi the asi to set
     */
    public void setAsi(ApplicantServiceInfo asi) {
        this.asi = asi;
    }

    /**
     * @return the appVarsityInfoList
     */
    public List<ApplicantCoursesInfo> getAppVarsityInfoList() {
        return appVarsityInfoList;
    }

    /**
     * @param appVarsityInfoList the appVarsityInfoList to set
     */
    public void setAppVarsityInfoList(List<ApplicantCoursesInfo> appVarsityInfoList) {
        this.appVarsityInfoList = appVarsityInfoList;
    }

    /**
     * @return the appCoursesInfoList
     */
    public List<ApplicantCoursesInfo> getAppCoursesInfoList() {
        return appCoursesInfoList;
    }

    /**
     * @param appCoursesInfoList the appCoursesInfoList to set
     */
    public void setAppCoursesInfoList(List<ApplicantCoursesInfo> appCoursesInfoList) {
        this.appCoursesInfoList = appCoursesInfoList;
    }

    /**
     * @return the paymentInfo
     */
    public List<PaymentInfo> getPaymentInfo() {
        return paymentInfo;
    }

    /**
     * @param paymentInfo the paymentInfo to set
     */
    public void setPaymentInfo(List<PaymentInfo> paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    /**
     * @return the applicantType
     */
    public String getApplicantType() {
        return applicantType;
    }

    /**
     * @param applicantType the applicantType to set
     */
    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }

    /**
     * @return the applicantSector
     */
    public String getApplicantSector() {
        return applicantSector;
    }

    /**
     * @param applicantSector the applicantSector to set
     */
    public void setApplicantSector(String applicantSector) {
        this.applicantSector = applicantSector;
    }

    /**
     * @return the serviceInfo
     */
    public ServiceInfo getServiceInfo() {
        return serviceInfo;
    }

    /**
     * @param serviceInfo the serviceInfo to set
     */
    public void setServiceInfo(ServiceInfo serviceInfo) {
        this.serviceInfo = serviceInfo;
    }
}
