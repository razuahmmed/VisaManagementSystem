package com.persistance;

import java.util.List;

public class VarsityInfo {

    private Integer varsityID;
    private String varsityName;
    private String varsityContact;
    private String varsityEmail;
    private String varsityCountry;
    private String varsityAddress;
    private String varsityNotes;
    private Character varsityStatus;
    private String varsityInsertBy;
    private String varsityInsertDate;
    private String varsityUpdateBy;
    private String varsityUpdateDate;
    private List<CoursesInfo> coursesList;
    private CountryInfo countryInfos;

    private Integer courseID;
    private String courseName;
    private Character courseStatus;

    /**
     * @return the varsityID
     */
    public Integer getVarsityID() {
        return varsityID;
    }

    /**
     * @param varsityID the varsityID to set
     */
    public void setVarsityID(Integer varsityID) {
        this.varsityID = varsityID;
    }

    /**
     * @return the varsityName
     */
    public String getVarsityName() {
        return varsityName;
    }

    /**
     * @param varsityName the varsityName to set
     */
    public void setVarsityName(String varsityName) {
        this.varsityName = varsityName;
    }

    /**
     * @return the varsityContact
     */
    public String getVarsityContact() {
        return varsityContact;
    }

    /**
     * @param varsityContact the varsityContact to set
     */
    public void setVarsityContact(String varsityContact) {
        this.varsityContact = varsityContact;
    }

    /**
     * @return the varsityEmail
     */
    public String getVarsityEmail() {
        return varsityEmail;
    }

    /**
     * @param varsityEmail the varsityEmail to set
     */
    public void setVarsityEmail(String varsityEmail) {
        this.varsityEmail = varsityEmail;
    }

    /**
     * @return the varsityCountry
     */
    public String getVarsityCountry() {
        return varsityCountry;
    }

    /**
     * @param varsityCountry the varsityCountry to set
     */
    public void setVarsityCountry(String varsityCountry) {
        this.varsityCountry = varsityCountry;
    }

    /**
     * @return the varsityAddress
     */
    public String getVarsityAddress() {
        return varsityAddress;
    }

    /**
     * @param varsityAddress the varsityAddress to set
     */
    public void setVarsityAddress(String varsityAddress) {
        this.varsityAddress = varsityAddress;
    }

    /**
     * @return the varsityNotes
     */
    public String getVarsityNotes() {
        return varsityNotes;
    }

    /**
     * @param varsityNotes the varsityNotes to set
     */
    public void setVarsityNotes(String varsityNotes) {
        this.varsityNotes = varsityNotes;
    }

    /**
     * @return the courseID
     */
    public Integer getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the varsityStatus
     */
    public Character getVarsityStatus() {
        return varsityStatus;
    }

    /**
     * @param varsityStatus the varsityStatus to set
     */
    public void setVarsityStatus(Character varsityStatus) {
        this.varsityStatus = varsityStatus;
    }

    /**
     * @return the varsityInsertBy
     */
    public String getVarsityInsertBy() {
        return varsityInsertBy;
    }

    /**
     * @param varsityInsertBy the varsityInsertBy to set
     */
    public void setVarsityInsertBy(String varsityInsertBy) {
        this.varsityInsertBy = varsityInsertBy;
    }

    /**
     * @return the varsityInsertDate
     */
    public String getVarsityInsertDate() {
        return varsityInsertDate;
    }

    /**
     * @param varsityInsertDate the varsityInsertDate to set
     */
    public void setVarsityInsertDate(String varsityInsertDate) {
        this.varsityInsertDate = varsityInsertDate;
    }

    /**
     * @return the varsityUpdateBy
     */
    public String getVarsityUpdateBy() {
        return varsityUpdateBy;
    }

    /**
     * @param varsityUpdateBy the varsityUpdateBy to set
     */
    public void setVarsityUpdateBy(String varsityUpdateBy) {
        this.varsityUpdateBy = varsityUpdateBy;
    }

    /**
     * @return the varsityUpdateDate
     */
    public String getVarsityUpdateDate() {
        return varsityUpdateDate;
    }

    /**
     * @param varsityUpdateDate the varsityUpdateDate to set
     */
    public void setVarsityUpdateDate(String varsityUpdateDate) {
        this.varsityUpdateDate = varsityUpdateDate;
    }

    /**
     * @return the coursesList
     */
    public List<CoursesInfo> getCoursesList() {
        return coursesList;
    }

    /**
     * @param coursesList the coursesList to set
     */
    public void setCoursesList(List<CoursesInfo> coursesList) {
        this.coursesList = coursesList;
    }

    /**
     * @return the countryInfos
     */
    public CountryInfo getCountryInfos() {
        return countryInfos;
    }

    /**
     * @param countryInfos the countryInfos to set
     */
    public void setCountryInfos(CountryInfo countryInfos) {
        this.countryInfos = countryInfos;
    }

    /**
     * @return the courseStatus
     */
    public Character getCourseStatus() {
        return courseStatus;
    }

    /**
     * @param courseStatus the courseStatus to set
     */
    public void setCourseStatus(Character courseStatus) {
        this.courseStatus = courseStatus;
    }
}
