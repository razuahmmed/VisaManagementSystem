package com.persistance;

public class ApplicantCoursesInfo {

    private Integer courseTableID;
    private String courseID;
    private String applicantVarsityID;
    private String appCourseStatus;
    private VarsityInfo varsity;

    /**
     * @return the courseID
     */
    public String getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    /**
     * @return the applicantVarsityID
     */
    public String getApplicantVarsityID() {
        return applicantVarsityID;
    }

    /**
     * @param applicantVarsityID the applicantVarsityID to set
     */
    public void setApplicantVarsityID(String applicantVarsityID) {
        this.applicantVarsityID = applicantVarsityID;
    }

    /**
     * @return the appCourseStatus
     */
    public String getAppCourseStatus() {
        return appCourseStatus;
    }

    /**
     * @param appCourseStatus the appCourseStatus to set
     */
    public void setAppCourseStatus(String appCourseStatus) {
        this.appCourseStatus = appCourseStatus;
    }

    /**
     * @return the varsity
     */
    public VarsityInfo getVarsity() {
        return varsity;
    }

    /**
     * @param varsity the varsity to set
     */
    public void setVarsity(VarsityInfo varsity) {
        this.varsity = varsity;
    }

    /**
     * @return the courseTableID
     */
    public Integer getCourseTableID() {
        return courseTableID;
    }

    /**
     * @param courseTableID the courseTableID to set
     */
    public void setCourseTableID(Integer courseTableID) {
        this.courseTableID = courseTableID;
    }
}
