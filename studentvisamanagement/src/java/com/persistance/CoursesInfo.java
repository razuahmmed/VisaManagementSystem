package com.persistance;

public class CoursesInfo {

    private Integer courseID;
    private String courseName;
    private Character courseStatus;

    /**
     * @return the courseID
     */
    public Integer getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the courseStatus
     */
    public Character getCourseStatus() {
        return courseStatus;
    }

    /**
     * @param courseStatus the courseStatus to set
     */
    public void setCourseStatus(Character courseStatus) {
        this.courseStatus = courseStatus;
    }
}
