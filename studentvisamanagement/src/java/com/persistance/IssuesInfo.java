package com.persistance;

public class IssuesInfo {

    private Integer issuesId;
    private String issuesTitle;
    private String issuesDetails;
    private Character issuesStatus;
    private String commentDetails;
    private Character commentStatus;
    private String issuesInsertBy;
    private String issuesInsertDate;
    private String issuesUpdateBy;
    private String issueUpdateDate;
    private String commentBy;
    private String commentDate;
    private String commentUpdateBy;
    private String commentUpdateDate;
    private UserInfo userInfo;
    private ApplicantInfo applicantInfo;

    /**
     * @return the issuesId
     */
    public Integer getIssuesId() {
        return issuesId;
    }

    /**
     * @param issuesId the issuesId to set
     */
    public void setIssuesId(Integer issuesId) {
        this.issuesId = issuesId;
    }

    /**
     * @return the issuesTitle
     */
    public String getIssuesTitle() {
        return issuesTitle;
    }

    /**
     * @param issuesTitle the issuesTitle to set
     */
    public void setIssuesTitle(String issuesTitle) {
        this.issuesTitle = issuesTitle;
    }

    /**
     * @return the issuesDetails
     */
    public String getIssuesDetails() {
        return issuesDetails;
    }

    /**
     * @param issuesDetails the issuesDetails to set
     */
    public void setIssuesDetails(String issuesDetails) {
        this.issuesDetails = issuesDetails;
    }

    /**
     * @return the issuesStatus
     */
    public Character getIssuesStatus() {
        return issuesStatus;
    }

    /**
     * @param issuesStatus the issuesStatus to set
     */
    public void setIssuesStatus(Character issuesStatus) {
        this.issuesStatus = issuesStatus;
    }

    /**
     * @return the commentDetails
     */
    public String getCommentDetails() {
        return commentDetails;
    }

    /**
     * @param commentDetails the commentDetails to set
     */
    public void setCommentDetails(String commentDetails) {
        this.commentDetails = commentDetails;
    }

    /**
     * @return the commentStatus
     */
    public Character getCommentStatus() {
        return commentStatus;
    }

    /**
     * @param commentStatus the commentStatus to set
     */
    public void setCommentStatus(Character commentStatus) {
        this.commentStatus = commentStatus;
    }

    /**
     * @return the issuesInsertBy
     */
    public String getIssuesInsertBy() {
        return issuesInsertBy;
    }

    /**
     * @param issuesInsertBy the issuesInsertBy to set
     */
    public void setIssuesInsertBy(String issuesInsertBy) {
        this.issuesInsertBy = issuesInsertBy;
    }

    /**
     * @return the issuesInsertDate
     */
    public String getIssuesInsertDate() {
        return issuesInsertDate;
    }

    /**
     * @param issuesInsertDate the issuesInsertDate to set
     */
    public void setIssuesInsertDate(String issuesInsertDate) {
        this.issuesInsertDate = issuesInsertDate;
    }

    /**
     * @return the issuesUpdateBy
     */
    public String getIssuesUpdateBy() {
        return issuesUpdateBy;
    }

    /**
     * @param issuesUpdateBy the issuesUpdateBy to set
     */
    public void setIssuesUpdateBy(String issuesUpdateBy) {
        this.issuesUpdateBy = issuesUpdateBy;
    }

    /**
     * @return the issueUpdateDate
     */
    public String getIssueUpdateDate() {
        return issueUpdateDate;
    }

    /**
     * @param issueUpdateDate the issueUpdateDate to set
     */
    public void setIssueUpdateDate(String issueUpdateDate) {
        this.issueUpdateDate = issueUpdateDate;
    }

    /**
     * @return the commentBy
     */
    public String getCommentBy() {
        return commentBy;
    }

    /**
     * @param commentBy the commentBy to set
     */
    public void setCommentBy(String commentBy) {
        this.commentBy = commentBy;
    }

    /**
     * @return the commentDate
     */
    public String getCommentDate() {
        return commentDate;
    }

    /**
     * @param commentDate the commentDate to set
     */
    public void setCommentDate(String commentDate) {
        this.commentDate = commentDate;
    }

    /**
     * @return the commentUpdateBy
     */
    public String getCommentUpdateBy() {
        return commentUpdateBy;
    }

    /**
     * @param commentUpdateBy the commentUpdateBy to set
     */
    public void setCommentUpdateBy(String commentUpdateBy) {
        this.commentUpdateBy = commentUpdateBy;
    }

    /**
     * @return the commentUpdateDate
     */
    public String getCommentUpdateDate() {
        return commentUpdateDate;
    }

    /**
     * @param commentUpdateDate the commentUpdateDate to set
     */
    public void setCommentUpdateDate(String commentUpdateDate) {
        this.commentUpdateDate = commentUpdateDate;
    }

    /**
     * @return the userInfo
     */
    public UserInfo getUserInfo() {
        return userInfo;
    }

    /**
     * @param userInfo the userInfo to set
     */
    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    /**
     * @return the applicantInfo
     */
    public ApplicantInfo getApplicantInfo() {
        return applicantInfo;
    }

    /**
     * @param applicantInfo the applicantInfo to set
     */
    public void setApplicantInfo(ApplicantInfo applicantInfo) {
        this.applicantInfo = applicantInfo;
    }
}
