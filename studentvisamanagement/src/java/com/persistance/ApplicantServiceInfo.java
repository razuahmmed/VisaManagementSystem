package com.persistance;

public class ApplicantServiceInfo {

    private Integer appServiceID;
    private Double tutionBasic;
    private Double tutionAgree;
    private Double othersBasic;
    private Double othersAgree;
    private Character tuitionStatus;
    private Character othersStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;

    /**
     * @return the appServiceID
     */
    public Integer getAppServiceID() {
        return appServiceID;
    }

    /**
     * @param appServiceID the appServiceID to set
     */
    public void setAppServiceID(Integer appServiceID) {
        this.appServiceID = appServiceID;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the tutionBasic
     */
    public Double getTutionBasic() {
        return tutionBasic;
    }

    /**
     * @param tutionBasic the tutionBasic to set
     */
    public void setTutionBasic(Double tutionBasic) {
        this.tutionBasic = tutionBasic;
    }

    /**
     * @return the tutionAgree
     */
    public Double getTutionAgree() {
        return tutionAgree;
    }

    /**
     * @param tutionAgree the tutionAgree to set
     */
    public void setTutionAgree(Double tutionAgree) {
        this.tutionAgree = tutionAgree;
    }

    /**
     * @return the othersBasic
     */
    public Double getOthersBasic() {
        return othersBasic;
    }

    /**
     * @param othersBasic the othersBasic to set
     */
    public void setOthersBasic(Double othersBasic) {
        this.othersBasic = othersBasic;
    }

    /**
     * @return the othersAgree
     */
    public Double getOthersAgree() {
        return othersAgree;
    }

    /**
     * @param othersAgree the othersAgree to set
     */
    public void setOthersAgree(Double othersAgree) {
        this.othersAgree = othersAgree;
    }

    /**
     * @return the othersStatus
     */
    public Character getOthersStatus() {
        return othersStatus;
    }

    /**
     * @param othersStatus the othersStatus to set
     */
    public void setOthersStatus(Character othersStatus) {
        this.othersStatus = othersStatus;
    }

    /**
     * @return the tuitionStatus
     */
    public Character getTuitionStatus() {
        return tuitionStatus;
    }

    /**
     * @param tuitionStatus the tuitionStatus to set
     */
    public void setTuitionStatus(Character tuitionStatus) {
        this.tuitionStatus = tuitionStatus;
    }
}
