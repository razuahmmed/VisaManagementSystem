package com.persistance;

public class ApplicantEducationInfo {

    private Integer appEducationID;
    private String degreeName;
    private String result;
    private String passingYear;
    private String instituteName;

    /**
     * @return the appEducationID
     */
    public Integer getAppEducationID() {
        return appEducationID;
    }

    /**
     * @param appEducationID the appEducationID to set
     */
    public void setAppEducationID(Integer appEducationID) {
        this.appEducationID = appEducationID;
    }

    /**
     * @return the degreeName
     */
    public String getDegreeName() {
        return degreeName;
    }

    /**
     * @param degreeName the degreeName to set
     */
    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the passingYear
     */
    public String getPassingYear() {
        return passingYear;
    }

    /**
     * @param passingYear the passingYear to set
     */
    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    /**
     * @return the instituteName
     */
    public String getInstituteName() {
        return instituteName;
    }

    /**
     * @param instituteName the instituteName to set
     */
    public void setInstituteName(String instituteName) {
        this.instituteName = instituteName;
    }
}
