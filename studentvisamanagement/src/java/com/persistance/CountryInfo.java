package com.persistance;

public class CountryInfo {

    private Integer countryID;
    private String countryName;
    private Character countryStatus;
    private String countryInsertBy;
    private String countryInsertDate;
    private String countryUpdateBy;
    private String countryUpdateDate;

    /**
     * @return the countryID
     */
    public Integer getCountryID() {
        return countryID;
    }

    /**
     * @param countryID the countryID to set
     */
    public void setCountryID(Integer countryID) {
        this.countryID = countryID;
    }

    /**
     * @return the countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @param countryName the countryName to set
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * @return the countryStatus
     */
    public Character getCountryStatus() {
        return countryStatus;
    }

    /**
     * @param countryStatus the countryStatus to set
     */
    public void setCountryStatus(Character countryStatus) {
        this.countryStatus = countryStatus;
    }

    /**
     * @return the countryInsertBy
     */
    public String getCountryInsertBy() {
        return countryInsertBy;
    }

    /**
     * @param countryInsertBy the countryInsertBy to set
     */
    public void setCountryInsertBy(String countryInsertBy) {
        this.countryInsertBy = countryInsertBy;
    }

    /**
     * @return the countryInsertDate
     */
    public String getCountryInsertDate() {
        return countryInsertDate;
    }

    /**
     * @param countryInsertDate the countryInsertDate to set
     */
    public void setCountryInsertDate(String countryInsertDate) {
        this.countryInsertDate = countryInsertDate;
    }

    /**
     * @return the countryUpdateBy
     */
    public String getCountryUpdateBy() {
        return countryUpdateBy;
    }

    /**
     * @param countryUpdateBy the countryUpdateBy to set
     */
    public void setCountryUpdateBy(String countryUpdateBy) {
        this.countryUpdateBy = countryUpdateBy;
    }

    /**
     * @return the countryUpdateDate
     */
    public String getCountryUpdateDate() {
        return countryUpdateDate;
    }

    /**
     * @param countryUpdateDate the countryUpdateDate to set
     */
    public void setCountryUpdateDate(String countryUpdateDate) {
        this.countryUpdateDate = countryUpdateDate;
    }
}
