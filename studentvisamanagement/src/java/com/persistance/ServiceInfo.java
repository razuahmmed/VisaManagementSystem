package com.persistance;

public class ServiceInfo {

    private Integer serviceID;
    private String serviceName;
    private String serviceAmount;
    private Character serviceStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;

    /**
     * @return the serviceID
     */
    public Integer getServiceID() {
        return serviceID;
    }

    /**
     * @param serviceID the serviceID to set
     */
    public void setServiceID(Integer serviceID) {
        this.serviceID = serviceID;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the serviceAmount
     */
    public String getServiceAmount() {
        return serviceAmount;
    }

    /**
     * @param serviceAmount the serviceAmount to set
     */
    public void setServiceAmount(String serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    /**
     * @return the serviceStatus
     */
    public Character getServiceStatus() {
        return serviceStatus;
    }

    /**
     * @param serviceStatus the serviceStatus to set
     */
    public void setServiceStatus(Character serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }
}
