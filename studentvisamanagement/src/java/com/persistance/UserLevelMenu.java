package com.persistance;

import java.util.List;

public class UserLevelMenu {

    private Integer userLevelMenuID;
    private Character levelMenuStatus;
    private String insertBy;
    private String insertDate;
    private String updateBy;
    private String updateDate;
    private MenuMaster menuMaster;
    private MenuChild menuChild;
    private List<MenuChild> userLevelMenuChildList;
    private UserGroup userGroupInfo;

    /**
     * @return the userLevelMenuID
     */
    public Integer getUserLevelMenuID() {
        return userLevelMenuID;
    }

    /**
     * @param userLevelMenuID the userLevelMenuID to set
     */
    public void setUserLevelMenuID(Integer userLevelMenuID) {
        this.userLevelMenuID = userLevelMenuID;
    }

    /**
     * @return the levelMenuStatus
     */
    public Character getLevelMenuStatus() {
        return levelMenuStatus;
    }

    /**
     * @param levelMenuStatus the levelMenuStatus to set
     */
    public void setLevelMenuStatus(Character levelMenuStatus) {
        this.levelMenuStatus = levelMenuStatus;
    }

    /**
     * @return the insertBy
     */
    public String getInsertBy() {
        return insertBy;
    }

    /**
     * @param insertBy the insertBy to set
     */
    public void setInsertBy(String insertBy) {
        this.insertBy = insertBy;
    }

    /**
     * @return the insertDate
     */
    public String getInsertDate() {
        return insertDate;
    }

    /**
     * @param insertDate the insertDate to set
     */
    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    /**
     * @return the updateBy
     */
    public String getUpdateBy() {
        return updateBy;
    }

    /**
     * @param updateBy the updateBy to set
     */
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    /**
     * @return the updateDate
     */
    public String getUpdateDate() {
        return updateDate;
    }

    /**
     * @param updateDate the updateDate to set
     */
    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * @return the userGroupInfo
     */
    public UserGroup getUserGroupInfo() {
        return userGroupInfo;
    }

    /**
     * @param userGroupInfo the userGroupInfo to set
     */
    public void setUserGroupInfo(UserGroup userGroupInfo) {
        this.userGroupInfo = userGroupInfo;
    }

    /**
     * @return the menuMaster
     */
    public MenuMaster getMenuMaster() {
        return menuMaster;
    }

    /**
     * @param menuMaster the menuMaster to set
     */
    public void setMenuMaster(MenuMaster menuMaster) {
        this.menuMaster = menuMaster;
    }

    /**
     * @return the menuChild
     */
    public MenuChild getMenuChild() {
        return menuChild;
    }

    /**
     * @param menuChild the menuChild to set
     */
    public void setMenuChild(MenuChild menuChild) {
        this.menuChild = menuChild;
    }

    /**
     * @return the userLevelMenuChildList
     */
    public List<MenuChild> getUserLevelMenuChildList() {
        return userLevelMenuChildList;
    }

    /**
     * @param userLevelMenuChildList the userLevelMenuChildList to set
     */
    public void setUserLevelMenuChildList(List<MenuChild> userLevelMenuChildList) {
        this.userLevelMenuChildList = userLevelMenuChildList;
    }
}
