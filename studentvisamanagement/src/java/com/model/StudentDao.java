package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentDao {

    public int addNewApplicant(
            Connection connection,
            String applicantID,
            String applicantName,
            String nationality,
            String passportNumber,
            String alternatePassportNo,
            String dob,
            String passportIssuedPlace,
            String passportIssueDate,
            String passportExpiryDate,
            String gender,
            String applicantEmail,
            String applicantAlternateEmail,
            String maritalStatus,
            String contact,
            String city,
            String postalCode,
            String state,
            String country,
            String residentNumber,
            String permanentAddress,
            String corContact,
            String corCity,
            String corPostalCode,
            String corState,
            String corCountry,
            String corResidentNumber,
            String corAddress,
            String fathersName,
            String fathersOccupation,
            String mothersName,
            String mothersOccupation,
            String highestQualification,
            String hqPassYear,
            String overAllGrade,
            String visaCountry,
            String visaExpiryDate,
            String visaReleaseLetter,
            String visaCollegeName,
            String visaType,
            String arrivalDate,
            String appType,
            String agentID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String appInsertStatement = " INSERT INTO "
                        + " applicant_info ( "
                        + " APPLICANT_ID, "
                        + " APPLICANT_NAME, "
                        + " APP_NATIONALITY, "
                        + " APP_PASSPORT_NO, "
                        + " APP_ALT_PASSPORT_NO, "
                        + " APP_DOB, "
                        + " PASS_ISSUED_PLACE, "
                        + " PASS_ISSUED_DATE, "
                        + " PASS_EXPIRY_DATE, "
                        + " APP_GENDER, "
                        + " APP_EMAIL, "
                        + " APP_ALT_EMAIL, "
                        + " APP_MARITAL_STATUS, "
                        + " APP_CONTACT, "
                        + " APP_CITY, "
                        + " APP_POSTAL_CODE, "
                        + " APP_STATE, "
                        + " APP_COUNTRY, "
                        + " APP_RESD_NO, "
                        + " APP_PERM_ADDR, "
                        + " COR_CONTACT, "
                        + " COR_CITY, "
                        + " COR_POSTAL_CODE, "
                        + " COR_SATE, "
                        + " COR_COUNTRY, "
                        + " COR_RESD_NO, "
                        + " COR_ADDRESS, "
                        + " APP_FATHER, "
                        + " FATHER_OCCUPATION, "
                        + " APP_MOTHER, "
                        + " MOTHER_OCCUPATION, "
                        + " HIGHT_QUALI, "
                        + " PASS_YEAR, "
                        + " OVER_ALL_GRADE, "
                        + " VISA_COUNTRY, "
                        + " VISA_EX_DATE, "
                        + " VISA_LETTER_ISSUE, "
                        + " VISA_COLLEGE, "
                        + " VISA_TYPE, "
                        + " ARRIVAL_DATE, "
                        + " APP_TYPE, "
                        + " USER_ID, "
                        + " APP_INSERT_BY, "
                        + " APP_INSERT_DATE "
                        + " )VALUES( "
                        + " '" + applicantID + "', "
                        + " '" + applicantName + "', "
                        + " '" + nationality + "', "
                        + " '" + passportNumber + "', "
                        + " '" + alternatePassportNo + "', "
                        + " '" + dob + "', "
                        + " '" + passportIssuedPlace + "', "
                        + " '" + passportIssueDate + "', "
                        + " '" + passportExpiryDate + "', "
                        + " '" + gender + "', "
                        + " '" + applicantEmail + "', "
                        + " '" + applicantAlternateEmail + "', "
                        + " '" + maritalStatus + "', "
                        + " '" + contact + "', "
                        + " '" + city + "', "
                        + " '" + postalCode + "', "
                        + " '" + state + "', "
                        + " '" + country + "', "
                        + " '" + residentNumber + "', "
                        + " '" + permanentAddress + "', "
                        + " '" + corContact + "', "
                        + " '" + corCity + "', "
                        + " '" + corPostalCode + "', "
                        + " '" + corState + "', "
                        + " '" + corCountry + "', "
                        + " '" + corResidentNumber + "', "
                        + " '" + corAddress + "', "
                        + " '" + fathersName + "', "
                        + " '" + fathersOccupation + "', "
                        + " '" + mothersName + "', "
                        + " '" + mothersOccupation + "', "
                        + " '" + highestQualification + "', "
                        + " '" + hqPassYear + "', "
                        + " '" + overAllGrade + "', "
                        + " '" + visaCountry + "', "
                        + " '" + visaExpiryDate + "', "
                        + " '" + visaReleaseLetter + "', "
                        + " '" + visaCollegeName + "', "
                        + " '" + visaType + "', "
                        + " '" + arrivalDate + "', "
                        + " '" + appType + "', "
                        + " '" + agentID + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(appInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int addNewWorker(
            Connection connection,
            String applicantID,
            String applicantName,
            String passportNumber,
            String dob,
            String country,
            String appType,
            String sector,
            String agentID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String appInsertStatement = " INSERT INTO "
                        + " applicant_info ( "
                        + " APPLICANT_ID, "
                        + " APPLICANT_NAME, "
                        + " APP_PASSPORT_NO, "
                        + " APP_DOB, "
                        + " APP_COUNTRY, "
                        + " APP_TYPE, "
                        + " SECTOR, "
                        + " USER_ID, "
                        + " APP_INSERT_BY, "
                        + " APP_INSERT_DATE "
                        + " )VALUES( "
                        + " '" + applicantID + "', "
                        + " '" + applicantName + "', "
                        + " '" + passportNumber + "', "
                        + " '" + dob + "', "
                        + " '" + country + "', "
                        + " '" + appType + "', "
                        + " '" + sector + "', "
                        + " '" + agentID + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(appInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int applicantEducation(
            Connection connection,
            String degreeName,
            String result,
            String passingYear,
            String institution,
            String applicantID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String eduInsertStatement = " INSERT INTO "
                        + " applicant_education ( "
                        + " DEGREE_NAME, "
                        + " RESULT, "
                        + " PASSING_YEAR, "
                        + " INSTITUTION, "
                        + " APPLICANT_ID "
                        + " )VALUES( "
                        + " '" + degreeName + "', "
                        + " '" + result + "', "
                        + " '" + passingYear + "', "
                        + " '" + institution + "', "
                        + " '" + applicantID + "')";

                ps = connection.prepareStatement(eduInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int applicantCourse(
            Connection connection,
            String appCourseID,
            String appVarsiryID,
            String applicantID) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String courseInsertStatement = " INSERT INTO "
                        + " applicant_courses ( "
                        + " COURSE_ID, "
                        + " VARCITY_ID, "
                        + " APPLICANT_ID "
                        + " )VALUES( "
                        + " '" + appCourseID + "', "
                        + " '" + appVarsiryID + "', "
                        + " '" + applicantID + "')";

                ps = connection.prepareStatement(courseInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int applicantPaymentReceive(
            Connection connection,
            String payMode,
            String payDate,
            Double payAmount,
            Double convRate,
            String bankName,
            String chkRef,
            String chkDate,
            String courrency,
            String service,
            String applicantID,
            String agentID,
            String insertBy) {

        int status = 0;
        int updateStatus = 0;

        PreparedStatement ps = null;
        PreparedStatement psUpdate = null;

        if (connection != null) {

            try {

                String insertPayHistory = "";
                String insertPayment = "";
                String updatePayment = "";

                if (service.equals("1")) {
                    insertPayHistory = " INSERT INTO "
                            + " payment_history ( "
                            + " PAYMENT_MODE, "
                            + " PAYMENT_DATE, "
                            + " TUITION_FEE, "
                            + " CONVERSION_RATE, "
                            + " BANK_NAME, "
                            + " CHEQUE_REFERENCE, "
                            + " CHEQUE_DATE, "
                            + " CURRENCY, "
                            + " SERVICE_ID, "
                            + " APPLICANT_ID, "
                            + " USER_ID, "
                            + " INSERT_BY, "
                            + " INSERT_DATE "
                            + " )VALUES( "
                            + " '" + payMode + "', "
                            + " '" + payDate + "', "
                            + " '" + payAmount + "', "
                            + " '" + convRate + "', "
                            + " '" + bankName + "', "
                            + " '" + chkRef + "', "
                            + " '" + chkDate + "', "
                            + " '" + courrency + "', "
                            + " '" + service + "', "
                            + " '" + applicantID + "', "
                            + " '" + agentID + "', "
                            + " '" + insertBy + "', "
                            + " CURRENT_TIMESTAMP)";

                    updatePayment = " UPDATE "
                            + " payment_info "
                            + " SET "
                            + " PAYMENT_MODE = '" + payMode + "', "
                            + " PAYMENT_DATE = '" + payDate + "', "
                            + " TUITION_FEE = TUITION_FEE + " + payAmount + ", "
                            + " CONVERSION_RATE = " + convRate + ", "
                            + " BANK_NAME = '" + bankName + "', "
                            + " CHEQUE_REFERENCE = '" + chkRef + "', "
                            + " CHEQUE_DATE = '" + chkDate + "', "
                            + " CURRENCY = '" + courrency + "', "
                            + " SERVICE_ID = '" + service + "', "
                            + " UPDATE_BY = '" + insertBy + "', "
                            + " UPDATE_DATE = CURRENT_TIMESTAMP "
                            + " WHERE "
                            + " APPLICANT_ID = '" + applicantID + "' ";

                    psUpdate = connection.prepareStatement(updatePayment);
                    updateStatus = psUpdate.executeUpdate();

                    if (updateStatus == 0) {

                        insertPayment = " INSERT INTO "
                                + " payment_info ( "
                                + " PAYMENT_MODE, "
                                + " PAYMENT_DATE, "
                                + " TUITION_FEE, "
                                + " CONVERSION_RATE, "
                                + " BANK_NAME, "
                                + " CHEQUE_REFERENCE, "
                                + " CHEQUE_DATE, "
                                + " CURRENCY, "
                                + " SERVICE_ID, "
                                + " APPLICANT_ID, "
                                + " USER_ID, "
                                + " INSERT_BY, "
                                + " INSERT_DATE "
                                + " )VALUES( "
                                + " '" + payMode + "', "
                                + " '" + payDate + "', "
                                + " '" + payAmount + "', "
                                + " '" + convRate + "', "
                                + " '" + bankName + "', "
                                + " '" + chkRef + "', "
                                + " '" + chkDate + "', "
                                + " '" + courrency + "', "
                                + " '" + service + "', "
                                + " '" + applicantID + "', "
                                + " '" + agentID + "', "
                                + " '" + insertBy + "', "
                                + " CURRENT_TIMESTAMP)";

                        psUpdate = connection.prepareStatement(insertPayment);
                        updateStatus = psUpdate.executeUpdate();
                    }
                } else if (service.equals("2")) {
                    insertPayHistory = " INSERT INTO "
                            + " payment_history ( "
                            + " PAYMENT_MODE, "
                            + " PAYMENT_DATE, "
                            + " OTHERS_AMOUNT, "
                            + " CONVERSION_RATE, "
                            + " BANK_NAME, "
                            + " CHEQUE_REFERENCE, "
                            + " CHEQUE_DATE, "
                            + " CURRENCY, "
                            + " SERVICE_ID, "
                            + " APPLICANT_ID, "
                            + " USER_ID, "
                            + " INSERT_BY, "
                            + " INSERT_DATE "
                            + " )VALUES( "
                            + " '" + payMode + "', "
                            + " '" + payDate + "', "
                            + " '" + payAmount + "', "
                            + " '" + convRate + "', "
                            + " '" + bankName + "', "
                            + " '" + chkRef + "', "
                            + " '" + chkDate + "', "
                            + " '" + courrency + "', "
                            + " '" + service + "', "
                            + " '" + applicantID + "', "
                            + " '" + agentID + "', "
                            + " '" + insertBy + "', "
                            + " CURRENT_TIMESTAMP)";

                    updatePayment = " UPDATE "
                            + " payment_info "
                            + " SET "
                            + " PAYMENT_MODE = '" + payMode + "', "
                            + " PAYMENT_DATE = '" + payDate + "', "
                            + " OTHERS_AMOUNT = OTHERS_AMOUNT + " + payAmount + ", "
                            + " CONVERSION_RATE = " + convRate + ", "
                            + " BANK_NAME = '" + bankName + "', "
                            + " CHEQUE_REFERENCE = '" + chkRef + "', "
                            + " CHEQUE_DATE = '" + chkDate + "', "
                            + " CURRENCY = '" + courrency + "', "
                            + " SERVICE_ID = '" + service + "', "
                            + " UPDATE_BY = '" + insertBy + "', "
                            + " UPDATE_DATE = CURRENT_TIMESTAMP "
                            + " WHERE "
                            + " APPLICANT_ID = '" + applicantID + "' ";

                    psUpdate = connection.prepareStatement(updatePayment);
                    updateStatus = psUpdate.executeUpdate();

                    if (updateStatus == 0) {

                        insertPayment = " INSERT INTO "
                                + " payment_info ( "
                                + " PAYMENT_MODE, "
                                + " PAYMENT_DATE, "
                                + " OTHERS_AMOUNT, "
                                + " CONVERSION_RATE, "
                                + " BANK_NAME, "
                                + " CHEQUE_REFERENCE, "
                                + " CHEQUE_DATE, "
                                + " CURRENCY, "
                                + " SERVICE_ID, "
                                + " APPLICANT_ID, "
                                + " USER_ID, "
                                + " INSERT_BY, "
                                + " INSERT_DATE "
                                + " )VALUES( "
                                + " '" + payMode + "', "
                                + " '" + payDate + "', "
                                + " '" + payAmount + "', "
                                + " '" + convRate + "', "
                                + " '" + bankName + "', "
                                + " '" + chkRef + "', "
                                + " '" + chkDate + "', "
                                + " '" + courrency + "', "
                                + " '" + service + "', "
                                + " '" + applicantID + "', "
                                + " '" + agentID + "', "
                                + " '" + insertBy + "', "
                                + " CURRENT_TIMESTAMP)";

                        psUpdate = connection.prepareStatement(insertPayment);
                        updateStatus = psUpdate.executeUpdate();
                    }
                } else if (service.equals("3")) {
                    insertPayHistory = " INSERT INTO "
                            + " payment_history ( "
                            + " PAYMENT_MODE, "
                            + " PAYMENT_DATE, "
                            + " WORKER_AMOUNT, "
                            + " CONVERSION_RATE, "
                            + " BANK_NAME, "
                            + " CHEQUE_REFERENCE, "
                            + " CHEQUE_DATE, "
                            + " CURRENCY, "
                            + " SERVICE_ID, "
                            + " APPLICANT_ID, "
                            + " USER_ID, "
                            + " INSERT_BY, "
                            + " INSERT_DATE "
                            + " )VALUES( "
                            + " '" + payMode + "', "
                            + " '" + payDate + "', "
                            + " '" + payAmount + "', "
                            + " '" + convRate + "', "
                            + " '" + bankName + "', "
                            + " '" + chkRef + "', "
                            + " '" + chkDate + "', "
                            + " '" + courrency + "', "
                            + " '" + service + "', "
                            + " '" + applicantID + "', "
                            + " '" + agentID + "', "
                            + " '" + insertBy + "', "
                            + " CURRENT_TIMESTAMP)";

                    updatePayment = " UPDATE "
                            + " payment_info "
                            + " SET "
                            + " PAYMENT_MODE = '" + payMode + "', "
                            + " PAYMENT_DATE = '" + payDate + "', "
                            + " WORKER_AMOUNT = WORKER_AMOUNT + " + payAmount + ", "
                            + " CONVERSION_RATE = " + convRate + ", "
                            + " BANK_NAME = '" + bankName + "', "
                            + " CHEQUE_REFERENCE = '" + chkRef + "', "
                            + " CHEQUE_DATE = '" + chkDate + "', "
                            + " CURRENCY = '" + courrency + "', "
                            + " SERVICE_ID = '" + service + "', "
                            + " UPDATE_BY = '" + insertBy + "', "
                            + " UPDATE_DATE = CURRENT_TIMESTAMP "
                            + " WHERE "
                            + " APPLICANT_ID = '" + applicantID + "' ";

                    psUpdate = connection.prepareStatement(updatePayment);
                    updateStatus = psUpdate.executeUpdate();

                    if (updateStatus == 0) {

                        insertPayment = " INSERT INTO "
                                + " payment_info ( "
                                + " PAYMENT_MODE, "
                                + " PAYMENT_DATE, "
                                + " WORKER_AMOUNT, "
                                + " CONVERSION_RATE, "
                                + " BANK_NAME, "
                                + " CHEQUE_REFERENCE, "
                                + " CHEQUE_DATE, "
                                + " CURRENCY, "
                                + " SERVICE_ID, "
                                + " APPLICANT_ID, "
                                + " USER_ID, "
                                + " INSERT_BY, "
                                + " INSERT_DATE "
                                + " )VALUES( "
                                + " '" + payMode + "', "
                                + " '" + payDate + "', "
                                + " '" + payAmount + "', "
                                + " '" + convRate + "', "
                                + " '" + bankName + "', "
                                + " '" + chkRef + "', "
                                + " '" + chkDate + "', "
                                + " '" + courrency + "', "
                                + " '" + service + "', "
                                + " '" + applicantID + "', "
                                + " '" + agentID + "', "
                                + " '" + insertBy + "', "
                                + " CURRENT_TIMESTAMP)";

                        psUpdate = connection.prepareStatement(insertPayment);
                        updateStatus = psUpdate.executeUpdate();
                    }
                }

                ps = connection.prepareStatement(insertPayHistory);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
                updateStatus = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
                if (psUpdate != null) {
                    try {
                        psUpdate.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
