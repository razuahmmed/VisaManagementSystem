package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ProfileDao {

    public int updateUserProfile(
            Connection connection,
            String fullName,
            String phoneNumber,
            String faxNumber,
            String email,
            String dob,
            String address,
            String country,
            String company,
            String updateBy,
            String userid) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String userUpdateStatement = " UPDATE "
                        + " user_info "
                        + " SET "
                        + " USER_FULL_NAME = '" + fullName + "', "
                        + " USER_PHONE = '" + phoneNumber + "', "
                        + " USER_FAX = '" + faxNumber + "', "
                        + " USER_EMAIL = '" + email + "', "
                        + " USER_DOB = '" + dob + "', "
                        + " USER_ADDRESS = '" + address + "', "
                        + " COUNTRY_ID = '" + country + "', "
                        + " COMPANY_ID = '" + company + "', "
                        + " UPDATED_BY = '" + updateBy + "', "
                        + " UPDATED_DATE = CURRENT_TIMESTAMP "
                        + " WHERE USER_ID = '" + userid + "'";

                ps = connection.prepareStatement(userUpdateStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateUserPassword(
            Connection connection,
            String newPassword,
            String updateBy,
            String userid) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String userUpdateStatement = " UPDATE "
                        + " user_info "
                        + " SET "
                        + " USER_PASSWORD = '" + newPassword + "', "
                        + " UPDATED_BY = '" + updateBy + "', "
                        + " UPDATED_DATE = CURRENT_TIMESTAMP "
                        + " WHERE USER_ID = '" + userid + "'";

                ps = connection.prepareStatement(userUpdateStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
