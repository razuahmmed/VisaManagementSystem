package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SettingsDao {

    public int addNewVarsity(
            Connection connection,
            String varsityID,
            String varsityName,
            String varsityContact,
            String varsityEmail,
            String varsityCountry,
            String varsityAddress,
            String varsityNotes,
            String courseID,
            String courseName,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String varsityInsertStatement = " INSERT INTO "
                        + " varsity_info ( "
                        + " VARSITY_ID, "
                        + " VARSITY_NAME, "
                        + " VARSITY_CONTACT, "
                        + " VARSITY_EMAIL, "
                        + " VARSITY_COUNTRY, "
                        + " VARSITY_ADDRESS, "
                        + " VARSITY_NOTES, "
                        + " COURSE_ID, "
                        + " COURSE_NAME, "
                        + " VARSITY_INSERT_BY, "
                        + " VARSITY_INSERT_DATE "
                        + " )VALUES( "
                        + " '" + varsityID + "', "
                        + " '" + varsityName + "', "
                        + " '" + varsityContact + "', "
                        + " '" + varsityEmail + "', "
                        + " '" + varsityCountry + "', "
                        + " '" + varsityAddress + "', "
                        + " '" + varsityNotes + "', "
                        + " '" + courseID + "', "
                        + " '" + courseName + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(varsityInsertStatement);
                status = ps.executeUpdate();
                // ALTER TABLE varsity_info ADD CONSTRAINT constraint_name PRIMARY KEY (VARSITY_ID,COURSE_ID)
            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateVarsityCourses(
            Connection connection,
            String varsityID,
            String varsityName,
            String varsityContact,
            String varsityEmail,
            String varsityCountry,
            Character varsityStatus,
            String varsityAddress,
            String varsityNotes,
            String courseID,
            String courseName,
            String courseStatus,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String varsityInsertStatement = " UPDATE "
                        + " varsity_info "
                        + " SET "
                        + " VARSITY_NAME = '" + varsityName + "', "
                        + " VARSITY_CONTACT = '" + varsityContact + "', "
                        + " VARSITY_EMAIL = '" + varsityEmail + "', "
                        + " VARSITY_COUNTRY = '" + varsityCountry + "', "
                        + " VARSITY_STATUS = '" + varsityStatus + "', "
                        + " VARSITY_ADDRESS = '" + varsityAddress + "', "
                        + " VARSITY_NOTES = '" + varsityNotes + "', "
                        + " COURSE_NAME = '" + courseName + "', "
                        + " COURSE_STATUS = '" + courseStatus + "', "
                        + " VARSITY_UPDATE_BY = '" + updateBy + "', "
                        + " VARSITY_UPDATE_DATE = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " VARSITY_ID = '" + varsityID + "' "
                        + " AND "
                        + " COURSE_ID = '" + courseID + "' ";

                ps = connection.prepareStatement(varsityInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int addNewServiceInfo(
            Connection connection,
            String serName,
            String serAmount,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String serviceInsertStatement = " INSERT INTO "
                        + " service_info ( "
                        + " SERVICE_NAME, "
                        + " SERVICE_AMOUNT, "
                        + " INSERT_BY, "
                        + " INSERT_DATE "
                        + " )VALUES( "
                        + " '" + serName + "', "
                        + " '" + serAmount + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(serviceInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateServiceInfo(
            Connection connection,
            String serName,
            String serAmount,
            Character serStatus,
            String serviceId,
            String updateBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String serviceInsertStatement = " UPDATE "
                        + " service_info "
                        + " SET "
                        + " SERVICE_NAME = '" + serName + "', "
                        + " SERVICE_AMOUNT = '" + serAmount + "', "
                        + " SERVICE_STATUS = '" + serStatus + "', "
                        + " UPDATE_BY = '" + updateBy + "', "
                        + " UPDATE_DATE = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " SERVICE_ID = '" + serviceId + "' ";

                ps = connection.prepareStatement(serviceInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
