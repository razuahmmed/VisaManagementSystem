package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class IssueDao {

    public int insertIssue(
            Connection connection,
            String issueTitle,
            String issueDetails,
            String appid,
            String agentid,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String issueInsertStatement = " INSERT INTO "
                        + " issues_info ( "
                        + " ISSUE_TITLE, "
                        + " ISSUE_DETAILS, "
                        + " APP_ID, "
                        + " AGENT_ID, "
                        + " ISSUE_INSERT_BY, "
                        + " ISSUE_INSERT_DATE "
                        + " )VALUES( "
                        + " '" + issueTitle + "', "
                        + " '" + issueDetails + "', "
                        + " '" + appid + "', "
                        + " '" + agentid + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(issueInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int onCommentIssues(
            Connection connection,
            String commentDetails,
            String commentBy,
            Integer issuesid) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String issuesUpdateStatement = " UPDATE "
                        + " issues_info "
                        + " SET "
                        + " COMMENT_DETAILS = '" + commentDetails + "', "
                        + " COMMENT_STATUS = 'Y', "
                        + " COMMENT_BY = '" + commentBy + "', "
                        + " COMMENT_DATE = CURRENT_TIMESTAMP "
                        + " WHERE "
                        + " ISSUE_ID = '" + issuesid + "'";

                ps = connection.prepareStatement(issuesUpdateStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int insertDocuments(
            Connection connection,
            String appid,
            String agentid,
            String d1,
            String d2,
            String d3,
            String d4,
            String d5,
            String d6,
            String d7,
            String d8,
            String d9,
            String d10,
            String d11,
            String d12,
            String d13,
            String d14,
            String d15,
            String d16,
            String d17,
            String d18,
            String d19,
            String d20,
            String d21,
            String d22,
            String d23,
            String d24,
            String d25,
            String d26,
            String d27,
            String d28,
            String d29,
            String d30,
            String othersName,
            String othersDoc,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String issueInsertStatement = null;

                issueInsertStatement = " UPDATE "
                        + " document_info "
                        + " SET "
                        + " D1 = '" + d1 + "', "
                        + " D2 = '" + d2 + "', "
                        + " D3 = '" + d3 + "', "
                        + " D4 = '" + d4 + "', "
                        + " D5 = '" + d5 + "', "
                        + " D6 = '" + d6 + "', "
                        + " D7 = '" + d7 + "', "
                        + " D8 = '" + d8 + "', "
                        + " D9 = '" + d9 + "', "
                        + " D10 = '" + d10 + "', "
                        + " D11 = '" + d11 + "', "
                        + " D12 = '" + d12 + "', "
                        + " D13 = '" + d13 + "', "
                        + " D14 = '" + d14 + "', "
                        + " D15 = '" + d15 + "', "
                        + " D16 = '" + d16 + "', "
                        + " D17 = '" + d17 + "', "
                        + " D18 = '" + d18 + "', "
                        + " D19 = '" + d19 + "', "
                        + " D20 = '" + d20 + "', "
                        + " D21 = '" + d21 + "', "
                        + " D22 = '" + d22 + "', "
                        + " D23 = '" + d23 + "', "
                        + " D24 = '" + d24 + "', "
                        + " D25 = '" + d25 + "', "
                        + " D26 = '" + d26 + "', "
                        + " D27 = '" + d27 + "', "
                        + " D28 = '" + d28 + "', "
                        + " D29 = '" + d29 + "', "
                        + " D30 = '" + d30 + "', "
                        + " OTHERS_DOC_NAME = '" + othersName + "', "
                        + " OTHERS_DOCUMENT = '" + othersDoc + "', "
                        + " UPDATE_BY = '" + insertBy + "', "
                        + " UPDATE_DATE = CURRENT_TIMESTAMP "
                        + " WHERE = '" + appid + "'";

                ps = connection.prepareStatement(issueInsertStatement);
                status = ps.executeUpdate();

                if (status == 0) {

                    issueInsertStatement = " INSERT INTO "
                            + " document_info ( "
                            + " APP_ID, "
                            + " AGENT_ID, "
                            + " D1, "
                            + " D2, "
                            + " D3, "
                            + " D4, "
                            + " D5, "
                            + " D6, "
                            + " D7, "
                            + " D8, "
                            + " D9, "
                            + " D10, "
                            + " D11, "
                            + " D12, "
                            + " D13, "
                            + " D14, "
                            + " D15, "
                            + " D16, "
                            + " D17, "
                            + " D18, "
                            + " D19, "
                            + " D20, "
                            + " D21, "
                            + " D22, "
                            + " D23, "
                            + " D24, "
                            + " D25, "
                            + " D26, "
                            + " D27, "
                            + " D28, "
                            + " D29, "
                            + " D30, "
                            + " OTHERS_DOC_NAME, "
                            + " OTHERS_DOCUMENT, "
                            + " INSERT_BY, "
                            + " INSERT_DATE "
                            + " )VALUES( "
                            + " '" + appid + "', "
                            + " '" + agentid + "', "
                            + " '" + d1 + "', "
                            + " '" + d2 + "', "
                            + " '" + d3 + "', "
                            + " '" + d4 + "', "
                            + " '" + d5 + "', "
                            + " '" + d6 + "', "
                            + " '" + d7 + "', "
                            + " '" + d8 + "', "
                            + " '" + d9 + "', "
                            + " '" + d10 + "', "
                            + " '" + d11 + "', "
                            + " '" + d12 + "', "
                            + " '" + d13 + "', "
                            + " '" + d14 + "', "
                            + " '" + d15 + "', "
                            + " '" + d16 + "', "
                            + " '" + d17 + "', "
                            + " '" + d18 + "', "
                            + " '" + d19 + "', "
                            + " '" + d20 + "', "
                            + " '" + d21 + "', "
                            + " '" + d22 + "', "
                            + " '" + d23 + "', "
                            + " '" + d24 + "', "
                            + " '" + d25 + "', "
                            + " '" + d26 + "', "
                            + " '" + d27 + "', "
                            + " '" + d28 + "', "
                            + " '" + d29 + "', "
                            + " '" + d30 + "', "
                            + " '" + othersName + "', "
                            + " '" + othersDoc + "', "
                            + " '" + insertBy + "', "
                            + " CURRENT_TIMESTAMP)";

                    ps = connection.prepareStatement(issueInsertStatement);
                    status = ps.executeUpdate();
                }
            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
