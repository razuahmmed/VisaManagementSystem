package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ServiceDao {

    public int allocateAppService(
            Connection connection,
            String applicantID,
            String agentID,
            String serviceID,
            String basicAmount,
            String agreeAmount,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String serviceInsertStatement = " INSERT INTO "
                        + " applicant_service ( "
                        + " APPLICANT_ID, "
                        + " USER_ID, "
                        + " SERVICE_ID, "
                        + " BASIC_AMOUNT, "
                        + " AGREE_AMOUNT, "
                        + " INSERT_BY, "
                        + " INSERT_DATE "
                        + " )VALUES( "
                        + " '" + applicantID + "', "
                        + " '" + agentID + "', "
                        + " '" + serviceID + "', "
                        + " '" + basicAmount + "', "
                        + " '" + agreeAmount + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(serviceInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
