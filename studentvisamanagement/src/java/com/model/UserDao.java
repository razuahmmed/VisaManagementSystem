package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserDao {

    public int addNewUserInfo(
            Connection connection,
            String fullName,
            String userName,
            String password,
            String phoneNumber,
            String faxNumber,
            String email,
            String dob,
            String notes,
            String address,
            String countryID,
            String companyID,
            String groupID,
            String insertBy) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String agentInsertStatement = " INSERT INTO "
                        + " user_info ( "
                        + " USER_FULL_NAME, "
                        + " USER_NAME, "
                        + " USER_PASSWORD, "
                        + " USER_PHONE, "
                        + " USER_FAX, "
                        + " USER_EMAIL, "
                        + " USER_DOB, "
                        + " USER_NOTES, "
                        + " USER_ADDRESS, "
                        + " COUNTRY_ID, "
                        + " COMPANY_ID, "
                        + " GROUP_ID, "
                        + " INSERT_BY, "
                        + " INSERT_DATE "
                        + " )VALUES( "
                        + " '" + fullName + "', "
                        + " '" + userName + "', "
                        + " '" + password + "', "
                        + " '" + phoneNumber + "', "
                        + " '" + faxNumber + "', "
                        + " '" + email + "', "
                        + " '" + dob + "', "
                        + " '" + notes + "', "
                        + " '" + address + "', "
                        + " '" + countryID + "', "
                        + " '" + companyID + "', "
                        + " '" + groupID + "', "
                        + " '" + insertBy + "', "
                        + " CURRENT_TIMESTAMP)";

                ps = connection.prepareStatement(agentInsertStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }

    public int updateUserInfo(
            Connection connection,
            String fullName,
            String phoneNumber,
            String faxNumber,
            String email,
            String dob,
            String notes,
            String address,
            String countryID,
            String companyID,
            Character userStatus,
            String updateBy,
            String userid) {

        int status = 0;

        PreparedStatement ps = null;

        if (connection != null) {

            try {

                String userUpdateStatement = " UPDATE "
                        + " user_info "
                        + " SET "
                        + " USER_FULL_NAME = '" + fullName + "', "
                        + " USER_PHONE = '" + phoneNumber + "', "
                        + " USER_FAX = '" + faxNumber + "', "
                        + " USER_EMAIL = '" + email + "', "
                        + " USER_DOB = '" + dob + "', "
                        + " USER_NOTES = '" + notes + "', "
                        + " USER_ADDRESS = '" + address + "', "
                        + " COUNTRY_ID = '" + countryID + "', "
                        + " COMPANY_ID = '" + companyID + "', "
                        + " USER_STATUS = '" + userStatus + "', "
                        + " UPDATED_BY = '" + updateBy + "', "
                        + " UPDATED_DATE = CURRENT_TIMESTAMP "
                        + " WHERE USER_ID = '" + userid + "'";

                ps = connection.prepareStatement(userUpdateStatement);
                status = ps.executeUpdate();

            } catch (Exception e) {
                e.printStackTrace();
                status = 0;
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                        System.out.println(ex);
                    }
                }
            }
        }

        return status;
    }
}
