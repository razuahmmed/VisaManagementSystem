package com.action;

import com.common.DBConnection;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.util.ValueStack;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JExcelApiExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.StrutsResultSupport;

public class JasperReportLoaderAction extends StrutsResultSupport {

    private String parameters;
    private String rptFormat;

    private Connection connection = null;

    /**
     * @return the parameters
     */
    public String getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    /**
     * @return the rptFormat
     */
    public String getRptFormat() {
        return rptFormat;
    }

    /**
     * @param rptFormat the rptFormat to set
     */
    public void setRptFormat(String rptFormat) {
        this.rptFormat = rptFormat;
    }

    @Override
    protected void doExecute(String arg0, ActionInvocation arg1) throws Exception {

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            OutputStream ouputStream = null;
            JRExporter exporter = null;

            String reportFileName = "";
            String reportFormat = "";

            try {

                // HttpServletRequest request = ServletActionContext.getRequest();
                // String pdfName = "Agent Report";
                //                
                HttpServletResponse response = ServletActionContext.getResponse();
                ValueStack stack = arg1.getStack();

                rptFormat = conditionalParse(rptFormat, arg1);
                reportFormat = (String) stack.findValue(rptFormat);

                HashMap hmParams = (HashMap) stack.findValue(parameters);
                parameters = conditionalParse(parameters, arg1);

                reportFileName = (String) hmParams.get("fileName");
                if (reportFileName.equalsIgnoreCase("1")) {
                    reportFileName = "student.jrxml";
                } else if (reportFileName.equalsIgnoreCase("2")) {
                    reportFileName = "worker.jrxml";
                }

                String jasperPath = ServletActionContext.getServletContext().getRealPath("/jasper");
                JasperReport jasperReport = JasperCompileManager.compileReport(jasperPath + "/" + reportFileName);

                if (jasperReport != null) {

                    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, hmParams, connection);
                    ouputStream = response.getOutputStream();

                    if (reportFormat.equalsIgnoreCase("pdf")) {
                        response.setContentType("application/pdf");
//                        response.setHeader("Content-Disposition", "inline; filename=\"Agent Report.pdf\"");

                        exporter = new JRPdfExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);

                        exporter.setParameter(JRPdfExporterParameter.METADATA_TITLE, hmParams.get("Title"));
                    } else if (reportFormat.equalsIgnoreCase("xls")) {
                        response.setContentType("application/xls");
                        response.setHeader("Content-Disposition", "inline; filename=\"Agent Report.xls\"");

                        exporter = new JExcelApiExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    } else if (reportFormat.equalsIgnoreCase("html")) {
                        exporter = new JRHtmlExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    } else if (reportFormat.equalsIgnoreCase("rtf")) {
                        response.setContentType("application/rtf");
                        response.setHeader("Content-Disposition", "inline; filename=\"file.rtf\"");

                        exporter = new JRRtfExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    } else if (reportFormat.equalsIgnoreCase("csv")) {
                        response.setContentType("application/csv");
                        response.setHeader("Content-Disposition", "inline; filename=\"file.csv\"");

                        exporter = new JRCsvExporter();
                        exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                        exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, ouputStream);
                    }

                    try {
                        exporter.exportReport();
                    } catch (JRException e) {
                        e.printStackTrace();
                    } finally {
                        if (ouputStream != null) {
                            try {
                                ouputStream.flush();
                                ouputStream.close();
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                }
            } catch (Exception sqlExp) {
                sqlExp.printStackTrace();
            } finally {
                try {
                    if (connection != null) {
                        connection.close();
                        connection = null;
                    }
                } catch (SQLException expSQL) {
                    expSQL.printStackTrace();
                }

            }
        }
    }
}
