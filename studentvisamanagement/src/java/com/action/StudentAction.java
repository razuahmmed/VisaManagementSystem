package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.IssueDao;
import com.model.StudentDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ApplicantInfo;
import com.persistance.CountryInfo;
import com.persistance.IssuesInfo;
import com.persistance.ServiceInfo;
import com.persistance.PaymentInfo;
import com.persistance.UserInfo;
import com.persistance.VarsityInfo;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;

public class StudentAction extends ActionSupport {

    private String agentId;
    private String appType;
    private String nationality;
    private String passportNumber;
    private String alternatePassportNo;
    private String applicantName;
    private String dob;
    private String passportIssuedPlace;
    private String passportIssueDate;
    private String passportExpiryDate;
    private String gender;
    private String applicantEmail;
    private String applicantAlternateEmail;
    private String maritalStatus;
    private String mobileNumber;
    private String city;
    private String postalCode;
    private String state;
    private String country;
    private String residentNumber;
    private String permanentAddress;

    private String corMobileNumber;
    private String corCity;
    private String corPostalCode;
    private String corState;
    private String corCountry;
    private String corResidentNumber;
    private String corAddress;

    private String fathersName;
    private String fathersOccupation;
    private String mothersName;
    private String mothersOccupation;
    private String highestQualification;
    private String hqPassYear;
    private String overAllGrade;

    private String visaCountry;
    private String visaExpiryDate;
    private String visaReleaseLetter;
    private String visaColName;
    private String visaType;
    private String arrivalDate;

    private String degreeName;
    private String result;
    private String passingYear;
    private String institution;

    private String universityID;
    private String varid;
    private String courseID;

    private String applicantID;
    private String paymentMode;
    private String paymentDate;
    private Double amount;
    private String currency;
    private Double conversionRate;
    private String bankName;
    private String chequeReference;
    private String chequeDate;
    private String serviceId;

    private Integer issueId;
    private String issueTitle;
    private String issueDetails;
    private String commentDetails;

    private String fieldName;
    private String fieldValue;

    private String messageString;

    private File document1;
    private String document1FileName;

    private File document2;
    private String document2FileName;

    private File document3;
    private String document3FileName;

    private File document4;
    private String document4FileName;

    private File document5;
    private String document5FileName;

    private File document6;
    private String document6FileName;

    private File document7;
    private String document7FileName;

    private File document8;
    private String document8FileName;

    private File document9;
    private String document9FileName;

    private File document10;
    private String document10FileName;

    private File document11;
    private String document11FileName;

    private File document12;
    private String document12FileName;

    private File document13;
    private String document13FileName;

    private File document14;
    private String document14FileName;

    private File document15;
    private String document15FileName;

    private File document16;
    private String document16FileName;

    private File document17;
    private String document17FileName;

    private File document18;
    private String document18FileName;

    private File document19;
    private String document19FileName;

    private File document20;
    private String document20FileName;

    private File document21;
    private String document21FileName;

    private File document22;
    private String document22FileName;

    private File document23;
    private String document23FileName;

    private File document24;
    private String document24FileName;

    private File document25;
    private String document25FileName;

    private File document26;
    private String document26FileName;

    private File document27;
    private String document27FileName;

    private File document28;
    private String document28FileName;

    private File document29;
    private String document29FileName;

    private File document30;
    private String document30FileName;

    private File documentOthers;
    private String documentOthersFileName;

    private Connection connection = null;
    private CommonList commonList = null;

    private StudentDao studentDao = null;
    private IssueDao issueDao = null;

    private List<UserInfo> agentInfoList = null;
    private List<CountryInfo> countryInfoList = null;
    private List<ApplicantInfo> applicantInfoList = null;
    private List<ApplicantInfo> singleAppInfoList = null;
    private List<VarsityInfo> varsityInfoList = null;
    private List<VarsityInfo> varsityCoursesInfoList = null;
    private List<ServiceInfo> serviceInfoList = null;
    private List<PaymentInfo> paymentInfoList = null;
    private List<PaymentInfo> payHisInfoList = null;
    private List<PaymentInfo> payDueInfoList = null;
    private List<IssuesInfo> issuesInfosList = null;

    public String addNewStudent() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (studentDao == null) {
            studentDao = new StudentDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            if (varid != null && courseID != null) {

                String applicantID = null;
                if (appIDGenerator() != null) {
                    applicantID = appIDGenerator();
                } else {
                    applicantID = "10000001";
                }

                int chk = studentDao.addNewApplicant(connection, applicantID, applicantName, nationality, passportNumber,
                        alternatePassportNo, dob, passportIssuedPlace, passportIssueDate, passportExpiryDate, gender,
                        applicantEmail, applicantAlternateEmail, maritalStatus, mobileNumber, city, postalCode, state, country,
                        residentNumber, permanentAddress, corMobileNumber, corCity, corPostalCode, corState, corCountry,
                        corResidentNumber, corAddress, fathersName, fathersOccupation, mothersName, mothersOccupation,
                        highestQualification, hqPassYear, overAllGrade, visaCountry, visaExpiryDate, visaReleaseLetter,
                        visaColName, visaType, arrivalDate, appType, agentId, userID);

                if (chk > 0) {

                    String[] degree = null;
                    String[] appResult = null;
                    String[] passYear = null;
                    String[] appIns = null;

                    if (degreeName != null && result != null && passingYear != null && institution != null) {
                        degree = degreeName.split(",");
                        appResult = result.split(",");
                        passYear = passingYear.split(",");
                        appIns = institution.split(",");
                    }

                    int eduChk = 0;
                    for (int i = 0; i < degree.length; i++) {
                        eduChk = studentDao.applicantEducation(connection, degree[i].trim(), appResult[i].trim(), passYear[i].trim(), appIns[i].trim(), applicantID);
                    }

                    if (eduChk > 0) {

                        String[] course = null;
                        String[] appAarID = null;

                        if (courseID != null && varid != null) {
                            course = courseID.split(",");
                            appAarID = varid.split(",");
                        }

                        int corChk = 0;
                        for (int i = 0; i < course.length; i++) {
                            corChk = studentDao.applicantCourse(connection, course[i].trim(), appAarID[i].trim(), applicantID);
                        }

                        if (corChk > 0) {
                            setMessageString("Applicant successfully added");
                        } else {
                            setMessageString("Applicant add failed");
                        }
                    } else {
                        setMessageString("Applicant add failed");
                    }
                } else {
                    setMessageString("Applicant add failed");
                }
            } else {
                setMessageString("Please check varsity and course");
            }
        }

        return SUCCESS;
    }

    private String appIDGenerator() {

        String applicantID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.APP_ID)+1),8,'0') "
                        + " AS APPLICANT_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(APPLICANT_ID, 1,12),UNSIGNED INTEGER) "
                        + " AS APP_ID FROM applicant_info)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    applicantID = rs.getString("APPLICANT_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return applicantID;
    }

    public String viewStudent() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setSingleAppInfoList(commonList.getSingleApplicantInfo(connection, applicantID));
            setAgentInfoList(commonList.getUserInfo(connection, "all"));
            setCountryInfoList(commonList.getCountryInfo(connection));
            setVarsityInfoList(commonList.getVarsityInfo(connection, "all"));
        }

        return SUCCESS;
    }

    public String searchApplicant() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setApplicantInfoList(commonList.searchApplicantInfo(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String deleteApplicant() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
//            setMessageString("Delete applicant");
        }

        return SUCCESS;
    }

    public String addIssues() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (issueDao == null) {
            issueDao = new IssueDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            int chk = issueDao.insertIssue(connection, issueTitle, issueDetails, applicantID, agentId, userID);

            if (chk > 0) {
                setMessageString("Your Issues successfully added");
            } else {
                setMessageString("Your Issues add failed");
            }
        }

        return SUCCESS;
    }

    public String commentOnIssues() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (issueDao == null) {
            issueDao = new IssueDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            int chk = issueDao.onCommentIssues(connection, commentDetails, userID, issueId);

            if (chk > 0) {
                setMessageString("You have been successfully comment on this issues");
            } else {
                setMessageString("Your comment on this issues have been failed");
            }
        }

        return SUCCESS;
    }

    public String addDocuments() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (issueDao == null) {
            issueDao = new IssueDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            System.out.println("document1 " + getDocument1());
            System.out.println("document2 " + getDocument2());
            System.out.println("document3 " + getDocument3());
            System.out.println("document4 " + getDocument4());
            System.out.println("document5 " + getDocument5());
            System.out.println("document6 " + getDocument6());
            System.out.println("document7 " + getDocument7());
            System.out.println("document8 " + getDocument8());
            System.out.println("document9 " + getDocument9());
            System.out.println("document10 " + getDocument10());
            System.out.println("document11 " + getDocument11());
            System.out.println("document12 " + getDocument12());
            System.out.println("document13 " + getDocument13());
            System.out.println("document14 " + getDocument14());
            System.out.println("document15 " + getDocument15());
            System.out.println("document16 " + getDocument16());
            System.out.println("document17 " + getDocument17());
            System.out.println("document18 " + getDocument18());
            System.out.println("document19 " + getDocument19());
            System.out.println("document20 " + getDocument20());
            System.out.println("document21 " + getDocument21());
            System.out.println("document22 " + getDocument22());
            System.out.println("document23 " + getDocument23());
            System.out.println("document24 " + getDocument24());
            System.out.println("document25 " + getDocument25());
            System.out.println("document26 " + getDocument26());
            System.out.println("document27 " + getDocument27());
            System.out.println("document28 " + getDocument28());
            System.out.println("document29 " + getDocument29());
            System.out.println("document30 " + getDocument30());
            System.out.println("documentOthers " + getDocumentOthers());

            int chk = 0;
            if (fileUploader(document1FileName, applicantID, document1)) {
                fileRename(document1FileName, applicantID);
                chk = issueDao.insertDocuments(connection, applicantID, agentId, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, dob, mothersName, mothersName, userID);
            }

            if (chk > 0) {
                setMessageString("Document successfully added");
            } else {
                setMessageString("Your Document add failed");
            }
        }

        return SUCCESS;
    }

    private boolean fileUploader(String fileName, String appid, File imageName) {

        String ext = FilenameUtils.getExtension(fileName);
        String imgName = appid + "." + ext;
        String destPath = ServletActionContext.getRequest().getSession().getServletContext().getRealPath("/DOCUMENTS_FILE");
        File fileToCreate = new File(destPath, imgName);

        try {

            FileUtils.copyFile(imageName, fileToCreate);

            return true;
        } catch (IOException ex) {
            Logger.getLogger(StudentAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;
    }

    private String fileRename(String fileName, String appid) {
        String ext = FilenameUtils.getExtension(fileName);
        return appid + "." + ext;
    }

    public String getVarsityCourses() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setVarsityCoursesInfoList(commonList.getVarsityCourseInfo(connection, "", universityID));
        }

        return SUCCESS;
    }

    public String applicantPaymentReceive() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        System.out.println("HHH " + chequeDate);

        if (studentDao == null) {
            studentDao = new StudentDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            if (allocateServiceCheck(applicantID) != 0) {

                if (chequeDate.isEmpty() && chequeDate.equals("")) {
                    chequeDate = "0000-00-00";
                }

                Double payAmount = 0.0;
                if (conversionRate != null) {
                    payAmount = amount / conversionRate;
                } else {
                    payAmount = amount;
                    conversionRate = 0.0;
                }

                Double appDue = 0.0;
                List<PaymentInfo> dueStatus = commonList.getPayDueHistory(connection, 0, "", applicantID);
                if (!dueStatus.isEmpty()) {
                    if (serviceId.equals("1")) {
                        appDue = dueStatus.get(0).getTuitionFeeDue();
                    } else if (serviceId.equals("2")) {
                        appDue = dueStatus.get(0).getOthersAmountDue();
                    } else if (serviceId.equals("3")) {
                        appDue = dueStatus.get(0).getWorkerAmountDue();
                    }
                } else {
                    if (serviceId.equals("1")) {
                        List<PaymentInfo> dueSt = commonList.getAmountAgree(connection, applicantID, "1");
                        appDue = dueSt.get(0).getTuitionAgree();
                    } else if (serviceId.equals("2")) {
                        List<PaymentInfo> dueSt = commonList.getAmountAgree(connection, applicantID, "1");
                        appDue = dueSt.get(0).getOthersAgree();
                    } else if (serviceId.equals("3")) {
                        List<PaymentInfo> dueSt = commonList.getAmountAgree(connection, applicantID, "2");
                        appDue = dueSt.get(0).getWorkerAgree();
                    }
                }

                if (appDue >= payAmount) {
                    int chk = studentDao.applicantPaymentReceive(connection, paymentMode, paymentDate, payAmount, conversionRate,
                            bankName, chequeReference, chequeDate, currency, serviceId, applicantID, agentId, userID);

                    if (chk > 0) {
                        setMessageString("Applicant Payment Successfully Received");
                    } else {
                        setMessageString("Applicant Payment Received Failed");
                    }
                } else {
                    setMessageString("You aren't able to receive more than applicant due");
                }
            } else {
                setMessageString("Didn't found any service this applicant");
            }
        }

        return SUCCESS;
    }

    private int allocateServiceCheck(String appid) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT COUNT(APPLICANT_ID) "
                        + " TOTAL "
                        + " FROM "
                        + " applicant_service "
                        + " WHERE "
                        + " APPLICANT_ID = '" + appid + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (Exception ex) {
                count = 0;
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return count;
    }

    public String getCurrentPayment() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setPaymentInfoList(commonList.getCurrentPayment(connection, serviceId, applicantID));
        }

        return SUCCESS;
    }

    public String searchPayHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setPayHisInfoList(commonList.searchPaymentHistory(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String searchDuePayment() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setPayDueInfoList(commonList.searchDuePayHistory(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String getApplicantByAgent() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setApplicantInfoList(commonList.getApplicantByAgent(connection, agentId));
        }

        return SUCCESS;
    }

    public String getServiceByApplicant() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setApplicantInfoList(commonList.getAppService(connection, applicantID));
        }

        return SUCCESS;
    }

    public String goAddStudentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setAgentInfoList(commonList.getUserInfo(connection, "active"));
            setCountryInfoList(commonList.getCountryInfo(connection));
            setVarsityInfoList(commonList.getVarsityInfo(connection, "active"));
        }

        return SUCCESS;
    }

    public String goStudentHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setApplicantInfoList(commonList.getApplicantInfo(connection, groupID, userID, "1"));
        }

        return SUCCESS;
    }

    public String goPaymentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setAgentInfoList(commonList.getUserInfo(connection, "all"));
            setApplicantInfoList(commonList.getApplicantInfo(connection, groupID, userID, ""));
            setServiceInfoList(commonList.getServiceInfo(connection, "all"));
        }

        return SUCCESS;
    }

    public String goPaymentHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setPayHisInfoList(commonList.getPaymentHistory(connection, groupID, userID));
        }

        return SUCCESS;
    }

    public String goPaymentSchedulePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goDuePaymentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setPayDueInfoList(commonList.getPayDueHistory(connection, groupID, userID, ""));
        }

        return SUCCESS;
    }

    public String goApplicantDocumentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setSingleAppInfoList(commonList.getSingleApplicantInfo(connection, applicantID));
        }

        return SUCCESS;
    }

    public String goApplicantIssuesPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setSingleAppInfoList(commonList.getSingleApplicantInfo(connection, applicantID));
            setIssuesInfosList(commonList.getIssuesInfo(connection, applicantID, userID));
        }

        return SUCCESS;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return the nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * @param nationality the nationality to set
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * @return the passportNumber
     */
    public String getPassportNumber() {
        return passportNumber;
    }

    /**
     * @param passportNumber the passportNumber to set
     */
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * @return the alternatePassportNo
     */
    public String getAlternatePassportNo() {
        return alternatePassportNo;
    }

    /**
     * @param alternatePassportNo the alternatePassportNo to set
     */
    public void setAlternatePassportNo(String alternatePassportNo) {
        this.alternatePassportNo = alternatePassportNo;
    }

    /**
     * @return the applicantName
     */
    public String getApplicantName() {
        return applicantName;
    }

    /**
     * @param applicantName the applicantName to set
     */
    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the passportIssuedPlace
     */
    public String getPassportIssuedPlace() {
        return passportIssuedPlace;
    }

    /**
     * @param passportIssuedPlace the passportIssuedPlace to set
     */
    public void setPassportIssuedPlace(String passportIssuedPlace) {
        this.passportIssuedPlace = passportIssuedPlace;
    }

    /**
     * @return the passportIssueDate
     */
    public String getPassportIssueDate() {
        return passportIssueDate;
    }

    /**
     * @param passportIssueDate the passportIssueDate to set
     */
    public void setPassportIssueDate(String passportIssueDate) {
        this.passportIssueDate = passportIssueDate;
    }

    /**
     * @return the passportExpiryDate
     */
    public String getPassportExpiryDate() {
        return passportExpiryDate;
    }

    /**
     * @param passportExpiryDate the passportExpiryDate to set
     */
    public void setPassportExpiryDate(String passportExpiryDate) {
        this.passportExpiryDate = passportExpiryDate;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the applicantEmail
     */
    public String getApplicantEmail() {
        return applicantEmail;
    }

    /**
     * @param applicantEmail the applicantEmail to set
     */
    public void setApplicantEmail(String applicantEmail) {
        this.applicantEmail = applicantEmail;
    }

    /**
     * @return the applicantAlternateEmail
     */
    public String getApplicantAlternateEmail() {
        return applicantAlternateEmail;
    }

    /**
     * @param applicantAlternateEmail the applicantAlternateEmail to set
     */
    public void setApplicantAlternateEmail(String applicantAlternateEmail) {
        this.applicantAlternateEmail = applicantAlternateEmail;
    }

    /**
     * @return the maritalStatus
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the postalCode
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * @param postalCode the postalCode to set
     */
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the residentNumber
     */
    public String getResidentNumber() {
        return residentNumber;
    }

    /**
     * @param residentNumber the residentNumber to set
     */
    public void setResidentNumber(String residentNumber) {
        this.residentNumber = residentNumber;
    }

    /**
     * @return the permanentAddress
     */
    public String getPermanentAddress() {
        return permanentAddress;
    }

    /**
     * @param permanentAddress the permanentAddress to set
     */
    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    /**
     * @return the corAddress
     */
    public String getCorAddress() {
        return corAddress;
    }

    /**
     * @param corAddress the corAddress to set
     */
    public void setCorAddress(String corAddress) {
        this.corAddress = corAddress;
    }

    /**
     * @return the corCity
     */
    public String getCorCity() {
        return corCity;
    }

    /**
     * @param corCity the corCity to set
     */
    public void setCorCity(String corCity) {
        this.corCity = corCity;
    }

    /**
     * @return the corPostalCode
     */
    public String getCorPostalCode() {
        return corPostalCode;
    }

    /**
     * @param corPostalCode the corPostalCode to set
     */
    public void setCorPostalCode(String corPostalCode) {
        this.corPostalCode = corPostalCode;
    }

    /**
     * @return the corState
     */
    public String getCorState() {
        return corState;
    }

    /**
     * @param corState the corState to set
     */
    public void setCorState(String corState) {
        this.corState = corState;
    }

    /**
     * @return the corCountry
     */
    public String getCorCountry() {
        return corCountry;
    }

    /**
     * @param corCountry the corCountry to set
     */
    public void setCorCountry(String corCountry) {
        this.corCountry = corCountry;
    }

    /**
     * @return the corMobileNumber
     */
    public String getCorMobileNumber() {
        return corMobileNumber;
    }

    /**
     * @param corMobileNumber the corMobileNumber to set
     */
    public void setCorMobileNumber(String corMobileNumber) {
        this.corMobileNumber = corMobileNumber;
    }

    /**
     * @return the corResidentNumber
     */
    public String getCorResidentNumber() {
        return corResidentNumber;
    }

    /**
     * @param corResidentNumber the corResidentNumber to set
     */
    public void setCorResidentNumber(String corResidentNumber) {
        this.corResidentNumber = corResidentNumber;
    }

    /**
     * @return the fathersName
     */
    public String getFathersName() {
        return fathersName;
    }

    /**
     * @param fathersName the fathersName to set
     */
    public void setFathersName(String fathersName) {
        this.fathersName = fathersName;
    }

    /**
     * @return the fathersOccupation
     */
    public String getFathersOccupation() {
        return fathersOccupation;
    }

    /**
     * @param fathersOccupation the fathersOccupation to set
     */
    public void setFathersOccupation(String fathersOccupation) {
        this.fathersOccupation = fathersOccupation;
    }

    /**
     * @return the mothersName
     */
    public String getMothersName() {
        return mothersName;
    }

    /**
     * @param mothersName the mothersName to set
     */
    public void setMothersName(String mothersName) {
        this.mothersName = mothersName;
    }

    /**
     * @return the mothersOccupation
     */
    public String getMothersOccupation() {
        return mothersOccupation;
    }

    /**
     * @param mothersOccupation the mothersOccupation to set
     */
    public void setMothersOccupation(String mothersOccupation) {
        this.mothersOccupation = mothersOccupation;
    }

    /**
     * @return the highestQualification
     */
    public String getHighestQualification() {
        return highestQualification;
    }

    /**
     * @param highestQualification the highestQualification to set
     */
    public void setHighestQualification(String highestQualification) {
        this.highestQualification = highestQualification;
    }

    /**
     * @return the hqPassYear
     */
    public String getHqPassYear() {
        return hqPassYear;
    }

    /**
     * @param hqPassYear the hqPassYear to set
     */
    public void setHqPassYear(String hqPassYear) {
        this.hqPassYear = hqPassYear;
    }

    /**
     * @return the visaCountry
     */
    public String getVisaCountry() {
        return visaCountry;
    }

    /**
     * @param visaCountry the visaCountry to set
     */
    public void setVisaCountry(String visaCountry) {
        this.visaCountry = visaCountry;
    }

    /**
     * @return the visaExpiryDate
     */
    public String getVisaExpiryDate() {
        return visaExpiryDate;
    }

    /**
     * @param visaExpiryDate the visaExpiryDate to set
     */
    public void setVisaExpiryDate(String visaExpiryDate) {
        this.visaExpiryDate = visaExpiryDate;
    }

    /**
     * @return the visaReleaseLetter
     */
    public String getVisaReleaseLetter() {
        return visaReleaseLetter;
    }

    /**
     * @param visaReleaseLetter the visaReleaseLetter to set
     */
    public void setVisaReleaseLetter(String visaReleaseLetter) {
        this.visaReleaseLetter = visaReleaseLetter;
    }

    /**
     * @return the visaColName
     */
    public String getVisaColName() {
        return visaColName;
    }

    /**
     * @param visaColName the visaColName to set
     */
    public void setVisaColName(String visaColName) {
        this.visaColName = visaColName;
    }

    /**
     * @return the visaType
     */
    public String getVisaType() {
        return visaType;
    }

    /**
     * @param visaType the visaType to set
     */
    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the overAllGrade
     */
    public String getOverAllGrade() {
        return overAllGrade;
    }

    /**
     * @param overAllGrade the overAllGrade to set
     */
    public void setOverAllGrade(String overAllGrade) {
        this.overAllGrade = overAllGrade;
    }

    /**
     * @return the applicantInfoList
     */
    public List<ApplicantInfo> getApplicantInfoList() {
        return applicantInfoList;
    }

    /**
     * @param applicantInfoList the applicantInfoList to set
     */
    public void setApplicantInfoList(List<ApplicantInfo> applicantInfoList) {
        this.applicantInfoList = applicantInfoList;
    }

    /**
     * @return the degreeName
     */
    public String getDegreeName() {
        return degreeName;
    }

    /**
     * @param degreeName the degreeName to set
     */
    public void setDegreeName(String degreeName) {
        this.degreeName = degreeName;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @return the passingYear
     */
    public String getPassingYear() {
        return passingYear;
    }

    /**
     * @param passingYear the passingYear to set
     */
    public void setPassingYear(String passingYear) {
        this.passingYear = passingYear;
    }

    /**
     * @return the institution
     */
    public String getInstitution() {
        return institution;
    }

    /**
     * @param institution the institution to set
     */
    public void setInstitution(String institution) {
        this.institution = institution;
    }

    /**
     * @return the arrivalDate
     */
    public String getArrivalDate() {
        return arrivalDate;
    }

    /**
     * @param arrivalDate the arrivalDate to set
     */
    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    /**
     * @return the varsityInfoList
     */
    public List<VarsityInfo> getVarsityInfoList() {
        return varsityInfoList;
    }

    /**
     * @param varsityInfoList the varsityInfoList to set
     */
    public void setVarsityInfoList(List<VarsityInfo> varsityInfoList) {
        this.varsityInfoList = varsityInfoList;
    }

    /**
     * @return the courseID
     */
    public String getCourseID() {
        return courseID;
    }

    /**
     * @param courseID the courseID to set
     */
    public void setCourseID(String courseID) {
        this.courseID = courseID;
    }

    /**
     * @return the varsityCoursesInfoList
     */
    public List<VarsityInfo> getVarsityCoursesInfoList() {
        return varsityCoursesInfoList;
    }

    /**
     * @param varsityCoursesInfoList the varsityCoursesInfoList to set
     */
    public void setVarsityCoursesInfoList(List<VarsityInfo> varsityCoursesInfoList) {
        this.varsityCoursesInfoList = varsityCoursesInfoList;
    }

    /**
     * @return the universityID
     */
    public String getUniversityID() {
        return universityID;
    }

    /**
     * @param universityID the universityID to set
     */
    public void setUniversityID(String universityID) {
        this.universityID = universityID;
    }

    /**
     * @return the serviceInfoList
     */
    public List<ServiceInfo> getServiceInfoList() {
        return serviceInfoList;
    }

    /**
     * @param serviceInfoList the serviceInfoList to set
     */
    public void setServiceInfoList(List<ServiceInfo> serviceInfoList) {
        this.serviceInfoList = serviceInfoList;
    }

    /**
     * @return the applicantID
     */
    public String getApplicantID() {
        return applicantID;
    }

    /**
     * @param applicantID the applicantID to set
     */
    public void setApplicantID(String applicantID) {
        this.applicantID = applicantID;
    }

    /**
     * @return the paymentMode
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * @param paymentMode the paymentMode to set
     */
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    /**
     * @return the paymentDate
     */
    public String getPaymentDate() {
        return paymentDate;
    }

    /**
     * @param paymentDate the paymentDate to set
     */
    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    /**
     * @return the currency
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * @param currency the currency to set
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * @return the bankName
     */
    public String getBankName() {
        return bankName;
    }

    /**
     * @param bankName the bankName to set
     */
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    /**
     * @return the chequeReference
     */
    public String getChequeReference() {
        return chequeReference;
    }

    /**
     * @param chequeReference the chequeReference to set
     */
    public void setChequeReference(String chequeReference) {
        this.chequeReference = chequeReference;
    }

    /**
     * @return the chequeDate
     */
    public String getChequeDate() {
        return chequeDate;
    }

    /**
     * @param chequeDate the chequeDate to set
     */
    public void setChequeDate(String chequeDate) {
        this.chequeDate = chequeDate;
    }

    /**
     * @return the paymentInfoList
     */
    public List<PaymentInfo> getPaymentInfoList() {
        return paymentInfoList;
    }

    /**
     * @param paymentInfoList the paymentInfoList to set
     */
    public void setPaymentInfoList(List<PaymentInfo> paymentInfoList) {
        this.paymentInfoList = paymentInfoList;
    }

    /**
     * @return the agentInfoList
     */
    public List<UserInfo> getAgentInfoList() {
        return agentInfoList;
    }

    /**
     * @param agentInfoList the agentInfoList to set
     */
    public void setAgentInfoList(List<UserInfo> agentInfoList) {
        this.agentInfoList = agentInfoList;
    }

    /**
     * @return the singleAppInfoList
     */
    public List<ApplicantInfo> getSingleAppInfoList() {
        return singleAppInfoList;
    }

    /**
     * @param singleAppInfoList the singleAppInfoList to set
     */
    public void setSingleAppInfoList(List<ApplicantInfo> singleAppInfoList) {
        this.singleAppInfoList = singleAppInfoList;
    }

    /**
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

    /**
     * @return the payHisInfoList
     */
    public List<PaymentInfo> getPayHisInfoList() {
        return payHisInfoList;
    }

    /**
     * @param payHisInfoList the payHisInfoList to set
     */
    public void setPayHisInfoList(List<PaymentInfo> payHisInfoList) {
        this.payHisInfoList = payHisInfoList;
    }

    /**
     * @return the payDueInfoList
     */
    public List<PaymentInfo> getPayDueInfoList() {
        return payDueInfoList;
    }

    /**
     * @param payDueInfoList the payDueInfoList to set
     */
    public void setPayDueInfoList(List<PaymentInfo> payDueInfoList) {
        this.payDueInfoList = payDueInfoList;
    }

    /**
     * @return the conversionRate
     */
    public Double getConversionRate() {
        return conversionRate;
    }

    /**
     * @param conversionRate the conversionRate to set
     */
    public void setConversionRate(Double conversionRate) {
        this.conversionRate = conversionRate;
    }

    /**
     * @return the varid
     */
    public String getVarid() {
        return varid;
    }

    /**
     * @param varid the varid to set
     */
    public void setVarid(String varid) {
        this.varid = varid;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the issueTitle
     */
    public String getIssueTitle() {
        return issueTitle;
    }

    /**
     * @param issueTitle the issueTitle to set
     */
    public void setIssueTitle(String issueTitle) {
        this.issueTitle = issueTitle;
    }

    /**
     * @return the issueDetails
     */
    public String getIssueDetails() {
        return issueDetails;
    }

    /**
     * @param issueDetails the issueDetails to set
     */
    public void setIssueDetails(String issueDetails) {
        this.issueDetails = issueDetails;
    }

    /**
     * @return the issuesInfosList
     */
    public List<IssuesInfo> getIssuesInfosList() {
        return issuesInfosList;
    }

    /**
     * @param issuesInfosList the issuesInfosList to set
     */
    public void setIssuesInfosList(List<IssuesInfo> issuesInfosList) {
        this.issuesInfosList = issuesInfosList;
    }

    /**
     * @return the issueId
     */
    public Integer getIssueId() {
        return issueId;
    }

    /**
     * @param issueId the issueId to set
     */
    public void setIssueId(Integer issueId) {
        this.issueId = issueId;
    }

    /**
     * @return the commentDetails
     */
    public String getCommentDetails() {
        return commentDetails;
    }

    /**
     * @param commentDetails the commentDetails to set
     */
    public void setCommentDetails(String commentDetails) {
        this.commentDetails = commentDetails;
    }

    /**
     * @return the document1
     */
    public File getDocument1() {
        return document1;
    }

    /**
     * @param document1 the document1 to set
     */
    public void setDocument1(File document1) {
        this.document1 = document1;
    }

    /**
     * @return the document1FileName
     */
    public String getDocument1FileName() {
        return document1FileName;
    }

    /**
     * @param document1FileName the document1FileName to set
     */
    public void setDocument1FileName(String document1FileName) {
        this.document1FileName = document1FileName;
    }

    /**
     * @return the document2
     */
    public File getDocument2() {
        return document2;
    }

    /**
     * @param document2 the document2 to set
     */
    public void setDocument2(File document2) {
        this.document2 = document2;
    }

    /**
     * @return the document2FileName
     */
    public String getDocument2FileName() {
        return document2FileName;
    }

    /**
     * @param document2FileName the document2FileName to set
     */
    public void setDocument2FileName(String document2FileName) {
        this.document2FileName = document2FileName;
    }

    /**
     * @return the document3
     */
    public File getDocument3() {
        return document3;
    }

    /**
     * @param document3 the document3 to set
     */
    public void setDocument3(File document3) {
        this.document3 = document3;
    }

    /**
     * @return the document3FileName
     */
    public String getDocument3FileName() {
        return document3FileName;
    }

    /**
     * @param document3FileName the document3FileName to set
     */
    public void setDocument3FileName(String document3FileName) {
        this.document3FileName = document3FileName;
    }

    /**
     * @return the document4
     */
    public File getDocument4() {
        return document4;
    }

    /**
     * @param document4 the document4 to set
     */
    public void setDocument4(File document4) {
        this.document4 = document4;
    }

    /**
     * @return the document4FileName
     */
    public String getDocument4FileName() {
        return document4FileName;
    }

    /**
     * @param document4FileName the document4FileName to set
     */
    public void setDocument4FileName(String document4FileName) {
        this.document4FileName = document4FileName;
    }

    /**
     * @return the document5
     */
    public File getDocument5() {
        return document5;
    }

    /**
     * @param document5 the document5 to set
     */
    public void setDocument5(File document5) {
        this.document5 = document5;
    }

    /**
     * @return the document5FileName
     */
    public String getDocument5FileName() {
        return document5FileName;
    }

    /**
     * @param document5FileName the document5FileName to set
     */
    public void setDocument5FileName(String document5FileName) {
        this.document5FileName = document5FileName;
    }

    /**
     * @return the document6
     */
    public File getDocument6() {
        return document6;
    }

    /**
     * @param document6 the document6 to set
     */
    public void setDocument6(File document6) {
        this.document6 = document6;
    }

    /**
     * @return the document6FileName
     */
    public String getDocument6FileName() {
        return document6FileName;
    }

    /**
     * @param document6FileName the document6FileName to set
     */
    public void setDocument6FileName(String document6FileName) {
        this.document6FileName = document6FileName;
    }

    /**
     * @return the document7
     */
    public File getDocument7() {
        return document7;
    }

    /**
     * @param document7 the document7 to set
     */
    public void setDocument7(File document7) {
        this.document7 = document7;
    }

    /**
     * @return the document7FileName
     */
    public String getDocument7FileName() {
        return document7FileName;
    }

    /**
     * @param document7FileName the document7FileName to set
     */
    public void setDocument7FileName(String document7FileName) {
        this.document7FileName = document7FileName;
    }

    /**
     * @return the document8
     */
    public File getDocument8() {
        return document8;
    }

    /**
     * @param document8 the document8 to set
     */
    public void setDocument8(File document8) {
        this.document8 = document8;
    }

    /**
     * @return the document8FileName
     */
    public String getDocument8FileName() {
        return document8FileName;
    }

    /**
     * @param document8FileName the document8FileName to set
     */
    public void setDocument8FileName(String document8FileName) {
        this.document8FileName = document8FileName;
    }

    /**
     * @return the document9
     */
    public File getDocument9() {
        return document9;
    }

    /**
     * @param document9 the document9 to set
     */
    public void setDocument9(File document9) {
        this.document9 = document9;
    }

    /**
     * @return the document9FileName
     */
    public String getDocument9FileName() {
        return document9FileName;
    }

    /**
     * @param document9FileName the document9FileName to set
     */
    public void setDocument9FileName(String document9FileName) {
        this.document9FileName = document9FileName;
    }

    /**
     * @return the document10
     */
    public File getDocument10() {
        return document10;
    }

    /**
     * @param document10 the document10 to set
     */
    public void setDocument10(File document10) {
        this.document10 = document10;
    }

    /**
     * @return the document10FileName
     */
    public String getDocument10FileName() {
        return document10FileName;
    }

    /**
     * @param document10FileName the document10FileName to set
     */
    public void setDocument10FileName(String document10FileName) {
        this.document10FileName = document10FileName;
    }

    /**
     * @return the document11
     */
    public File getDocument11() {
        return document11;
    }

    /**
     * @param document11 the document11 to set
     */
    public void setDocument11(File document11) {
        this.document11 = document11;
    }

    /**
     * @return the document11FileName
     */
    public String getDocument11FileName() {
        return document11FileName;
    }

    /**
     * @param document11FileName the document11FileName to set
     */
    public void setDocument11FileName(String document11FileName) {
        this.document11FileName = document11FileName;
    }

    /**
     * @return the document12
     */
    public File getDocument12() {
        return document12;
    }

    /**
     * @param document12 the document12 to set
     */
    public void setDocument12(File document12) {
        this.document12 = document12;
    }

    /**
     * @return the document12FileName
     */
    public String getDocument12FileName() {
        return document12FileName;
    }

    /**
     * @param document12FileName the document12FileName to set
     */
    public void setDocument12FileName(String document12FileName) {
        this.document12FileName = document12FileName;
    }

    /**
     * @return the document13
     */
    public File getDocument13() {
        return document13;
    }

    /**
     * @param document13 the document13 to set
     */
    public void setDocument13(File document13) {
        this.document13 = document13;
    }

    /**
     * @return the document13FileName
     */
    public String getDocument13FileName() {
        return document13FileName;
    }

    /**
     * @param document13FileName the document13FileName to set
     */
    public void setDocument13FileName(String document13FileName) {
        this.document13FileName = document13FileName;
    }

    /**
     * @return the document14
     */
    public File getDocument14() {
        return document14;
    }

    /**
     * @param document14 the document14 to set
     */
    public void setDocument14(File document14) {
        this.document14 = document14;
    }

    /**
     * @return the document14FileName
     */
    public String getDocument14FileName() {
        return document14FileName;
    }

    /**
     * @param document14FileName the document14FileName to set
     */
    public void setDocument14FileName(String document14FileName) {
        this.document14FileName = document14FileName;
    }

    /**
     * @return the document15
     */
    public File getDocument15() {
        return document15;
    }

    /**
     * @param document15 the document15 to set
     */
    public void setDocument15(File document15) {
        this.document15 = document15;
    }

    /**
     * @return the document15FileName
     */
    public String getDocument15FileName() {
        return document15FileName;
    }

    /**
     * @param document15FileName the document15FileName to set
     */
    public void setDocument15FileName(String document15FileName) {
        this.document15FileName = document15FileName;
    }

    /**
     * @return the document16
     */
    public File getDocument16() {
        return document16;
    }

    /**
     * @param document16 the document16 to set
     */
    public void setDocument16(File document16) {
        this.document16 = document16;
    }

    /**
     * @return the document16FileName
     */
    public String getDocument16FileName() {
        return document16FileName;
    }

    /**
     * @param document16FileName the document16FileName to set
     */
    public void setDocument16FileName(String document16FileName) {
        this.document16FileName = document16FileName;
    }

    /**
     * @return the document17
     */
    public File getDocument17() {
        return document17;
    }

    /**
     * @param document17 the document17 to set
     */
    public void setDocument17(File document17) {
        this.document17 = document17;
    }

    /**
     * @return the document17FileName
     */
    public String getDocument17FileName() {
        return document17FileName;
    }

    /**
     * @param document17FileName the document17FileName to set
     */
    public void setDocument17FileName(String document17FileName) {
        this.document17FileName = document17FileName;
    }

    /**
     * @return the document18
     */
    public File getDocument18() {
        return document18;
    }

    /**
     * @param document18 the document18 to set
     */
    public void setDocument18(File document18) {
        this.document18 = document18;
    }

    /**
     * @return the document18FileName
     */
    public String getDocument18FileName() {
        return document18FileName;
    }

    /**
     * @param document18FileName the document18FileName to set
     */
    public void setDocument18FileName(String document18FileName) {
        this.document18FileName = document18FileName;
    }

    /**
     * @return the document19
     */
    public File getDocument19() {
        return document19;
    }

    /**
     * @param document19 the document19 to set
     */
    public void setDocument19(File document19) {
        this.document19 = document19;
    }

    /**
     * @return the document19FileName
     */
    public String getDocument19FileName() {
        return document19FileName;
    }

    /**
     * @param document19FileName the document19FileName to set
     */
    public void setDocument19FileName(String document19FileName) {
        this.document19FileName = document19FileName;
    }

    /**
     * @return the document20
     */
    public File getDocument20() {
        return document20;
    }

    /**
     * @param document20 the document20 to set
     */
    public void setDocument20(File document20) {
        this.document20 = document20;
    }

    /**
     * @return the document20FileName
     */
    public String getDocument20FileName() {
        return document20FileName;
    }

    /**
     * @param document20FileName the document20FileName to set
     */
    public void setDocument20FileName(String document20FileName) {
        this.document20FileName = document20FileName;
    }

    /**
     * @return the document21
     */
    public File getDocument21() {
        return document21;
    }

    /**
     * @param document21 the document21 to set
     */
    public void setDocument21(File document21) {
        this.document21 = document21;
    }

    /**
     * @return the document21FileName
     */
    public String getDocument21FileName() {
        return document21FileName;
    }

    /**
     * @param document21FileName the document21FileName to set
     */
    public void setDocument21FileName(String document21FileName) {
        this.document21FileName = document21FileName;
    }

    /**
     * @return the document22
     */
    public File getDocument22() {
        return document22;
    }

    /**
     * @param document22 the document22 to set
     */
    public void setDocument22(File document22) {
        this.document22 = document22;
    }

    /**
     * @return the document22FileName
     */
    public String getDocument22FileName() {
        return document22FileName;
    }

    /**
     * @param document22FileName the document22FileName to set
     */
    public void setDocument22FileName(String document22FileName) {
        this.document22FileName = document22FileName;
    }

    /**
     * @return the document23
     */
    public File getDocument23() {
        return document23;
    }

    /**
     * @param document23 the document23 to set
     */
    public void setDocument23(File document23) {
        this.document23 = document23;
    }

    /**
     * @return the document23FileName
     */
    public String getDocument23FileName() {
        return document23FileName;
    }

    /**
     * @param document23FileName the document23FileName to set
     */
    public void setDocument23FileName(String document23FileName) {
        this.document23FileName = document23FileName;
    }

    /**
     * @return the document24
     */
    public File getDocument24() {
        return document24;
    }

    /**
     * @param document24 the document24 to set
     */
    public void setDocument24(File document24) {
        this.document24 = document24;
    }

    /**
     * @return the document24FileName
     */
    public String getDocument24FileName() {
        return document24FileName;
    }

    /**
     * @param document24FileName the document24FileName to set
     */
    public void setDocument24FileName(String document24FileName) {
        this.document24FileName = document24FileName;
    }

    /**
     * @return the document25
     */
    public File getDocument25() {
        return document25;
    }

    /**
     * @param document25 the document25 to set
     */
    public void setDocument25(File document25) {
        this.document25 = document25;
    }

    /**
     * @return the document25FileName
     */
    public String getDocument25FileName() {
        return document25FileName;
    }

    /**
     * @param document25FileName the document25FileName to set
     */
    public void setDocument25FileName(String document25FileName) {
        this.document25FileName = document25FileName;
    }

    /**
     * @return the document26
     */
    public File getDocument26() {
        return document26;
    }

    /**
     * @param document26 the document26 to set
     */
    public void setDocument26(File document26) {
        this.document26 = document26;
    }

    /**
     * @return the document26FileName
     */
    public String getDocument26FileName() {
        return document26FileName;
    }

    /**
     * @param document26FileName the document26FileName to set
     */
    public void setDocument26FileName(String document26FileName) {
        this.document26FileName = document26FileName;
    }

    /**
     * @return the document27
     */
    public File getDocument27() {
        return document27;
    }

    /**
     * @param document27 the document27 to set
     */
    public void setDocument27(File document27) {
        this.document27 = document27;
    }

    /**
     * @return the document27FileName
     */
    public String getDocument27FileName() {
        return document27FileName;
    }

    /**
     * @param document27FileName the document27FileName to set
     */
    public void setDocument27FileName(String document27FileName) {
        this.document27FileName = document27FileName;
    }

    /**
     * @return the document28
     */
    public File getDocument28() {
        return document28;
    }

    /**
     * @param document28 the document28 to set
     */
    public void setDocument28(File document28) {
        this.document28 = document28;
    }

    /**
     * @return the document28FileName
     */
    public String getDocument28FileName() {
        return document28FileName;
    }

    /**
     * @param document28FileName the document28FileName to set
     */
    public void setDocument28FileName(String document28FileName) {
        this.document28FileName = document28FileName;
    }

    /**
     * @return the document29
     */
    public File getDocument29() {
        return document29;
    }

    /**
     * @param document29 the document29 to set
     */
    public void setDocument29(File document29) {
        this.document29 = document29;
    }

    /**
     * @return the document29FileName
     */
    public String getDocument29FileName() {
        return document29FileName;
    }

    /**
     * @param document29FileName the document29FileName to set
     */
    public void setDocument29FileName(String document29FileName) {
        this.document29FileName = document29FileName;
    }

    /**
     * @return the document30
     */
    public File getDocument30() {
        return document30;
    }

    /**
     * @param document30 the document30 to set
     */
    public void setDocument30(File document30) {
        this.document30 = document30;
    }

    /**
     * @return the document30FileName
     */
    public String getDocument30FileName() {
        return document30FileName;
    }

    /**
     * @param document30FileName the document30FileName to set
     */
    public void setDocument30FileName(String document30FileName) {
        this.document30FileName = document30FileName;
    }

    /**
     * @return the documentOthers
     */
    public File getDocumentOthers() {
        return documentOthers;
    }

    /**
     * @param documentOthers the documentOthers to set
     */
    public void setDocumentOthers(File documentOthers) {
        this.documentOthers = documentOthers;
    }

    /**
     * @return the documentOthersFileName
     */
    public String getDocumentOthersFileName() {
        return documentOthersFileName;
    }

    /**
     * @param documentOthersFileName the documentOthersFileName to set
     */
    public void setDocumentOthersFileName(String documentOthersFileName) {
        this.documentOthersFileName = documentOthersFileName;
    }

    /**
     * @return the appType
     */
    public String getAppType() {
        return appType;
    }

    /**
     * @param appType the appType to set
     */
    public void setAppType(String appType) {
        this.appType = appType;
    }
}
