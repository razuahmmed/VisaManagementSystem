package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.UserDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.CountryInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class AgentAction extends ActionSupport {

    private String userId;
    private String fullName;
    private String userName;
    private String password;
    private String phoneNumber;
    private String faxNumber;
    private String email;
    private String dob;
    private Character userStatus;
    private String notes;
    private String address;
    private String country;
    private String company;
    private String userGroup;

    private String fieldName;
    private String fieldValue;

    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;

    private UserDao userDao = null;

    private List<UserInfo> agentInfoList = null;
    private List<UserInfo> singleAgentInfoList = null;
    private List<CountryInfo> countryInfoList = null;

    public String viewUserInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleAgentInfoList(commonList.getUserInfo(connection, userId));
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String addNewUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (userDao == null) {
            userDao = new UserDao();
        }

        if (connection != null) {

            if (duplicateUserNameChecked(userName) == 0) {
                int chk = userDao.addNewUserInfo(connection, fullName, userName, password, phoneNumber, faxNumber, email, dob,
                        notes, address, country, company, userGroup, userID);
                if (chk > 0) {
                    setMessageString("User has been successfully added");
                } else {
                    setMessageString("User added failed, try again later");
                }
            } else {
                setMessageString("User Name duplicate found, please try another User Name");
            }
        }

        return SUCCESS;
    }

    public String editUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        String uName = (String) session.get("USER_NAME");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (userDao == null) {
            userDao = new UserDao();
        }

        if (connection != null) {

            int chk = userDao.updateUserInfo(connection, fullName, phoneNumber, faxNumber, email, dob,
                    notes, address, country, company, userStatus, userID, userId);

            if (chk > 0) {
                messageString = "User has been edited successfully";
            } else {
                messageString = "User edit failed, try again later";
            }
        }

        return SUCCESS;
    }

    public String searchUser() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        String uName = (String) session.get("USER_NAME");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAgentInfoList(commonList.searchUserInfo(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    private int duplicateUserNameChecked(String uName) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT COUNT(USER_NAME) "
                        + " TOTAL "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " USER_NAME = '" + uName + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (Exception ex) {
                count = 0;
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return count;
    }

    public String goAddAgentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String goManageAgentPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }
        if (connection != null) {
            setAgentInfoList(commonList.getUserInfo(connection, "agent"));
        }

        return SUCCESS;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the agentInfoList
     */
    public List<UserInfo> getAgentInfoList() {
        return agentInfoList;
    }

    /**
     * @param agentInfoList the agentInfoList to set
     */
    public void setAgentInfoList(List<UserInfo> agentInfoList) {
        this.agentInfoList = agentInfoList;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the faxNumber
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * @param faxNumber the faxNumber to set
     */
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the notes
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @param notes the notes to set
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the userGroup
     */
    public String getUserGroup() {
        return userGroup;
    }

    /**
     * @param userGroup the userGroup to set
     */
    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    /**
     * @return the singleAgentInfoList
     */
    public List<UserInfo> getSingleAgentInfoList() {
        return singleAgentInfoList;
    }

    /**
     * @param singleAgentInfoList the singleAgentInfoList to set
     */
    public void setSingleAgentInfoList(List<UserInfo> singleAgentInfoList) {
        this.singleAgentInfoList = singleAgentInfoList;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the userStatus
     */
    public Character getUserStatus() {
        return userStatus;
    }

    /**
     * @param userStatus the userStatus to set
     */
    public void setUserStatus(Character userStatus) {
        this.userStatus = userStatus;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }
}
