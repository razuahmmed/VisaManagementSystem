package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReportAction extends ActionSupport {

    private String agentId;
    private String dateFrom;
    private String dateTo;
    private String fileType;
    private String rptFmt;

    private Map<String, Object> param = null;

    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;

    private List<UserInfo> agentInfoList = null;

    public String generateNewReport() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            try {
//                if (rptFmt.equals("pdf")) {
//
//                    jasperPath = ServletActionContext.getServletContext().getRealPath("/jasper");
//                    pdfName = "Agent Report";
//                    rpt = "agent.jrxml";
//
//                    HashMap<String, Object> parameters = new HashMap<String, Object>();
//                    parameters.put("userid", agentId);
//
//                    JasperReport jr = JasperCompileManager.compileReport(jasperPath + "/" + rpt);
//                    JasperPrint jp = JasperFillManager.fillReport(jr, parameters, connection);
//
//                    JasperExportManager.exportReportToPdfFile(jp, jasperPath + pdfName + ".pdf");
//                    fileInputStream = new FileInputStream(new File(jasperPath + pdfName + ".pdf"));
//                }
//
                param = new HashMap();
                param.put("userId", agentId);
                param.put("dateFrom", dateFrom);
                param.put("dateTo", dateTo);
                param.put("fileName", fileType);
                param.put("Title", "Payment Report");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return SUCCESS;
    }

    public String goGenerateReportPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAgentInfoList(commonList.getUserInfo(connection, "all"));
        }

        return SUCCESS;
    }

    public String goExpenseStatementPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goIncomeStatementPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goAgentHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goBillStatementPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goMoreReportPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    /**
     * @return the agentInfoList
     */
    public List<UserInfo> getAgentInfoList() {
        return agentInfoList;
    }

    /**
     * @param agentInfoList the agentInfoList to set
     */
    public void setAgentInfoList(List<UserInfo> agentInfoList) {
        this.agentInfoList = agentInfoList;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the dateFrom
     */
    public String getDateFrom() {
        return dateFrom;
    }

    /**
     * @param dateFrom the dateFrom to set
     */
    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    /**
     * @return the dateTo
     */
    public String getDateTo() {
        return dateTo;
    }

    /**
     * @param dateTo the dateTo to set
     */
    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return the param
     */
    public Map<String, Object> getParam() {
        return param;
    }

    /**
     * @param param the param to set
     */
    public void setParam(Map<String, Object> param) {
        this.param = param;
    }

    /**
     * @return the rptFmt
     */
    public String getRptFmt() {
        return rptFmt;
    }

    /**
     * @param rptFmt the rptFmt to set
     */
    public void setRptFmt(String rptFmt) {
        this.rptFmt = rptFmt;
    }

    /**
     * @return the fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType the fileType to set
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
