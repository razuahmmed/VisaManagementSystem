package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.ProfileDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.CountryInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class ProfileAction extends ActionSupport {

    private String userId;
    private String fullName;
    private String company;
    private String phoneNumber;
    private String faxNumber;
    private String email;
    private String dob;
    private String country;
    private String address;
    private String currentPassword;
    private String newPassword;
    private String messageString;
    private String messageColor;

    private Connection connection = null;
    private CommonList commonList = null;
    private ProfileDao profileDao = null;

    private List<UserInfo> singleAgentInfoList = null;
    private List<CountryInfo> countryInfoList = null;

    public String editUserProfile() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (profileDao == null) {
            profileDao = new ProfileDao();
        }

        if (connection != null) {

            int chk = profileDao.updateUserProfile(connection, fullName, phoneNumber, faxNumber, email, dob,
                    address, country, company, userID, userID);

            if (chk > 0) {
                setMessageString("User profile has been successfully edited");
            } else {
                setMessageString("User profile edit failed");
            }
        }

        return SUCCESS;
    }

    public String changeUserPassword() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (profileDao == null) {
            profileDao = new ProfileDao();
        }

        if (connection != null) {
            String cdbpass = getCurPass(userID).trim();
            if (currentPassword.trim().equals(cdbpass)) {
                if (!newPassword.trim().equals(cdbpass)) {
                    int chk = profileDao.updateUserPassword(connection, newPassword, userID, userID);
                    if (chk > 0) {
                        setMessageString("User password has been successfully changed");
                    } else {
                        setMessageString("User password change failed, Please try again later");
                    }
                } else {
                    setMessageString("New password and current password are matching, choose another password");
                }
            } else {
                setMessageString("Incorrect current password");
            }
        }

        return SUCCESS;
    }

    public String changePassByAdmin() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (profileDao == null) {
            profileDao = new ProfileDao();
        }

        if (connection != null) {
            int chk = profileDao.updateUserPassword(connection, newPassword, userID, userId);
            if (chk > 0) {
                setMessageColor("success");
                setMessageString("User password has been successfully changed");
            } else {
                setMessageColor("danger");
                setMessageString("User password change failed, Please try again later");
            }
        }

        return SUCCESS;
    }

    private String getCurPass(String id) {

        String pass = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = " SELECT "
                        + " USER_PASSWORD "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " USER_ID = '" + id + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    pass = rs.getString("USER_PASSWORD");
                }

            } catch (Exception ex) {
                pass = "";
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return pass;
    }

    public String goMyProfilePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleAgentInfoList(commonList.getUserInfo(connection, userID));
        }

        return SUCCESS;
    }

    public String goEditProfilePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleAgentInfoList(commonList.getUserInfo(connection, userID));
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String goChangePasswordPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    /**
     * @return the singleAgentInfoList
     */
    public List<UserInfo> getSingleAgentInfoList() {
        return singleAgentInfoList;
    }

    /**
     * @param singleAgentInfoList the singleAgentInfoList to set
     */
    public void setSingleAgentInfoList(List<UserInfo> singleAgentInfoList) {
        this.singleAgentInfoList = singleAgentInfoList;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the phoneNumber
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the faxNumber
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * @param faxNumber the faxNumber to set
     */
    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    /**
     * @return the currentPassword
     */
    public String getCurrentPassword() {
        return currentPassword;
    }

    /**
     * @param currentPassword the currentPassword to set
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    /**
     * @return the newPassword
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * @param newPassword the newPassword to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }
}
