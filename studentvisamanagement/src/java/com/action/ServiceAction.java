package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.ServiceDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ApplicantInfo;
import com.persistance.ServiceInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class ServiceAction extends ActionSupport {

    private String agentId;
    private String applicantID;
    private String serviceID;
    private String basicAmount;
    private String agreeAmount;

    private String fieldName;
    private String fieldValue;

    private String curAppName;
    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;

    private ServiceDao serviceDao = null;

    private List<ServiceInfo> serviceInfoList = null;
    private List<UserInfo> agentInfoList = null;
    private List<ApplicantInfo> applicantInfoList = null;
    private List<ApplicantInfo> applicantServiceInfoList = null;

    public String allocateApplicantService() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (serviceDao == null) {
            serviceDao = new ServiceDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            if (serviceID != null) {
                if (allocateServiceCheck(applicantID) == 0) {

                    String[] servID = null;
                    String[] basic = null;
                    String[] agree = null;

                    if (serviceID != null && agreeAmount != null) {
                        servID = serviceID.split(",");
                        basic = basicAmount.split(",");
                        agree = agreeAmount.split(",");
                    }

                    int chk = 0;
                    for (int i = 0; i < servID.length; i++) {
                        chk = serviceDao.allocateAppService(connection, applicantID, agentId, servID[i].trim(),
                                basic[i].trim(), agree[i].trim(), userID);
                    }

                    if (chk > 0) {
                        setMessageString("Applicant Service has been successfully allocated");
                    } else {
                        setMessageString("Applicant Service allocate failed");
                    }
                } else {
                    setMessageString("Service has been already allocated for this applicant");
                }
            } else {
                setMessageString("Please select applicant service");
            }
        }

        return SUCCESS;
    }

    private int allocateServiceCheck(String appid) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectQuery = " SELECT COUNT(APPLICANT_ID) "
                        + " TOTAL "
                        + " FROM "
                        + " applicant_service "
                        + " WHERE "
                        + " APPLICANT_ID = '" + appid + "' ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (Exception ex) {
                count = 0;
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return count;
    }

    public String currentAppName() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            String selectApplicantStatement = " SELECT "
                    + " APPLICANT_NAME "
                    + " FROM "
                    + " applicant_info "
                    + " WHERE "
                    + " APPLICANT_ID = '" + applicantID + "' ";
            try {
                ps = connection.prepareStatement(selectApplicantStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    setCurAppName(rs.getString("APPLICANT_NAME"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return SUCCESS;
    }

    public String viewApplicantService() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setApplicantServiceInfoList(commonList.getAppServHisInfo(connection, applicantID));
        }

        return SUCCESS;
    }

    public String searchAppService() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setApplicantServiceInfoList(commonList.searchAppServHisInfo(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String goStudentServicePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setServiceInfoList(commonList.getServiceInfo(connection, "active"));
            setAgentInfoList(commonList.getUserInfo(connection, "active"));
            setApplicantInfoList(commonList.getApplicantInfo(connection, groupID, userID, ""));
        }

        return SUCCESS;
    }

    public String goApplicantServiceHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setApplicantServiceInfoList(commonList.getAppServHisInfo(connection, "all"));
        }

        return SUCCESS;
    }

    /**
     * @return the serviceInfoList
     */
    public List<ServiceInfo> getServiceInfoList() {
        return serviceInfoList;
    }

    /**
     * @param serviceInfoList the serviceInfoList to set
     */
    public void setServiceInfoList(List<ServiceInfo> serviceInfoList) {
        this.serviceInfoList = serviceInfoList;
    }

    /**
     * @return the applicantInfoList
     */
    public List<ApplicantInfo> getApplicantInfoList() {
        return applicantInfoList;
    }

    /**
     * @param applicantInfoList the applicantInfoList to set
     */
    public void setApplicantInfoList(List<ApplicantInfo> applicantInfoList) {
        this.applicantInfoList = applicantInfoList;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return the applicantID
     */
    public String getApplicantID() {
        return applicantID;
    }

    /**
     * @param applicantID the applicantID to set
     */
    public void setApplicantID(String applicantID) {
        this.applicantID = applicantID;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the basicAmount
     */
    public String getBasicAmount() {
        return basicAmount;
    }

    /**
     * @param basicAmount the basicAmount to set
     */
    public void setBasicAmount(String basicAmount) {
        this.basicAmount = basicAmount;
    }

    /**
     * @return the agreeAmount
     */
    public String getAgreeAmount() {
        return agreeAmount;
    }

    /**
     * @param agreeAmount the agreeAmount to set
     */
    public void setAgreeAmount(String agreeAmount) {
        this.agreeAmount = agreeAmount;
    }

    /**
     * @return the serviceID
     */
    public String getServiceID() {
        return serviceID;
    }

    /**
     * @param serviceID the serviceID to set
     */
    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    /**
     * @return the agentInfoList
     */
    public List<UserInfo> getAgentInfoList() {
        return agentInfoList;
    }

    /**
     * @param agentInfoList the agentInfoList to set
     */
    public void setAgentInfoList(List<UserInfo> agentInfoList) {
        this.agentInfoList = agentInfoList;
    }

    /**
     * @return the applicantServiceInfoList
     */
    public List<ApplicantInfo> getApplicantServiceInfoList() {
        return applicantServiceInfoList;
    }

    /**
     * @param applicantServiceInfoList the applicantServiceInfoList to set
     */
    public void setApplicantServiceInfoList(List<ApplicantInfo> applicantServiceInfoList) {
        this.applicantServiceInfoList = applicantServiceInfoList;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the curAppName
     */
    public String getCurAppName() {
        return curAppName;
    }

    /**
     * @param curAppName the curAppName to set
     */
    public void setCurAppName(String curAppName) {
        this.curAppName = curAppName;
    }
}
