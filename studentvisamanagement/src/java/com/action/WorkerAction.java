package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.StudentDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.ApplicantInfo;
import com.persistance.CountryInfo;
import com.persistance.UserInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class WorkerAction extends ActionSupport {

    private String appType;
    private String agentId;
    private String fullName;
    private String passportNumber;
    private String sector;
    private String dob;
    private String nationality;

    private String mobileNumber;
    private String email;
    private String gender;
    private String maritalStatus;
    private String address;

    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;
    private StudentDao studentDao = null;

    private List<CountryInfo> countryInfoList = null;
    private List<UserInfo> agentInfoList = null;
    private List<ApplicantInfo> applicantInfoList = null;

    public String addNewWorker() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (studentDao == null) {
            studentDao = new StudentDao();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            String applicantID = null;
            if (appIDGenerator() != null) {
                applicantID = appIDGenerator();
            } else {
                applicantID = "10000001";
            }

            int chk = studentDao.addNewWorker(connection, applicantID, fullName, passportNumber, dob, nationality,
                    appType, sector, agentId, userID);

            if (chk > 0) {
                setMessageString("Applicant successfully added");
            } else {
                setMessageString("Applicant add failed");
            }
        }

        return SUCCESS;
    }

    private String appIDGenerator() {

        String applicantID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT "
                        + " LPAD((MAX(A.APP_ID)+1),8,'0') "
                        + " AS APPLICANT_ID "
                        + " FROM "
                        + " (SELECT CONVERT(SUBSTRING(APPLICANT_ID, 1,12),UNSIGNED INTEGER) "
                        + " AS APP_ID FROM applicant_info)A ";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    applicantID = rs.getString("APPLICANT_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return applicantID;
    }

    public String goAddWorkerPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setAgentInfoList(commonList.getUserInfo(connection, "active"));
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String goWorkerHistoryPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setApplicantInfoList(commonList.getApplicantInfo(connection, groupID, userID, "2"));
        }

        return SUCCESS;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the appType
     */
    public String getAppType() {
        return appType;
    }

    /**
     * @param appType the appType to set
     */
    public void setAppType(String appType) {
        this.appType = appType;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the nationality
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * @param nationality the nationality to set
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * @return the mobileNumber
     */
    public String getMobileNumber() {
        return mobileNumber;
    }

    /**
     * @param mobileNumber the mobileNumber to set
     */
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the maritalStatus
     */
    public String getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * @param maritalStatus the maritalStatus to set
     */
    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the agentInfoList
     */
    public List<UserInfo> getAgentInfoList() {
        return agentInfoList;
    }

    /**
     * @param agentInfoList the agentInfoList to set
     */
    public void setAgentInfoList(List<UserInfo> agentInfoList) {
        this.agentInfoList = agentInfoList;
    }

    /**
     * @return the agentId
     */
    public String getAgentId() {
        return agentId;
    }

    /**
     * @param agentId the agentId to set
     */
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    /**
     * @return the passportNumber
     */
    public String getPassportNumber() {
        return passportNumber;
    }

    /**
     * @param passportNumber the passportNumber to set
     */
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    /**
     * @return the sector
     */
    public String getSector() {
        return sector;
    }

    /**
     * @param sector the sector to set
     */
    public void setSector(String sector) {
        this.sector = sector;
    }

    /**
     * @return the applicantInfoList
     */
    public List<ApplicantInfo> getApplicantInfoList() {
        return applicantInfoList;
    }

    /**
     * @param applicantInfoList the applicantInfoList to set
     */
    public void setApplicantInfoList(List<ApplicantInfo> applicantInfoList) {
        this.applicantInfoList = applicantInfoList;
    }
}
