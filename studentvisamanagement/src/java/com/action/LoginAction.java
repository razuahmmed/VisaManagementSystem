package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.common.MacAddress;
import com.model.LoginDao;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

public class LoginAction extends ActionSupport {

    private String userId;
    private String userPassword;
    private String userName;
    private String email;

    private String messageString;
    private String loginMessage;
    private String messageColor;
    private String messageShow;

    private CommonList commonList = null;
    private Connection connection = null;

    private LoginDao loginDao = null;

    public String login() {

        Map session = ActionContext.getContext().getSession();

        if (userName.isEmpty() || userPassword.isEmpty()) {
            setMessageShow("show");
            messageColor = "danger";
            messageString = "Enter any User Name and password.";
            return INPUT;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (loginDao == null) {
            loginDao = new LoginDao();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

//            String encPass = passwordEncryptor(userPassword);
            List<UserInfo> userList = commonList.loginInfo(connection, userName, userPassword);

            if ((!userList.isEmpty()) && (userList.size() > 0)) {

                for (UserInfo user : userList) {

                    if ((user.getUserName().trim().equals(userName)) && (user.getUserPassword().trim().equals(userPassword))) {

                        session.put("USER_ID", user.getUserID());
                        session.put("USER_NAME", user.getUserName());
                        session.put("USER_FULL_NAME", user.getUserFullName());
                        session.put("USER_PHONE", user.getUserPhone());
                        session.put("GROUP_ID", user.getUserGroupInfo().getUserGroupID());
                        session.put("GROUP_NAME", user.getUserGroupInfo().getGroupName());

                        List<UserLevelMenu> menus = commonList.getManus(connection, (String) session.get("USER_ID"), (Integer) session.get("GROUP_ID"));

                        for (UserLevelMenu menu : menus) {
                            session.put(menu.getMenuMaster().getMasterMenuID(), menu.getLevelMenuStatus());
                        }
                        setLoginMessage("1");

//                        String add[] = MacAddress.getMacAddress();
//                        loginDao.updateLoginStatus(connection, add[0].trim(), add[1].trim(), (String) session.get("USER_ID"));
                    } else {
                        setMessageShow("show");
                        messageColor = "danger";
                        messageString = "Invalid User Name or Password.";
                        return ERROR;
                    }
                }
            } else {
                setMessageShow("show");
                messageColor = "danger";
                messageString = "Invalid User Name or Password.";
                return ERROR;
            }
        } else {
            setMessageShow("show");
            messageColor = "danger";
            messageString = "DB Connection not found !";
            return LOGIN;
        }

        return SUCCESS;
    }

    public String passwordEncryptor(String userPassword) {

        String encryptedPassword = null;

        try {
            if (userPassword != null) {
                MessageDigest md = MessageDigest.getInstance("MD5");
                byte[] passDigest = md.digest(userPassword.getBytes());
                BigInteger number = new BigInteger(1, passDigest);
                encryptedPassword = number.toString(16);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return encryptedPassword;
    }

    public String logout() {

        Map session = ActionContext.getContext().getSession();
        session.remove("USER_ID");
        session.remove("USER_NAME");
        session.remove("USER_FULL_NAME");
        session.remove("USER_PHONE");
        session.remove("GROUP_ID");
        session.remove("GROUP_NAME");

        session.clear();

        return SUCCESS;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param aUserId the userId to set
     */
    public void setUserId(String aUserId) {
        userId = aUserId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userPassword
     */
    public String getUserPassword() {
        return userPassword;
    }

    /**
     * @param userPassword the userPassword to set
     */
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the loginMessage
     */
    public String getLoginMessage() {
        return loginMessage;
    }

    /**
     * @param loginMessage the loginMessage to set
     */
    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the messageShow
     */
    public String getMessageShow() {
        return messageShow;
    }

    /**
     * @param messageShow the messageShow to set
     */
    public void setMessageShow(String messageShow) {
        this.messageShow = messageShow;
    }
}
