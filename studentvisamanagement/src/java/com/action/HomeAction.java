package com.action;

import com.common.DBConnection;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;

public class HomeAction extends ActionSupport {

    private String messageString;
    private Integer totalAgent;
    private Integer totalApplicant;
    private String totalAgree;
    private String totalPayment;
    private String totalDue;

    private Connection connection = null;

    public String goDashboardPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        Integer groupID = (Integer) session.get("GROUP_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        setTotalAgent(getAgentTotal());
        setTotalApplicant(getApplicantTotal(userID, groupID));
        setTotalAgree(getAgreeTotal(userID, groupID));
        setTotalPayment(getPaymentTotal(userID, groupID));
        setTotalDue(getDueTotal(userID, groupID));

        return SUCCESS;
    }

    private int getAgentTotal() {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = " SELECT COUNT(USER_NAME) "
                        + " TOTAL "
                        + " FROM "
                        + " user_info "
                        + " WHERE "
                        + " GROUP_ID = 2 ";

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (Exception ex) {
                count = 0;
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return count;
    }

    private int getApplicantTotal(String uID, Integer gID) {

        int count = 0;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = "";
                if (gID == 2) {
                    selectQuery = " SELECT COUNT(APPLICANT_ID) "
                            + " TOTAL "
                            + " FROM "
                            + " applicant_info "
                            + " WHERE "
                            + " USER_ID = '" + uID + "' ";
                } else {
                    selectQuery = " SELECT COUNT(APPLICANT_ID) "
                            + " TOTAL "
                            + " FROM "
                            + " applicant_info ";
                }

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    count = rs.getInt("TOTAL");
                }

            } catch (Exception ex) {
                count = 0;
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return count;
    }

    private String getAgreeTotal(String uID, Integer gID) {

        String total = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = "";
                if (gID == 2) {
                    selectQuery = " SELECT (SUM(A.TUITION_AGREE) + SUM(B.OTHERS_AGREE)) "
                            + " TOTAL_AGREE "
                            + " FROM "
                            + " (SELECT USER_ID, APPLICANT_ID, AGREE_AMOUNT AS TUITION_AGREE FROM applicant_service WHERE SERVICE_ID = 1)A "
                            + " INNER JOIN "
                            + " (SELECT APPLICANT_ID, AGREE_AMOUNT AS OTHERS_AGREE FROM applicant_service WHERE SERVICE_ID = 2)B "
                            + " ON A.APPLICANT_ID = B.APPLICANT_ID "
                            + " WHERE "
                            + " A.USER_ID = '" + uID + "' ";
                } else {
                    selectQuery = " SELECT (SUM(A.TUITION_AGREE) + SUM(B.OTHERS_AGREE)) "
                            + " TOTAL_AGREE "
                            + " FROM "
                            + " (SELECT APPLICANT_ID, AGREE_AMOUNT AS TUITION_AGREE FROM applicant_service WHERE SERVICE_ID = 1)A "
                            + " INNER JOIN "
                            + " (SELECT APPLICANT_ID, AGREE_AMOUNT AS OTHERS_AGREE FROM applicant_service WHERE SERVICE_ID = 2)B "
                            + " ON A.APPLICANT_ID = B.APPLICANT_ID ";
                }

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    total = rs.getString("TOTAL_AGREE");
                }

            } catch (Exception ex) {
                total = "";
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return total;
    }

    private String getPaymentTotal(String uID, Integer gID) {

        String total = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = "";
                if (gID == 2) {
                    selectQuery = " SELECT (SUM(TUITION_FEE)+SUM(OTHERS_AMOUNT)) "
                            + " TOTAL "
                            + " FROM "
                            + " payment_info "
                            + " WHERE "
                            + " USER_ID = '" + uID + "' ";
                } else {
                    selectQuery = " SELECT (SUM(TUITION_FEE)+SUM(OTHERS_AMOUNT)) "
                            + " TOTAL "
                            + " FROM "
                            + " payment_info ";
                }

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    total = rs.getString("TOTAL");
                }

            } catch (Exception ex) {
                total = "";
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return total;
    }

    private String getDueTotal(String uID, Integer gID) {

        String total = "";
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {
                String selectQuery = "";
                if (gID == 2) {
                    selectQuery = " SELECT (SUM(B.TUITION_AGREE) + SUM(C.OTHERS_AGREE)) "
                            + "  - "
                            + " (SUM(A.TUITION_FEE) + SUM(A.OTHERS_AMOUNT)) "
                            + " TOTAL_DUE "
                            + " FROM "
                            + " payment_info A "
                            + " INNER JOIN "
                            + " ((SELECT APPLICANT_ID, AGREE_AMOUNT AS TUITION_AGREE "
                            + " FROM applicant_service WHERE SERVICE_ID = 1)B) "
                            + " ON A.APPLICANT_ID = B.APPLICANT_ID "
                            + " INNER JOIN "
                            + " ((SELECT APPLICANT_ID, AGREE_AMOUNT AS OTHERS_AGREE "
                            + " FROM applicant_service WHERE SERVICE_ID = 2)C) "
                            + " ON A.APPLICANT_ID = C.APPLICANT_ID "
                            + " WHERE A.USER_ID = '" + uID + "' ";
                } else {
                    selectQuery = " SELECT (SUM(B.TUITION_AGREE) + SUM(C.OTHERS_AGREE)) "
                            + "  - "
                            + " (SUM(A.TUITION_FEE) + SUM(A.OTHERS_AMOUNT)) "
                            + " TOTAL_DUE "
                            + " FROM "
                            + " payment_info A "
                            + " INNER JOIN "
                            + " ((SELECT APPLICANT_ID, AGREE_AMOUNT AS TUITION_AGREE "
                            + " FROM applicant_service WHERE SERVICE_ID = 1)B) "
                            + " ON A.APPLICANT_ID = B.APPLICANT_ID "
                            + " INNER JOIN "
                            + " ((SELECT APPLICANT_ID, AGREE_AMOUNT AS OTHERS_AGREE "
                            + " FROM applicant_service WHERE SERVICE_ID = 2)C) "
                            + " ON A.APPLICANT_ID = C.APPLICANT_ID ";
                }

                ps = connection.prepareStatement(selectQuery);
                rs = ps.executeQuery();

                while (rs.next()) {
                    total = rs.getString("TOTAL_DUE");
                }

            } catch (Exception ex) {
                total = "";
                ex.printStackTrace();
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        return total;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the totalAgent
     */
    public Integer getTotalAgent() {
        return totalAgent;
    }

    /**
     * @param totalAgent the totalAgent to set
     */
    public void setTotalAgent(Integer totalAgent) {
        this.totalAgent = totalAgent;
    }

    /**
     * @return the totalApplicant
     */
    public Integer getTotalApplicant() {
        return totalApplicant;
    }

    /**
     * @param totalApplicant the totalApplicant to set
     */
    public void setTotalApplicant(Integer totalApplicant) {
        this.totalApplicant = totalApplicant;
    }

    /**
     * @return the totalDue
     */
    public String getTotalDue() {
        return totalDue;
    }

    /**
     * @param totalDue the totalDue to set
     */
    public void setTotalDue(String totalDue) {
        this.totalDue = totalDue;
    }

    /**
     * @return the totalPayment
     */
    public String getTotalPayment() {
        return totalPayment;
    }

    /**
     * @param totalPayment the totalPayment to set
     */
    public void setTotalPayment(String totalPayment) {
        this.totalPayment = totalPayment;
    }

    /**
     * @return the totalAgree
     */
    public String getTotalAgree() {
        return totalAgree;
    }

    /**
     * @param totalAgree the totalAgree to set
     */
    public void setTotalAgree(String totalAgree) {
        this.totalAgree = totalAgree;
    }
}
