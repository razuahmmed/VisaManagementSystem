package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.CountryInfo;
import com.persistance.MenuMaster;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

public class AdminAction extends ActionSupport {

    private String userGroupID;
    private String formMenuIdStatus;

    private String messageColor;
    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;

    private List<MenuMaster> menuMasterInfoList = null;
    private List<UserGroup> userGroupInfoList = null;
    private List<UserLevelMenu> userLevelMenuInfoList = null;
    private List<CountryInfo> countryInfoList = null;
    private List<UserInfo> adminUserInfoList = null;

    public String menuBySelectedGroup() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {

            List<UserLevelMenu> checked = commonList.getMenuByGroup(connection, userGroupID);
            if (checked.size() != 0) {
                setUserLevelMenuInfoList(commonList.getMenuByGroup(connection, userGroupID));
            } else {
                setMenuMasterInfoList(commonList.getMenuParentInfo(connection));
            }
        }

        return SUCCESS;
    }

    public String saveMenuPermission() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");
        String groupName = (String) session.get("GROUP_NAME");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        PreparedStatement ps = null;

        String[] firstArray = null;
        String[] secondArray = null;
        String menuID = null;
        String menuStatus = null;

        String insertStatement = "";
        String updateStatement = "";
        int status = 0;

        if (connection != null) {
            if ((userGroupID != null) && (formMenuIdStatus != null)) {
                if ((!userGroupID.isEmpty()) && (!formMenuIdStatus.toString().isEmpty())) {
                    firstArray = formMenuIdStatus.split("/mstatus/");
                    if (firstArray != null) {
                        for (int i = 0; i < firstArray.length; i++) {
                            if ((firstArray[i] != null) && (!firstArray[i].isEmpty())) {
                                secondArray = firstArray[i].split("/mid/");
                                if (secondArray != null) {
                                    menuID = secondArray[0];
                                    menuStatus = secondArray[1];
                                    if ((menuID != null) && (menuStatus != null)) {
                                        if ((!menuID.isEmpty()) && (!menuStatus.isEmpty())) {
                                            try {

                                                status = 0;

                                                updateStatement = " UPDATE "
                                                        + " user_level_menu "
                                                        + " SET "
                                                        + " LEVEL_MENU_STATUS = '" + menuStatus + "', "
                                                        + " UPDATE_BY = '" + groupName + "', "
                                                        + " UPDATE_DATE = CURRENT_TIMESTAMP "
                                                        + " WHERE "
                                                        + " GROUP_ID = '" + userGroupID + "' "
                                                        + " AND "
                                                        + " MENU_ID = '" + menuID + "' ";

                                                ps = connection.prepareStatement(updateStatement);
                                                status = ps.executeUpdate();

                                                if (status == 0) {

                                                    insertStatement = " INSERT INTO "
                                                            + " user_level_menu( "
                                                            + " MENU_ID, "
                                                            + " GROUP_ID, "
                                                            + " LEVEL_MENU_STATUS, "
                                                            + " INSERT_BY, "
                                                            + " INSERT_DATE "
                                                            + " )VALUES( "
                                                            + "'" + menuID + "', "
                                                            + "'" + userGroupID + "', "
                                                            + "'" + menuStatus + "', "
                                                            + "'" + groupName + "', "
                                                            + " CURRENT_TIMESTAMP)";

                                                    ps = connection.prepareStatement(insertStatement);
                                                    status = ps.executeUpdate();
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (status > 0) {
            setMessageColor("success");
            setMessageString("Menu permission saved successfully.");
        } else {
            setMessageColor("danger");
            setMessageString("Menu permission save failed!");
        }

        return SUCCESS;
    }

    public static void main(String[] args) {
        try {
            PreparedStatement ps = null;
            Connection c = DBConnection.getMySqlConnection();
            String deleteStatement = " DELETE FROM "
                    + " user_level_menu ";
            ps = c.prepareStatement(deleteStatement);
//            ps.executeUpdate();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public String goAddAdminUserPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String goManageAdminUserPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }
        if (connection != null) {
            setAdminUserInfoList(commonList.getUserInfo(connection, "aduser"));
        }

        return SUCCESS;
    }

    public String goActivityLogPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goMenuSettingsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setMenuMasterInfoList(commonList.getMenuParentInfo(connection));
            setUserGroupInfoList(commonList.getUserGroup(connection));
        }

        return SUCCESS;
    }

    /**
     * @return the menuMasterInfoList
     */
    public List<MenuMaster> getMenuMasterInfoList() {
        return menuMasterInfoList;
    }

    /**
     * @param menuMasterInfoList the menuMasterInfoList to set
     */
    public void setMenuMasterInfoList(List<MenuMaster> menuMasterInfoList) {
        this.menuMasterInfoList = menuMasterInfoList;
    }

    /**
     * @return the userGroupInfoList
     */
    public List<UserGroup> getUserGroupInfoList() {
        return userGroupInfoList;
    }

    /**
     * @param userGroupInfoList the userGroupInfoList to set
     */
    public void setUserGroupInfoList(List<UserGroup> userGroupInfoList) {
        this.userGroupInfoList = userGroupInfoList;
    }

    /**
     * @return the userGroupID
     */
    public String getUserGroupID() {
        return userGroupID;
    }

    /**
     * @param userGroupID the userGroupID to set
     */
    public void setUserGroupID(String userGroupID) {
        this.userGroupID = userGroupID;
    }

    /**
     * @return the formMenuIdStatus
     */
    public String getFormMenuIdStatus() {
        return formMenuIdStatus;
    }

    /**
     * @param formMenuIdStatus the formMenuIdStatus to set
     */
    public void setFormMenuIdStatus(String formMenuIdStatus) {
        this.formMenuIdStatus = formMenuIdStatus;
    }

    /**
     * @return the messageColor
     */
    public String getMessageColor() {
        return messageColor;
    }

    /**
     * @param messageColor the messageColor to set
     */
    public void setMessageColor(String messageColor) {
        this.messageColor = messageColor;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the userLevelMenuInfoList
     */
    public List<UserLevelMenu> getUserLevelMenuInfoList() {
        return userLevelMenuInfoList;
    }

    /**
     * @param userLevelMenuInfoList the userLevelMenuInfoList to set
     */
    public void setUserLevelMenuInfoList(List<UserLevelMenu> userLevelMenuInfoList) {
        this.userLevelMenuInfoList = userLevelMenuInfoList;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the adminUserInfoList
     */
    public List<UserInfo> getAdminUserInfoList() {
        return adminUserInfoList;
    }

    /**
     * @param adminUserInfoList the adminUserInfoList to set
     */
    public void setAdminUserInfoList(List<UserInfo> adminUserInfoList) {
        this.adminUserInfoList = adminUserInfoList;
    }
}
