package com.action;

import com.common.CommonList;
import com.common.DBConnection;
import com.model.SettingsDao;
import static com.opensymphony.xwork2.Action.LOGIN;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.persistance.CountryInfo;
import com.persistance.ServiceInfo;
import com.persistance.VarsityInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class SettingsAction extends ActionSupport {

    private String varsityId;
    private String varsityName;
    private String varsityContact;
    private String varsityEmail;
    private String varsityCountry;
    private Character varsityStatus;
    private String varsityAddress;
    private String varsityNotes;

    private String courseId;
    private String courseName;
    private String courseStatus;

    private String fieldName;
    private String fieldValue;

    private String serviceId;
    private String serviceType;
    private String serviceName;
    private String serviceAmount;
    private Character serviceStatus;

    private String messageString;

    private Connection connection = null;
    private CommonList commonList = null;

    private List<ServiceInfo> serviceInfoList = null;
    private List<VarsityInfo> varsityInfoList = null;
    private List<VarsityInfo> singleVarsityInfoList = null;
    private List<CountryInfo> countryInfoList = null;

    private SettingsDao settingsDao = null;

    public String addNewUniversity() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (settingsDao == null) {
            settingsDao = new SettingsDao();
        }

        if (connection != null) {

            String uniVarID = null;
            if (varsityIDGenerator() != null) {
                uniVarID = varsityIDGenerator();
            } else {
                uniVarID = "1";
            }

            int chk = 0;

            String[] course = null;
            if (courseName != null) {
                course = courseName.split(",");
            }

            for (int i = 0; i < course.length; i++) {

                String varCourseID = null;
                if (courseIDGenerator() != null) {
                    varCourseID = courseIDGenerator();
                } else {
                    varCourseID = "1";
                }

                chk = settingsDao.addNewVarsity(connection, uniVarID, varsityName, varsityContact, varsityEmail, varsityCountry,
                        varsityAddress, varsityNotes, varCourseID, course[i].trim(), userID);

            }

            if (chk > 0) {
                setMessageString("Varsity Courses successfully added");
            } else {
                setMessageString("Varsity Courses added faild");
            }
        }

        return SUCCESS;
    }

    public String viewVarsityCourseInfo() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setSingleVarsityInfoList(commonList.getVarsityCourseInfo(connection, "all", varsityId));
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String editUniversityCourses() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (settingsDao == null) {
            settingsDao = new SettingsDao();
        }

        if (connection != null) {

            String[] cidArr = null;
            String[] cnmArr = null;
            String[] cstsArr = null;

            if (courseId != null && courseName != null) {
                cidArr = courseId.split(",");
                cnmArr = courseName.split(",");
                cstsArr = courseStatus.split(",");
            }

            for (int i = 0; i < cidArr.length; i++) {
                int chk = settingsDao.updateVarsityCourses(connection, varsityId, varsityName, varsityContact, varsityEmail,
                        varsityCountry, varsityStatus, varsityAddress, varsityNotes, cidArr[i].trim(), cnmArr[i].trim(),
                        cstsArr[i].trim(), userID);
                if (chk > 0) {
                    setMessageString("Varsity Courses successfully edited");
                } else {
                    setMessageString("Varsity Courses edit faild");
                }
            }
        }

        return SUCCESS;
    }

    private String varsityIDGenerator() {

        String varID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT MAX((VARSITY_ID)+1) AS VAR_ID FROM varsity_info";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    varID = rs.getString("VAR_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return varID;
    }

    private String courseIDGenerator() {

        String courseID = "";

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {

            try {

                String selectStatement = " SELECT MAX((COURSE_ID)+1) AS COR_ID FROM varsity_info";

                ps = connection.prepareStatement(selectStatement);
                rs = ps.executeQuery();

                while (rs.next()) {
                    courseID = rs.getString("COR_ID");
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return courseID;
    }

    public String addNewService() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (settingsDao == null) {
            settingsDao = new SettingsDao();
        }

        if (connection != null) {

            String[] sName = null;
            String[] sAmount = null;

            if ((serviceName != null) && (!serviceName.equals(""))) {
                sName = serviceName.split(",");
                sAmount = serviceAmount.split(",");
            }

            int chk = 0;
            if ((sName != null) && (!sName.equals(""))) {
                for (int i = 0; i < sName.length; i++) {
                    chk = settingsDao.addNewServiceInfo(connection, sName[i].trim(), sAmount[i].trim(), userID);
                }
            }

            if (chk > 0) {
                setMessageString("Service successfully created");
            } else {
                setMessageString("Service create failed");
            }
        }

        return SUCCESS;
    }

    public String viewServiceSettings() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setServiceInfoList(commonList.getServiceInfo(connection, serviceId));
        }

        return SUCCESS;
    }

    public String editService() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (settingsDao == null) {
            settingsDao = new SettingsDao();
        }

        if (connection != null) {
            int chk = settingsDao.updateServiceInfo(connection, serviceName, serviceAmount, serviceStatus, serviceId, userID);
            if (chk > 0) {
                setMessageString("Service successfully edited");
            } else {
                setMessageString("Service edit failed");
            }
        }

        return SUCCESS;
    }

    public String searchVarHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setVarsityInfoList(commonList.searchVarsityCourseInfo(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String searchServiceHistory() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setServiceInfoList(commonList.searchServiceInfo(connection, fieldName, fieldValue));
        }

        return SUCCESS;
    }

    public String goUniversitySetupPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setCountryInfoList(commonList.getCountryInfo(connection));
        }

        return SUCCESS;
    }

    public String goManageVarsityCoursesPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection != null) {
            setVarsityInfoList(commonList.getVarsityCourseInfo(connection, "all", "all"));
        }

        return SUCCESS;
    }

    public String goServiceSettingsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goManageServicePage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        if (commonList == null) {
            commonList = new CommonList();
        }

        if (connection == null) {
            connection = DBConnection.getMySqlConnection();
        }

        if (connection != null) {
            setServiceInfoList(commonList.getServiceInfo(connection, "all"));
        }

        return SUCCESS;
    }

    public String goPaymentTramsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goAccountsHeadPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    public String goEmployeeSettingsPage() {

        Map session = ActionContext.getContext().getSession();
        String userID = (String) session.get("USER_ID");

        if (userID == null || userID.length() == 0) {
            return LOGIN;
        }

        return SUCCESS;
    }

    /**
     * @return the varsityName
     */
    public String getVarsityName() {
        return varsityName;
    }

    /**
     * @param varsityName the varsityName to set
     */
    public void setVarsityName(String varsityName) {
        this.varsityName = varsityName;
    }

    /**
     * @return the varsityContact
     */
    public String getVarsityContact() {
        return varsityContact;
    }

    /**
     * @param varsityContact the varsityContact to set
     */
    public void setVarsityContact(String varsityContact) {
        this.varsityContact = varsityContact;
    }

    /**
     * @return the varsityEmail
     */
    public String getVarsityEmail() {
        return varsityEmail;
    }

    /**
     * @param varsityEmail the varsityEmail to set
     */
    public void setVarsityEmail(String varsityEmail) {
        this.varsityEmail = varsityEmail;
    }

    /**
     * @return the varsityCountry
     */
    public String getVarsityCountry() {
        return varsityCountry;
    }

    /**
     * @param varsityCountry the varsityCountry to set
     */
    public void setVarsityCountry(String varsityCountry) {
        this.varsityCountry = varsityCountry;
    }

    /**
     * @return the varsityAddress
     */
    public String getVarsityAddress() {
        return varsityAddress;
    }

    /**
     * @param varsityAddress the varsityAddress to set
     */
    public void setVarsityAddress(String varsityAddress) {
        this.varsityAddress = varsityAddress;
    }

    /**
     * @return the varsityNotes
     */
    public String getVarsityNotes() {
        return varsityNotes;
    }

    /**
     * @param varsityNotes the varsityNotes to set
     */
    public void setVarsityNotes(String varsityNotes) {
        this.varsityNotes = varsityNotes;
    }

    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the messageString
     */
    public String getMessageString() {
        return messageString;
    }

    /**
     * @param messageString the messageString to set
     */
    public void setMessageString(String messageString) {
        this.messageString = messageString;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return the serviceAmount
     */
    public String getServiceAmount() {
        return serviceAmount;
    }

    /**
     * @param serviceAmount the serviceAmount to set
     */
    public void setServiceAmount(String serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    /**
     * @return the serviceInfoList
     */
    public List<ServiceInfo> getServiceInfoList() {
        return serviceInfoList;
    }

    /**
     * @param serviceInfoList the serviceInfoList to set
     */
    public void setServiceInfoList(List<ServiceInfo> serviceInfoList) {
        this.serviceInfoList = serviceInfoList;
    }

    /**
     * @return the varsityInfoList
     */
    public List<VarsityInfo> getVarsityInfoList() {
        return varsityInfoList;
    }

    /**
     * @param varsityInfoList the varsityInfoList to set
     */
    public void setVarsityInfoList(List<VarsityInfo> varsityInfoList) {
        this.varsityInfoList = varsityInfoList;
    }

    /**
     * @return the varsityId
     */
    public String getVarsityId() {
        return varsityId;
    }

    /**
     * @param varsityId the varsityId to set
     */
    public void setVarsityId(String varsityId) {
        this.varsityId = varsityId;
    }

    /**
     * @return the singleVarsityInfoList
     */
    public List<VarsityInfo> getSingleVarsityInfoList() {
        return singleVarsityInfoList;
    }

    /**
     * @param singleVarsityInfoList the singleVarsityInfoList to set
     */
    public void setSingleVarsityInfoList(List<VarsityInfo> singleVarsityInfoList) {
        this.singleVarsityInfoList = singleVarsityInfoList;
    }

    /**
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId the serviceId to set
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return the courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the varsityStatus
     */
    public Character getVarsityStatus() {
        return varsityStatus;
    }

    /**
     * @param varsityStatus the varsityStatus to set
     */
    public void setVarsityStatus(Character varsityStatus) {
        this.varsityStatus = varsityStatus;
    }

    /**
     * @return the courseStatus
     */
    public String getCourseStatus() {
        return courseStatus;
    }

    /**
     * @param courseStatus the courseStatus to set
     */
    public void setCourseStatus(String courseStatus) {
        this.courseStatus = courseStatus;
    }

    /**
     * @return the countryInfoList
     */
    public List<CountryInfo> getCountryInfoList() {
        return countryInfoList;
    }

    /**
     * @param countryInfoList the countryInfoList to set
     */
    public void setCountryInfoList(List<CountryInfo> countryInfoList) {
        this.countryInfoList = countryInfoList;
    }

    /**
     * @return the serviceStatus
     */
    public Character getServiceStatus() {
        return serviceStatus;
    }

    /**
     * @param serviceStatus the serviceStatus to set
     */
    public void setServiceStatus(Character serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    /**
     * @return the fieldName
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * @param fieldName the fieldName to set
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     * @return the fieldValue
     */
    public String getFieldValue() {
        return fieldValue;
    }

    /**
     * @param fieldValue the fieldValue to set
     */
    public void setFieldValue(String fieldValue) {
        this.fieldValue = fieldValue;
    }

    /**
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType the serviceType to set
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }
}
