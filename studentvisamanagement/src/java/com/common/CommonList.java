package com.common;

import com.persistance.ApplicantCoursesInfo;
import com.persistance.ApplicantEducationInfo;
import com.persistance.ApplicantInfo;
import com.persistance.ApplicantServiceInfo;
import com.persistance.CountryInfo;
import com.persistance.CoursesInfo;
import com.persistance.IssuesInfo;
import com.persistance.MenuChild;
import com.persistance.MenuMaster;
import com.persistance.ServiceInfo;
import com.persistance.PaymentInfo;
import com.persistance.UserGroup;
import com.persistance.UserInfo;
import com.persistance.UserLevelMenu;
import com.persistance.VarsityInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommonList {

    public List<UserInfo> loginInfo(Connection connection, String userName, String password) {

        UserInfo userInfo = null;
        UserGroup userGroupInfo = null;
        List<UserInfo> userInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectLoginInfo(userName, password));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();
                    userGroupInfo = new UserGroup();

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserPassword(rs.getString("USER_PASSWORD"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));

                    userGroupInfo.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroupInfo.setGroupName(rs.getString("GROUP_NAME"));
                    userGroupInfo.setGroupType(rs.getString("GROUP_TYPE"));

                    userInfo.setUserGroupInfo(userGroupInfo);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<UserInfo> getUserInfo(Connection connection, String type) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        CountryInfo countryInfo = null;
        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectUserInfo(type));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    countryInfo = new CountryInfo();
                    userGroup = new UserGroup();

//                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));
                    userInfo.setUserFax(rs.getString("USER_FAX"));
                    userInfo.setUserEmail(rs.getString("USER_EMAIL"));
                    userInfo.setUserStatus(rs.getString("USER_STATUS").charAt(0));
                    userInfo.setCompany(rs.getString("COMPANY_ID"));
                    userInfo.setUserPassword(rs.getString("USER_PASSWORD"));
                    userInfo.setUserNotes(rs.getString("USER_NOTES"));
                    userInfo.setUserAddress(rs.getString("USER_ADDRESS"));

//                    Date dob = rs.getDate("USER_DOB");
//                    String dobDate = formatter.format(dob);
                    userInfo.setUserDob(rs.getString("USER_DOB"));

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    userInfo.setCountryInfo(countryInfo);

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userInfo.setUserGroupInfo(userGroup);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<UserInfo> searchUserInfo(Connection connection, String fName, String fValue) {

        UserInfo userInfo = null;
        List<UserInfo> userInfoList = null;

        CountryInfo countryInfo = null;
        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchUserInfo(fName, fValue));
                rs = ps.executeQuery();

                userInfoList = new ArrayList<UserInfo>();

                while (rs.next()) {

                    userInfo = new UserInfo();

                    countryInfo = new CountryInfo();
                    userGroup = new UserGroup();

//                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserName(rs.getString("USER_NAME"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));
                    userInfo.setUserFax(rs.getString("USER_FAX"));
                    userInfo.setUserEmail(rs.getString("USER_EMAIL"));
                    userInfo.setUserStatus(rs.getString("USER_STATUS").charAt(0));
                    userInfo.setCompany(rs.getString("COMPANY_ID"));
                    userInfo.setUserPassword(rs.getString("USER_PASSWORD"));
                    userInfo.setUserNotes(rs.getString("USER_NOTES"));
                    userInfo.setUserAddress(rs.getString("USER_ADDRESS"));

//                    Date dob = rs.getDate("USER_DOB");
//                    String dobDate = formatter.format(dob);
                    userInfo.setUserDob(rs.getString("USER_DOB"));

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    userInfo.setCountryInfo(countryInfo);

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userInfo.setUserGroupInfo(userGroup);

                    userInfoList.add(userInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                userInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userInfoList;
    }

    public List<CountryInfo> getCountryInfo(Connection connection) {

        CountryInfo countryInfo = null;
        List<CountryInfo> countryInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCountryInfo());
                rs = ps.executeQuery();

                countryInfoList = new ArrayList<CountryInfo>();

                while (rs.next()) {

                    countryInfo = new CountryInfo();

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    countryInfo.setCountryStatus(rs.getString("COUNTRY_STATUS").charAt(0));

                    countryInfoList.add(countryInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                countryInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return countryInfoList;
    }

    public List<ApplicantInfo> getApplicantInfo(Connection connection, Integer groupid, String userid, String appType) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfoList = null;

        UserInfo userInfo = null;
        CountryInfo countryInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectApplicantInfo(groupid, userid, appType));
                rs = ps.executeQuery();

                applicantInfoList = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    userInfo = new UserInfo();
                    countryInfo = new CountryInfo();

                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setDob(rs.getString("APP_DOB"));
                    applicantInfo.setMobileNumber(rs.getString("APP_CONTACT"));
                    applicantInfo.setPassportNumber(rs.getString("APP_PASSPORT_NO"));
                    applicantInfo.setFathersName(rs.getString("APP_FATHER"));
                    applicantInfo.setMothersName(rs.getString("APP_MOTHER"));
                    applicantInfo.setApplicantEmail(rs.getString("APP_EMAIL"));
                    applicantInfo.setVisaExpiryDate(rs.getString("VISA_EX_DATE"));
                    applicantInfo.setApplicantType(rs.getString("APP_TYPE"));
                    applicantInfo.setApplicantSector(rs.getString("SECTOR"));

                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));
                    applicantInfo.setUserInfo(userInfo);

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    applicantInfo.setCountryInfo(countryInfo);

                    // Applicant varsity information
                    applicantInfo.setAppVarsityInfoList(getAppVarsity(connection, rs.getString("APPLICANT_ID")));
                    // Applicant courses information
                    applicantInfo.setAppCoursesInfoList(getVarsityCoursesInfo(connection, rs.getString("APPLICANT_ID")));

                    applicantInfo.setPaymentInfo(getPayDueHistory(connection, groupid, userid, rs.getString("APPLICANT_ID")));

                    applicantInfoList.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfoList;
    }

    public List<ApplicantInfo> getApplicantByAgent(Connection connection, String userid) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfoList = null;

        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectApplicantByAgent(userid));
                rs = ps.executeQuery();

                applicantInfoList = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    userInfo = new UserInfo();

                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantType(rs.getString("APP_TYPE"));

                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    applicantInfo.setUserInfo(userInfo);

                    applicantInfoList.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfoList;
    }

    public List<ApplicantInfo> searchApplicantInfo(Connection connection, String fName, String fValue) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfoList = null;

        UserInfo userInfo = null;
        CountryInfo countryInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchApplicantInfo(fName, fValue));
                rs = ps.executeQuery();

                applicantInfoList = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    userInfo = new UserInfo();
                    countryInfo = new CountryInfo();

                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setDob(rs.getString("APP_DOB"));
                    applicantInfo.setMobileNumber(rs.getString("APP_CONTACT"));
                    applicantInfo.setPassportNumber(rs.getString("APP_PASSPORT_NO"));
                    applicantInfo.setFathersName(rs.getString("APP_FATHER"));
                    applicantInfo.setMothersName(rs.getString("APP_MOTHER"));
                    applicantInfo.setApplicantEmail(rs.getString("APP_EMAIL"));
                    applicantInfo.setVisaExpiryDate(rs.getString("VISA_EX_DATE"));

                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));
                    applicantInfo.setUserInfo(userInfo);

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    applicantInfo.setCountryInfo(countryInfo);

                    // Applicant varsity information
                    applicantInfo.setAppVarsityInfoList(getAppVarsity(connection, rs.getString("APPLICANT_ID")));
                    // Applicant courses information
                    applicantInfo.setAppCoursesInfoList(getVarsityCoursesInfo(connection, rs.getString("APPLICANT_ID")));

                    applicantInfo.setPaymentInfo(getPayDueHistory(connection, 0, "", rs.getString("APPLICANT_ID")));
                    applicantInfoList.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfoList;
    }

    public List<ApplicantInfo> getSingleApplicantInfo(Connection connection, String appID) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfoList = null;

        ApplicantEducationInfo educationInfo = null;
        ApplicantCoursesInfo coursesInfo = null;
        UserInfo userInfo = null;
        CountryInfo countryInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSingleApplicantInfo(appID));
                rs = ps.executeQuery();

                applicantInfoList = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    educationInfo = new ApplicantEducationInfo();
                    coursesInfo = new ApplicantCoursesInfo();
                    userInfo = new UserInfo();
                    countryInfo = new CountryInfo();

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    applicantInfo.setNationality(rs.getString("APP_NATIONALITY"));
                    applicantInfo.setPassportNumber(rs.getString("APP_PASSPORT_NO"));
                    applicantInfo.setAlternatePassportNo(rs.getString("APP_ALT_PASSPORT_NO"));
                    applicantInfo.setDob(rs.getString("APP_DOB"));
                    applicantInfo.setPassportIssuedPlace(rs.getString("PASS_ISSUED_PLACE"));
                    applicantInfo.setPassportIssueDate(rs.getString("PASS_ISSUED_DATE"));
                    applicantInfo.setPassportExpiryDate(rs.getString("PASS_EXPIRY_DATE"));
                    applicantInfo.setGender(rs.getString("APP_GENDER"));
                    applicantInfo.setApplicantEmail(rs.getString("APP_EMAIL"));
                    applicantInfo.setApplicantAlternateEmail(rs.getString("APP_ALT_EMAIL"));
                    applicantInfo.setMaritalStatus(rs.getString("APP_MARITAL_STATUS"));
                    applicantInfo.setMobileNumber(rs.getString("APP_CONTACT"));
                    applicantInfo.setCity(rs.getString("APP_CITY"));
                    applicantInfo.setPostalCode(rs.getString("APP_POSTAL_CODE"));
                    applicantInfo.setState(rs.getString("APP_STATE"));
                    applicantInfo.setResidentNumber(rs.getString("APP_RESD_NO"));
                    applicantInfo.setPermanentAddress(rs.getString("APP_PERM_ADDR"));
                    applicantInfo.setCorMobileNumber(rs.getString("COR_CONTACT"));
                    applicantInfo.setCorCity(rs.getString("COR_CITY"));
                    applicantInfo.setCorPostalCode(rs.getString("COR_POSTAL_CODE"));
                    applicantInfo.setCorState(rs.getString("COR_SATE"));
                    applicantInfo.setCorResidentNumber(rs.getString("COR_RESD_NO"));
                    applicantInfo.setCorAddress(rs.getString("COR_ADDRESS"));
                    applicantInfo.setFathersName(rs.getString("APP_FATHER"));
                    applicantInfo.setFathersOccupation(rs.getString("FATHER_OCCUPATION"));
                    applicantInfo.setMothersName(rs.getString("APP_MOTHER"));
                    applicantInfo.setMothersOccupation(rs.getString("MOTHER_OCCUPATION"));
                    applicantInfo.setHighestQualification(rs.getString("HIGHT_QUALI"));
                    applicantInfo.setHqPassYear(rs.getString("PASS_YEAR"));
                    applicantInfo.setOverAllGrade(rs.getString("OVER_ALL_GRADE"));
                    applicantInfo.setVisaCountry(rs.getString("VISA_COUNTRY"));
                    applicantInfo.setVisaExpiryDate(rs.getString("VISA_EX_DATE"));
                    applicantInfo.setVisaReleaseLetter(rs.getString("VISA_LETTER_ISSUE"));
                    applicantInfo.setVisaColName(rs.getString("VISA_COLLEGE"));
                    applicantInfo.setVisaType(rs.getString("VISA_TYPE"));
                    applicantInfo.setArrivalDate(rs.getString("ARRIVAL_DATE"));

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    userInfo.setUserPhone(rs.getString("USER_PHONE"));
                    applicantInfo.setUserInfo(userInfo);

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    applicantInfo.setCountryInfo(countryInfo);

                    // Applicant education information
                    applicantInfo.setEduInfoList(geEduInfo(connection, appID));
                    // Applicant varsity information
                    applicantInfo.setAppVarsityInfoList(getAppVarsity(connection, appID));
                    // Applicant courses information
                    applicantInfo.setAppCoursesInfoList(getVarsityCoursesInfo(connection, appID));

                    applicantInfoList.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfoList;
    }

    public List<ApplicantEducationInfo> geEduInfo(Connection connection, String appId) {

        ApplicantEducationInfo educationInfo = null;
        List<ApplicantEducationInfo> educationInfos = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectEducationInfo(appId));
                rs = ps.executeQuery();

                educationInfos = new ArrayList<ApplicantEducationInfo>();

                while (rs.next()) {

                    educationInfo = new ApplicantEducationInfo();

                    educationInfo.setAppEducationID(rs.getInt("APP_EDU_ID"));
                    educationInfo.setDegreeName(rs.getString("DEGREE_NAME"));
                    educationInfo.setResult(rs.getString("RESULT"));
                    educationInfo.setPassingYear(rs.getString("PASSING_YEAR"));
                    educationInfo.setInstituteName(rs.getString("INSTITUTION"));

                    educationInfos.add(educationInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                educationInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return educationInfos;
    }

    public List<ApplicantCoursesInfo> getVarsityCoursesInfo(Connection connection, String appId) {

        ApplicantCoursesInfo coursesInfo = null;
        List<ApplicantCoursesInfo> appCoursesInfos = null;

        VarsityInfo varsityInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectVarCourseInfo(appId));
                rs = ps.executeQuery();

                appCoursesInfos = new ArrayList<ApplicantCoursesInfo>();

                while (rs.next()) {

                    coursesInfo = new ApplicantCoursesInfo();

                    varsityInfo = new VarsityInfo();

                    coursesInfo.setCourseTableID(rs.getInt("APP_COURSE_ID"));

                    varsityInfo.setVarsityID(rs.getInt("VARSITY_ID"));
                    varsityInfo.setVarsityName(rs.getString("VARSITY_NAME"));
                    varsityInfo.setVarsityStatus(rs.getString("VARSITY_STATUS").charAt(0));
                    varsityInfo.setCourseID(rs.getInt("COURSE_ID"));
                    varsityInfo.setCourseName(rs.getString("COURSE_NAME"));
                    varsityInfo.setCourseStatus(rs.getString("COURSE_STATUS").charAt(0));
                    coursesInfo.setVarsity(varsityInfo);

                    appCoursesInfos.add(coursesInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                appCoursesInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return appCoursesInfos;
    }

    public List<ApplicantCoursesInfo> getAppVarsity(Connection connection, String appId) {

        ApplicantCoursesInfo coursesInfo = null;
        List<ApplicantCoursesInfo> coursesInfos = null;

        VarsityInfo varsityInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAppVarsity(appId));
                rs = ps.executeQuery();

                coursesInfos = new ArrayList<ApplicantCoursesInfo>();

                while (rs.next()) {

                    coursesInfo = new ApplicantCoursesInfo();

                    varsityInfo = new VarsityInfo();

                    varsityInfo.setVarsityID(rs.getInt("VARSITY_ID"));
                    varsityInfo.setVarsityName(rs.getString("VARSITY_NAME"));
                    varsityInfo.setVarsityStatus(rs.getString("VARSITY_STATUS").charAt(0));
                    coursesInfo.setVarsity(varsityInfo);

                    coursesInfos.add(coursesInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                coursesInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return coursesInfos;
    }

    // Only Varsity
    public List<VarsityInfo> getVarsityInfo(Connection connection, String type) {

        VarsityInfo varsityInfo = null;
        List<VarsityInfo> varsityInfoList = null;

        CountryInfo countryInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectVarsityInfo(type));
                rs = ps.executeQuery();

                varsityInfoList = new ArrayList<VarsityInfo>();

                while (rs.next()) {

                    varsityInfo = new VarsityInfo();

                    countryInfo = new CountryInfo();

                    varsityInfo.setVarsityID(rs.getInt("VARSITY_ID"));
                    varsityInfo.setVarsityName(rs.getString("VARSITY_NAME"));
                    varsityInfo.setVarsityStatus(rs.getString("VARSITY_STATUS").charAt(0));

                    varsityInfoList.add(varsityInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                varsityInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return varsityInfoList;
    }

    public List<VarsityInfo> getVarsityCourseInfo(Connection connection, String type, String varsityID) {

        VarsityInfo varsityInfo = null;
        List<VarsityInfo> varsityCourseInfoList = null;

        CountryInfo countryInfo = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectVarsityCourseInfo(varsityID));
                rs = ps.executeQuery();

                varsityCourseInfoList = new ArrayList<VarsityInfo>();

                while (rs.next()) {

                    varsityInfo = new VarsityInfo();

                    countryInfo = new CountryInfo();

                    varsityInfo.setVarsityID(rs.getInt("VARSITY_ID"));
                    varsityInfo.setVarsityName(rs.getString("VARSITY_NAME"));
                    varsityInfo.setVarsityContact(rs.getString("VARSITY_CONTACT"));
                    varsityInfo.setVarsityEmail(rs.getString("VARSITY_EMAIL"));
                    varsityInfo.setVarsityAddress(rs.getString("VARSITY_ADDRESS"));
                    varsityInfo.setVarsityNotes(rs.getString("VARSITY_NOTES"));
                    varsityInfo.setVarsityStatus(rs.getString("VARSITY_STATUS").charAt(0));

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    varsityInfo.setCountryInfos(countryInfo);

                    if (!varsityID.equalsIgnoreCase("all")) {
                        varsityInfo.setCoursesList(getCourses(connection, type, varsityID));
                    }

                    varsityCourseInfoList.add(varsityInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                varsityCourseInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return varsityCourseInfoList;
    }

    public List<VarsityInfo> searchVarsityCourseInfo(Connection connection, String fName, String fValue) {

        VarsityInfo varsityInfo = null;
        List<VarsityInfo> varsityCourseInfoList = null;

        CountryInfo countryInfo = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchVarsityCourseInfo(fName, fValue));
                rs = ps.executeQuery();

                varsityCourseInfoList = new ArrayList<VarsityInfo>();

                while (rs.next()) {

                    varsityInfo = new VarsityInfo();

                    countryInfo = new CountryInfo();

                    varsityInfo.setVarsityID(rs.getInt("VARSITY_ID"));
                    varsityInfo.setVarsityName(rs.getString("VARSITY_NAME"));
                    varsityInfo.setVarsityContact(rs.getString("VARSITY_CONTACT"));
                    varsityInfo.setVarsityEmail(rs.getString("VARSITY_EMAIL"));
                    varsityInfo.setVarsityAddress(rs.getString("VARSITY_ADDRESS"));
                    varsityInfo.setVarsityNotes(rs.getString("VARSITY_NOTES"));
                    varsityInfo.setVarsityStatus(rs.getString("VARSITY_STATUS").charAt(0));

                    countryInfo.setCountryID(rs.getInt("COUNTRY_ID"));
                    countryInfo.setCountryName(rs.getString("COUNTRY_NAME"));
                    varsityInfo.setCountryInfos(countryInfo);

//                    if (!varsityID.equalsIgnoreCase("all")) {
//                        varsityInfo.setCoursesList(getCourses(connection, type, varsityID));
//                    }
                    varsityCourseInfoList.add(varsityInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                varsityCourseInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return varsityCourseInfoList;
    }

    public List<CoursesInfo> getCourses(Connection connection, String type, String varid) {

        CoursesInfo coursesInfo = null;
        List<CoursesInfo> coursesInfos = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCourses(type, varid));
                rs = ps.executeQuery();

                coursesInfos = new ArrayList<CoursesInfo>();

                while (rs.next()) {

                    coursesInfo = new CoursesInfo();

                    coursesInfo.setCourseID(rs.getInt("COURSE_ID"));
                    coursesInfo.setCourseName(rs.getString("COURSE_NAME"));
                    coursesInfo.setCourseStatus(rs.getString("COURSE_STATUS").charAt(0));

                    coursesInfos.add(coursesInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                coursesInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return coursesInfos;
    }

    public List<ServiceInfo> getServiceInfo(Connection connection, String type) {

        ServiceInfo serviceInfo = null;
        List<ServiceInfo> serviceInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectServInfo(type));
                rs = ps.executeQuery();

                serviceInfoList = new ArrayList<ServiceInfo>();

                while (rs.next()) {

                    serviceInfo = new ServiceInfo();

                    serviceInfo.setServiceID(rs.getInt("SERVICE_ID"));
                    serviceInfo.setServiceName(rs.getString("SERVICE_NAME"));
                    serviceInfo.setServiceAmount(rs.getString("SERVICE_AMOUNT"));
                    serviceInfo.setServiceStatus(rs.getString("SERVICE_STATUS").charAt(0));

                    serviceInfoList.add(serviceInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                serviceInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return serviceInfoList;
    }

    public List<ServiceInfo> searchServiceInfo(Connection connection, String fName, String fValue) {

        ServiceInfo serviceInfo = null;
        List<ServiceInfo> serviceInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchServiceInfo(fName, fValue));
                rs = ps.executeQuery();

                serviceInfoList = new ArrayList<ServiceInfo>();

                while (rs.next()) {

                    serviceInfo = new ServiceInfo();

                    serviceInfo.setServiceID(rs.getInt("SERVICE_ID"));
                    serviceInfo.setServiceName(rs.getString("SERVICE_NAME"));
                    serviceInfo.setServiceAmount(rs.getString("SERVICE_AMOUNT"));
                    serviceInfo.setServiceStatus(rs.getString("SERVICE_STATUS").charAt(0));

                    serviceInfoList.add(serviceInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                serviceInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return serviceInfoList;
    }

    public List<PaymentInfo> getCurrentPayment(Connection connection, String service, String appId) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;
        ServiceInfo serviceInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectCurrentPayment(appId));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();
                    serviceInfo = new ServiceInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    paymentInfo.setPaymentID(rs.getInt("PAYMENT_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));

                    List<PaymentInfo> argee = getAmountAgree(connection, appId, rs.getString("APP_TYPE"));
                    if (service.equals("1")) {
                        paymentInfo.setAmount(rs.getDouble("TUITION_FEE"));
                        paymentInfo.setTuitionAgree(argee.get(0).getTuitionAgree());
                        paymentInfo.setTuitionFeeDue(argee.get(0).getTuitionAgree() - rs.getDouble("TUITION_FEE"));
                    } else if (service.equals("2")) {
                        paymentInfo.setAmount(rs.getDouble("OTHERS_AMOUNT"));
                        paymentInfo.setTuitionAgree(argee.get(0).getOthersAgree());
                        paymentInfo.setTuitionFeeDue(argee.get(0).getOthersAgree() - rs.getDouble("OTHERS_AMOUNT"));
                    } else if (service.equals("3")) {
                        paymentInfo.setAmount(rs.getDouble("WORKER_AMOUNT"));
                        paymentInfo.setTuitionAgree(argee.get(0).getWorkerAgree());
                        paymentInfo.setTuitionFeeDue(argee.get(0).getWorkerAgree() - rs.getDouble("WORKER_AMOUNT"));
                    }

                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);

                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));

                    serviceInfo.setServiceID(rs.getInt("SERVICE_ID"));
                    serviceInfo.setServiceName(rs.getString("SERVICE_NAME"));
                    serviceInfo.setServiceAmount(rs.getString("SERVICE_AMOUNT"));
                    paymentInfo.setServiceInfo(serviceInfo);

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    applicantInfo.setApplicantType(rs.getString("APP_TYPE"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<PaymentInfo> getPaymentHistory(Connection connection, Integer groupid, String userid) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;
        ServiceInfo serviceInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectPaymentHistory(groupid, userid));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();
                    serviceInfo = new ServiceInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    paymentInfo.setPaymentHistoryId(rs.getInt("PAY_HIS_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));
                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));

                    if (rs.getDouble("TUITION_FEE") != 0) {
                        paymentInfo.setAmount(rs.getDouble("TUITION_FEE"));
                    } else if (rs.getDouble("OTHERS_AMOUNT") != 0) {
                        paymentInfo.setAmount(rs.getDouble("OTHERS_AMOUNT"));
                    } else if (rs.getDouble("WORKER_AMOUNT") != 0) {
                        paymentInfo.setAmount(rs.getDouble("WORKER_AMOUNT"));
                    }

//                    paymentInfo.setAmount(rs.getDouble("TUITION_FEE") == 0 ? rs.getDouble("OTHERS_AMOUNT") : rs.getDouble("OTHERS_AMOUNT") == 0 ? rs.getDouble("WORKER_AMOUNT") : rs.getDouble("TUITION_FEE"));
                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);

                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));

                    serviceInfo.setServiceID(rs.getInt("SERVICE_ID"));
                    serviceInfo.setServiceName(rs.getString("SERVICE_NAME"));
                    serviceInfo.setServiceAmount(rs.getString("SERVICE_AMOUNT"));
                    paymentInfo.setServiceInfo(serviceInfo);

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<PaymentInfo> searchPaymentHistory(Connection connection, String fName, String fValue) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;
        ServiceInfo serviceInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchPaymentHistory(fName, fValue));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();
                    serviceInfo = new ServiceInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    paymentInfo.setPaymentHistoryId(rs.getInt("PAY_HIS_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));

                    paymentInfo.setAmount(rs.getDouble("TUITION_FEE") == 0 ? rs.getDouble("OTHERS_AMOUNT") : rs.getDouble("TUITION_FEE"));

                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);

                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));

                    serviceInfo.setServiceID(rs.getInt("SERVICE_ID"));
                    serviceInfo.setServiceName(rs.getString("SERVICE_NAME"));
                    serviceInfo.setServiceAmount(rs.getString("SERVICE_AMOUNT"));
                    paymentInfo.setServiceInfo(serviceInfo);

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<ApplicantInfo> getAppServHisInfo(Connection connection, String type) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfos = null;

        ApplicantServiceInfo applicantServiceInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAppServHistoryInfo(type));
                rs = ps.executeQuery();

                applicantInfos = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    applicantServiceInfo = new ApplicantServiceInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    applicantInfo.setUserInfo(userInfo);

                    applicantServiceInfo.setTutionBasic(rs.getDouble("TUITION_BASIC"));
                    applicantServiceInfo.setTutionAgree(rs.getDouble("TUITION_AGREE"));
                    applicantServiceInfo.setOthersBasic(rs.getDouble("OTHERS_BASIC"));
                    applicantServiceInfo.setOthersAgree(rs.getDouble("OTHERS_AGREE"));
                    applicantServiceInfo.setTuitionStatus(rs.getString("TSTATUS").charAt(0));
                    applicantServiceInfo.setOthersStatus(rs.getString("OSTATUS").charAt(0));
                    applicantInfo.setAsi(applicantServiceInfo);

                    applicantInfos.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfos;
    }

    public List<ApplicantInfo> searchAppServHisInfo(Connection connection, String fName, String fValue) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfos = null;

        ApplicantServiceInfo applicantServiceInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchAppServHistoryInfo(fName, fValue));
                rs = ps.executeQuery();

                applicantInfos = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    applicantServiceInfo = new ApplicantServiceInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    applicantInfo.setUserInfo(userInfo);

                    applicantServiceInfo.setTutionBasic(rs.getDouble("TUITION_BASIC"));
                    applicantServiceInfo.setTutionAgree(rs.getDouble("TUITION_AGREE"));
                    applicantServiceInfo.setOthersBasic(rs.getDouble("OTHERS_BASIC"));
                    applicantServiceInfo.setOthersAgree(rs.getDouble("OTHERS_AGREE"));
                    applicantServiceInfo.setTuitionStatus(rs.getString("TSTATUS").charAt(0));
                    applicantServiceInfo.setOthersStatus(rs.getString("OSTATUS").charAt(0));
                    applicantInfo.setAsi(applicantServiceInfo);

                    applicantInfos.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfos;
    }

    public List<MenuMaster> getMenuParentInfo(Connection connection) {

        MenuMaster menuMaster = null;
        List<MenuMaster> menuMasterInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String menuMasterSelectStatement = " SELECT "
                        + " MENU_ID, "
                        + " MENU_NAME, "
                        + " MENU_TITLE, "
                        + " PARENT_MENU_ID, "
                        + " MENU_URL, "
                        + " MENU_TYPE, "
                        + " MENU_STATUS "
                        + " FROM "
                        + " menu "
                        + " WHERE "
                        + " PARENT_MENU_ID = 0 ";

                ps = connection.prepareStatement(menuMasterSelectStatement);
                rs = ps.executeQuery();

                menuMasterInfoList = new ArrayList<MenuMaster>();

                while (rs.next()) {

                    menuMaster = new MenuMaster();

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    menuMaster.setMasterMenuName(rs.getString("MENU_NAME"));
                    menuMaster.setMasterMenuTitle(rs.getString("MENU_TITLE"));
                    menuMaster.setMasterParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    menuMaster.setMasterMenuUrl(rs.getString("MENU_URL"));
                    menuMaster.setMasterMenuType(rs.getString("MENU_TYPE"));
                    menuMaster.setMasterMenuStatus(rs.getString("MENU_STATUS").charAt(0));

                    menuMaster.setMenuChildList(getMenuChildInfo(connection, rs.getInt("MENU_ID")));

                    menuMasterInfoList.add(menuMaster);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuMasterInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuMasterInfoList;
    }

    public List<MenuChild> getMenuChildInfo(Connection connection, Integer masterMenuID) {

        MenuChild menuChild = null;
        List<MenuChild> menuChildInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String menuChildSelectStatement = " SELECT "
                        + " MENU_ID, "
                        + " MENU_NAME, "
                        + " MENU_TITLE, "
                        + " PARENT_MENU_ID, "
                        + " MENU_URL, "
                        + " MENU_TYPE, "
                        + " MENU_STATUS "
                        + " FROM "
                        + " menu "
                        + " WHERE "
                        + " PARENT_MENU_ID = '" + masterMenuID + "' ";

                ps = connection.prepareStatement(menuChildSelectStatement);
                rs = ps.executeQuery();

                menuChildInfoList = new ArrayList<MenuChild>();

                while (rs.next()) {

                    menuChild = new MenuChild();

                    menuChild.setChildMenuID(rs.getInt("MENU_ID"));
                    menuChild.setChildMenuName(rs.getString("MENU_NAME"));
                    menuChild.setChildMenuTitle(rs.getString("MENU_TITLE"));
                    menuChild.setChildParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    menuChild.setChildMenuUrl(rs.getString("MENU_URL"));
                    menuChild.setChildMenuType(rs.getString("MENU_TYPE"));
                    menuChild.setChildMenuStatus(rs.getString("MENU_STATUS").charAt(0));

                    menuChildInfoList.add(menuChild);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuChildInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuChildInfoList;
    }

    public List<UserGroup> getUserGroup(Connection connection) {

        UserGroup userGroup = null;
        List<UserGroup> userGroupInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String userGroupSelectStatement = " SELECT "
                        + " GROUP_ID, "
                        + " GROUP_NAME, "
                        + " GROUP_TYPE, "
                        + " GROUP_STATUS "
                        + " FROM "
                        + " user_group ";

                ps = connection.prepareStatement(userGroupSelectStatement);
                rs = ps.executeQuery();

                userGroupInfoList = new ArrayList<UserGroup>();

                while (rs.next()) {

                    userGroup = new UserGroup();

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userGroup.setGroupType(rs.getString("GROUP_TYPE"));
                    userGroup.setGroupStatus(rs.getString("GROUP_STATUS").charAt(0));

                    userGroupInfoList.add(userGroup);
                }
            } catch (Exception e) {
                System.out.println(e);
                userGroupInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userGroupInfoList;
    }

    public List<UserLevelMenu> getMenuByGroup(Connection connection, String groupID) {

        UserLevelMenu ulMenuInfo = null;
        List<UserLevelMenu> userLevelMenuInfoList = null;

        MenuMaster menuMaster = null;
        UserGroup userGroup = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String userLevelMenuSelectStatement = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " ULM.LEVEL_MENU_STATUS, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " MN.PARENT_MENU_ID, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_group UG "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UG.GROUP_ID = '" + groupID + "' "
                        + " AND "
                        + " MN.PARENT_MENU_ID = 0 ";

                ps = connection.prepareStatement(userLevelMenuSelectStatement);
                rs = ps.executeQuery();

                userLevelMenuInfoList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    ulMenuInfo = new UserLevelMenu();

                    menuMaster = new MenuMaster();
                    userGroup = new UserGroup();

                    ulMenuInfo.setUserLevelMenuID(rs.getInt("USER_LEVEL_MENU_ID"));
                    ulMenuInfo.setLevelMenuStatus(rs.getString("LEVEL_MENU_STATUS").charAt(0));

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    menuMaster.setMasterMenuName(rs.getString("MENU_NAME"));
                    menuMaster.setMasterParentMenuID(rs.getInt("PARENT_MENU_ID"));
                    ulMenuInfo.setMenuMaster(menuMaster);

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    ulMenuInfo.setUserGroupInfo(userGroup);

                    ulMenuInfo.setUserLevelMenuChildList(getMenuChildByGroup(connection, rs.getInt("GROUP_ID"), rs.getInt("MENU_ID")));

                    userLevelMenuInfoList.add(ulMenuInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                userLevelMenuInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userLevelMenuInfoList;
    }

    public List<MenuChild> getMenuChildByGroup(Connection connection, Integer groupID, Integer pmID) {

        MenuChild menuChild = null;
        List<MenuChild> menuChildInfoList = null;

        UserGroup userGroup = null;
        UserLevelMenu userLevelMenuInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String userLevelChildMenuSelectStatement = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " ULM.LEVEL_MENU_STATUS, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " MN.PARENT_MENU_ID, "
                        + " UG.GROUP_ID, "
                        + " UG.GROUP_NAME "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_group UG "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UG.GROUP_ID "
                        + " AND "
                        + " UG.GROUP_ID = '" + groupID + "' "
                        + " AND "
                        + " MN.PARENT_MENU_ID = '" + pmID + "' ";

                ps = connection.prepareStatement(userLevelChildMenuSelectStatement);
                rs = ps.executeQuery();

                menuChildInfoList = new ArrayList<MenuChild>();

                while (rs.next()) {

                    menuChild = new MenuChild();

                    userGroup = new UserGroup();
                    userLevelMenuInfo = new UserLevelMenu();

                    menuChild.setChildMenuID(rs.getInt("MENU_ID"));
                    menuChild.setChildMenuName(rs.getString("MENU_NAME"));
                    menuChild.setChildParentMenuID(rs.getInt("PARENT_MENU_ID"));

                    userGroup.setUserGroupID(rs.getInt("GROUP_ID"));
                    userGroup.setGroupName(rs.getString("GROUP_NAME"));
                    userLevelMenuInfo.setUserGroupInfo(userGroup);

                    userLevelMenuInfo.setUserLevelMenuID(rs.getInt("USER_LEVEL_MENU_ID"));
                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("LEVEL_MENU_STATUS").charAt(0));
                    menuChild.setUserLevelMenu(userLevelMenuInfo);

                    menuChildInfoList.add(menuChild);
                }
            } catch (Exception e) {
                System.out.println(e);
                menuChildInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return menuChildInfoList;
    }

    public List<UserLevelMenu> getManus(Connection connection, String userId, Integer groupId) {

        UserLevelMenu userLevelMenuInfo = null;
        List<UserLevelMenu> userLevelMenusList = null;

        MenuMaster menuMaster = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                String selectMenu = " SELECT "
                        + " ULM.USER_LEVEL_MENU_ID, "
                        + " MN.MENU_ID, "
                        + " MN.MENU_NAME, "
                        + " UI.USER_ID, "
                        + " UI.USER_NAME, "
                        + " CASE "
                        + " WHEN "
                        + " ULM.LEVEL_MENU_STATUS = 'Y' "
                        + " THEN 'T' ELSE 'F' "
                        + " END "
                        + " LEVEL_MENU_STATUS "
                        + " FROM "
                        + " user_level_menu ULM, "
                        + " menu MN, "
                        + " user_info UI "
                        + " WHERE "
                        + " ULM.MENU_ID = MN.MENU_ID "
                        + " AND "
                        + " ULM.GROUP_ID = UI.GROUP_ID "
                        + " AND "
                        + " ULM.GROUP_ID = '" + groupId + "' "
                        + " AND "
                        + " UI.USER_ID = '" + userId + "' ";

                ps = connection.prepareStatement(selectMenu);
                rs = ps.executeQuery();

                userLevelMenusList = new ArrayList<UserLevelMenu>();

                while (rs.next()) {

                    userLevelMenuInfo = new UserLevelMenu();

                    menuMaster = new MenuMaster();
                    userInfo = new UserInfo();

                    userLevelMenuInfo.setLevelMenuStatus(rs.getString("LEVEL_MENU_STATUS").charAt(0));

                    menuMaster.setMasterMenuID(rs.getInt("MENU_ID"));
                    userLevelMenuInfo.setMenuMaster(menuMaster);

                    userLevelMenusList.add(userLevelMenuInfo);
                }
            } catch (Exception e) {
                userLevelMenusList = null;
                System.out.println(e);
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return userLevelMenusList;
    }

    public List<PaymentInfo> getPayDueHistory(Connection connection, Integer groupid, String userid, String appid) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectDuePaymentHistory(groupid, userid, appid));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
//
                    paymentInfo.setPaymentID(rs.getInt("PAYMENT_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));
                    paymentInfo.setTuitionFee(rs.getDouble("TUITION_FEE"));
                    paymentInfo.setOthersAmount(rs.getDouble("OTHERS_AMOUNT"));
                    paymentInfo.setWorkerAmount(rs.getDouble("WORKER_AMOUNT"));
                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);
                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
//                    
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);
//                    
                    List<PaymentInfo> due = getAmountAgree(connection, rs.getString("APPLICANT_ID"), rs.getString("APP_TYPE"));

                    if (rs.getString("APP_TYPE").equals("1")) {
                        paymentInfo.setTuitionAgree(due.get(0).getTuitionAgree());
                        paymentInfo.setOthersAgree(due.get(0).getOthersAgree());

                        paymentInfo.setTuitionFeeDue(due.get(0).getTuitionAgree() - rs.getDouble("TUITION_FEE"));
                        paymentInfo.setOthersAmountDue(due.get(0).getOthersAgree() - rs.getDouble("OTHERS_AMOUNT"));
                    } else if (rs.getString("APP_TYPE").equals("2")) {
                        paymentInfo.setWorkerAgree(due.get(0).getWorkerAgree());

                        paymentInfo.setWorkerAmountDue(due.get(0).getWorkerAgree() - rs.getDouble("WORKER_AMOUNT"));
                    }

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return paymentInfoList;
    }

    public List<PaymentInfo> searchDuePayHistory(Connection connection, String fName, String fValue) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectSearchDuePaymentHistory(fName, fValue));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    paymentInfo.setPaymentID(rs.getInt("PAYMENT_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));
                    paymentInfo.setTuitionFee(rs.getDouble("TUITION_FEE"));
                    paymentInfo.setOthersAmount(rs.getDouble("OTHERS_AMOUNT"));
                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);
                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
//                    
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);

                    List<PaymentInfo> due = getAmountAgree(connection, rs.getString("APPLICANT_ID"), rs.getString("APP_TYPE"));

                    paymentInfo.setTuitionAgree(due.get(0).getTuitionAgree());
                    paymentInfo.setOthersAgree(due.get(0).getOthersAgree());

                    paymentInfo.setTuitionFeeDue(due.get(0).getTuitionAgree() - rs.getDouble("TUITION_FEE"));
                    paymentInfo.setOthersAmountDue(due.get(0).getOthersAgree() - rs.getDouble("OTHERS_AMOUNT"));

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<PaymentInfo> getAmountAgree(Connection connection, String appid, String apptype) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectDue(appid, apptype));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    if (apptype.equals("1")) {
                        paymentInfo.setTuitionAgree(rs.getDouble("TUITION_AGREE"));
                        paymentInfo.setOthersAgree(rs.getDouble("OTHERS_AGREE"));
                    } else if (apptype.equals("2")) {
                        paymentInfo.setWorkerAgree(rs.getDouble("WORKER_AGREE"));
                    }
                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public List<IssuesInfo> getIssuesInfo(Connection connection, String appid, String agid) {

        IssuesInfo issuesInfo = null;
        List<IssuesInfo> issuesInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectIssuesInfo(appid, agid));
                rs = ps.executeQuery();

                issuesInfoList = new ArrayList<IssuesInfo>();

                while (rs.next()) {

                    issuesInfo = new IssuesInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    issuesInfo.setIssuesId(rs.getInt("ISSUE_ID"));
                    issuesInfo.setIssuesTitle(rs.getString("ISSUE_TITLE"));
                    issuesInfo.setIssuesDetails(rs.getString("ISSUE_DETAILS"));
                    issuesInfo.setCommentDetails(rs.getString("COMMENT_DETAILS"));
                    issuesInfo.setIssuesStatus(rs.getString("ISSUE_STATUS").charAt(0));
                    issuesInfo.setCommentStatus(rs.getString("COMMENT_STATUS").charAt(0));
                    issuesInfo.setIssuesInsertBy(rs.getString("ISSUE_INSERT_BY"));
                    if (rs.getTimestamp("ISSUE_INSERT_DATE") != null) {
                        issuesInfo.setIssuesInsertDate(formatter.format(rs.getTimestamp("ISSUE_INSERT_DATE")));
                    }
                    issuesInfo.setCommentBy(rs.getString("COMMENT_BY"));
                    if (rs.getTimestamp("COMMENT_DATE") != null) {
                        issuesInfo.setCommentDate(formatter.format(rs.getTimestamp("COMMENT_DATE")));
                    }
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    issuesInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    issuesInfo.setUserInfo(userInfo);

                    issuesInfoList.add(issuesInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                issuesInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return issuesInfoList;
    }

    public List<ApplicantInfo> getAppService(Connection connection, String appid) {

        ApplicantInfo applicantInfo = null;
        List<ApplicantInfo> applicantInfos = null;

        ServiceInfo si = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectAppService(appid));
                rs = ps.executeQuery();

                applicantInfos = new ArrayList<ApplicantInfo>();

                while (rs.next()) {

                    applicantInfo = new ApplicantInfo();

                    si = new ServiceInfo();

                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));

                    si.setServiceID(rs.getInt("SERVICE_ID"));
                    si.setServiceName(rs.getString("SERVICE_NAME"));
                    applicantInfo.setServiceInfo(si);

                    applicantInfos.add(applicantInfo);
                }
            } catch (Exception e) {
                System.out.println(e);
                applicantInfos = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return applicantInfos;
    }

    public List<PaymentInfo> getPaymentDueByApplicant(Connection connection, String servid, String appid) {

        PaymentInfo paymentInfo = null;
        List<PaymentInfo> paymentInfoList = null;

        ApplicantInfo applicantInfo = null;
        UserInfo userInfo = null;

        PreparedStatement ps = null;
        ResultSet rs = null;

        if (connection != null) {

            try {

                ps = connection.prepareStatement(QueryStatement.selectDuePaymentByApplicant(servid, appid));
                rs = ps.executeQuery();

                paymentInfoList = new ArrayList<PaymentInfo>();

                while (rs.next()) {

                    paymentInfo = new PaymentInfo();

                    applicantInfo = new ApplicantInfo();
                    userInfo = new UserInfo();

                    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");

                    paymentInfo.setPaymentID(rs.getInt("PAYMENT_ID"));
                    paymentInfo.setPaymentMode(rs.getString("PAYMENT_MODE"));
                    paymentInfo.setTuitionFee(rs.getDouble("TUITION_FEE"));
                    paymentInfo.setOthersAmount(rs.getDouble("OTHERS_AMOUNT"));
                    Date payDate = rs.getDate("PAYMENT_DATE");
                    String pDate = formatter.format(payDate);
                    paymentInfo.setPaymentDate(pDate);
                    paymentInfo.setConversionRate(rs.getString("CONVERSION_RATE"));
                    paymentInfo.setBankName(rs.getString("BANK_NAME"));
                    paymentInfo.setChequeReference(rs.getString("CHEQUE_REFERENCE"));

//                    Date chqDate = rs.getDate("CHEQUE_DATE");
//                    if (chqDate != null) {
//                        String cDate = formatter.format(chqDate);
//                        paymentInfo.setChequeDate(cDate);
//                    } else {
//                        paymentInfo.setChequeDate("");
//                    }
//                    
                    paymentInfo.setCurrency(rs.getString("CURRENCY"));
                    applicantInfo.setApplicantID(rs.getString("APPLICANT_ID"));
                    applicantInfo.setApplicantName(rs.getString("APPLICANT_NAME"));
                    paymentInfo.setApplicantInfo(applicantInfo);

                    userInfo.setUserID(rs.getString("USER_ID"));
                    userInfo.setUserFullName(rs.getString("USER_FULL_NAME"));
                    paymentInfo.setUserInfo(userInfo);

                    List<PaymentInfo> due = getAmountAgree(connection, rs.getString("APPLICANT_ID"), "");

                    paymentInfo.setTuitionAgree(due.get(0).getTuitionAgree());
                    paymentInfo.setOthersAgree(due.get(0).getOthersAgree());

                    paymentInfo.setTuitionFeeDue(due.get(0).getTuitionAgree() - rs.getDouble("TUITION_FEE"));
                    paymentInfo.setOthersAmountDue(due.get(0).getOthersAgree() - rs.getDouble("OTHERS_AMOUNT"));

                    paymentInfoList.add(paymentInfo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                paymentInfoList = null;
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }

        return paymentInfoList;
    }

    public static void main(String[] args) {
        Connection c = DBConnection.getMySqlConnection();
        CommonList com = new CommonList();
        System.out.println(com.getPayDueHistory(c, 0, "", "10000001").get(0).getOthersAmountDue());
    }
}
