package com.common;

public class QueryStatement {

    public static String selectLoginInfo(String userName, String password) {

        String selectLoginStatement = " SELECT "
                + " UI.USER_ID, "
                + " UI.USER_FULL_NAME, "
                + " UI.USER_NAME, "
                + " UI.USER_PASSWORD, "
                + " UI.USER_PHONE, "
                + " UG.GROUP_ID, "
                + " UG.GROUP_NAME, "
                + " UG.GROUP_TYPE "
                + " FROM "
                + " user_info UI, "
                + " user_group UG "
                + " WHERE "
                + " UI.GROUP_ID = UG.GROUP_ID "
                + " AND "
                + " UI.USER_NAME = '" + userName + "' "
                + " AND "
                + " UI.USER_PASSWORD = '" + password + "' "
                + " AND "
                + " UI.USER_STATUS = 'Y' ";

        return selectLoginStatement;
    }

    public static String selectUserInfo(String type) {

        String selectAgentStatement = "";

        if (type.equalsIgnoreCase("all")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID ";
        } else if (type.equalsIgnoreCase("active")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_STATUS = 'Y' ";
        } else if (type.equalsIgnoreCase("aduser")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.GROUP_ID = 1 ";
        } else if (type.equalsIgnoreCase("agent")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.GROUP_ID = 2 ";
        } else {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_ID = '" + type + "' ";
        }

        return selectAgentStatement;
    }

    public static String selectSearchUserInfo(String fName, String fValue) {

        String selectAgentStatement = "";

        if (fName.equals("userFulName")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        } else if (fName.equals("userPhone")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.USER_PHONE = '" + fValue + "' ";
        } else if (fName.equals("userCompany")) {
            selectAgentStatement = " SELECT "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_NAME, "
                    + " UI.USER_PHONE, "
                    + " UI.USER_FAX, "
                    + " UI.USER_EMAIL, "
                    + " UI.USER_DOB, "
                    + " UI.USER_PASSWORD, "
                    + " UI.COMPANY_ID, "
                    + " UI.USER_STATUS, "
                    + " UI.USER_NOTES, "
                    + " UI.USER_ADDRESS, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME, "
                    + " UG.GROUP_ID, "
                    + " UG.GROUP_NAME, "
                    + " UG.GROUP_TYPE "
                    + " FROM "
                    + " user_info UI, "
                    + " country_info COUN, "
                    + " user_group UG "
                    + " WHERE "
                    + " UI.COUNTRY_ID = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.GROUP_ID = UG.GROUP_ID "
                    + " AND "
                    + " UI.COMPANY_ID = '" + fValue + "' ";
        }

        return selectAgentStatement;
    }

    public static String selectCountryInfo() {

        String selectCountryStatement = " SELECT "
                + " CI.COUNTRY_ID, "
                + " CI.COUNTRY_NAME, "
                + " CI.COUNTRY_STATUS "
                + " FROM "
                + " country_info CI ";

        return selectCountryStatement;
    }

    public static String selectApplicantInfo(Integer gID, String uID, String appType) {

        String selectApplicantStatement = "";
        if (gID == 2) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.APP_TYPE, "
                    + " APP.SECTOR, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " APP.APP_TYPE = '" + appType + "' "
                    + " AND "
                    + " UI.USER_ID = '" + uID + "' ";
        } else {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.APP_TYPE, "
                    + " APP.SECTOR, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " APP.APP_TYPE = '" + appType + "' ";
        }

        return selectApplicantStatement;
    }

    public static String selectApplicantByAgent(String uid) {

        String selectApplicantStatement = " SELECT "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " APP.APP_TYPE, "
                + " UI.USER_FULL_NAME "
                + " FROM "
                + " applicant_info APP, "
                + " user_info UI "
                + " WHERE "
                + " APP.USER_ID = UI.USER_ID "
                + " AND "
                + " UI.USER_ID = '" + uid + "' ";

        return selectApplicantStatement;
    }

    public static String selectSearchApplicantInfo(String fName, String fValue) {

        String selectApplicantStatement = "";
        if (fName.equals("applicantId")) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = '" + fValue + "' ";
        } else if (fName.equals("applicantName")) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " APP.APPLICANT_NAME = '" + fValue + "' ";
        } else if (fName.equals("applicantPhone")) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " APP.APP_CONTACT = '" + fValue + "' ";
        } else if (fName.equals("applicantCountry")) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " COUN.COUNTRY_NAME = '" + fValue + "' ";
        } else if (fName.equals("agentName")) {
            selectApplicantStatement = " SELECT "
                    + " APP.APPLICANT_TABLE_ID, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_NATIONALITY, "
                    + " APP.APP_PASSPORT_NO, "
                    + " APP.APP_ALT_PASSPORT_NO, "
                    + " APP.APP_DOB, "
                    + " APP.APP_CONTACT, "
                    + " APP.PASS_ISSUED_PLACE, "
                    + " APP.PASS_ISSUED_DATE, "
                    + " APP.APP_EMAIL, "
                    + " APP.APP_COUNTRY, "
                    + " APP.APP_FATHER, "
                    + " APP.APP_MOTHER, "
                    + " APP.VISA_TYPE, "
                    + " APP.VISA_COLLEGE, "
                    + " APP.VISA_EX_DATE, "
                    + " UI.USER_FULL_NAME, "
                    + " UI.USER_PHONE, "
                    + " COUN.COUNTRY_ID, "
                    + " COUN.COUNTRY_NAME "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " country_info COUN "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        }

        return selectApplicantStatement;
    }

    public static String selectSingleApplicantInfo(String appID) {

        String selectApplicantStatement = " SELECT "
                + " APP.APPLICANT_TABLE_ID, "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " APP.APP_NATIONALITY, "
                + " APP.APP_PASSPORT_NO, "
                + " APP.APP_ALT_PASSPORT_NO, "
                + " APP.APP_DOB, "
                + " APP.PASS_ISSUED_PLACE, "
                + " APP.PASS_ISSUED_DATE, "
                + " APP.PASS_EXPIRY_DATE, "
                + " APP.APP_GENDER, "
                + " APP.APP_EMAIL, "
                + " APP.APP_ALT_EMAIL, "
                + " APP.APP_MARITAL_STATUS, "
                + " APP.APP_CONTACT, "
                + " APP.APP_CITY, "
                + " APP.APP_POSTAL_CODE, "
                + " APP.APP_STATE, "
                + " APP.APP_RESD_NO, "
                + " APP.APP_PERM_ADDR, "
                + " APP.COR_CONTACT, "
                + " APP.COR_CITY, "
                + " APP.COR_POSTAL_CODE, "
                + " APP.COR_SATE, "
                + " APP.COR_RESD_NO, "
                + " APP.COR_ADDRESS, "
                + " APP.APP_FATHER, "
                + " APP.FATHER_OCCUPATION, "
                + " APP.APP_MOTHER, "
                + " APP.MOTHER_OCCUPATION, "
                + " APP.HIGHT_QUALI, "
                + " APP.PASS_YEAR, "
                + " APP.OVER_ALL_GRADE, "
                + " APP.VISA_COUNTRY, "
                + " APP.VISA_EX_DATE, "
                + " APP.VISA_LETTER_ISSUE, "
                + " APP.VISA_COLLEGE, "
                + " APP.VISA_TYPE, "
                + " APP.ARRIVAL_DATE, "
                + " UI.USER_ID, "
                + " UI.USER_FULL_NAME, "
                + " UI.USER_PHONE, "
                + " COUN.COUNTRY_ID, "
                + " COUN.COUNTRY_NAME "
                + " FROM "
                + " applicant_info APP, "
                + " user_info UI, "
                + " country_info COUN "
                + " WHERE "
                + " APP.USER_ID = UI.USER_ID "
                + " AND "
                + " APP.APP_COUNTRY = COUN.COUNTRY_ID "
                + " AND "
                + " APP.APPLICANT_ID = '" + appID + "' ";

        return selectApplicantStatement;
    }

    public static String selectEducationInfo(String idApp) {

        String selectVarsityCoursesStatement = " SELECT "
                + " EDU.APP_EDU_ID, "
                + " EDU.DEGREE_NAME, "
                + " EDU.RESULT, "
                + " EDU.PASSING_YEAR, "
                + " EDU.INSTITUTION "
                + " FROM "
                + " applicant_education EDU "
                + " WHERE "
                + " EDU.APPLICANT_ID = '" + idApp + "' ";

        return selectVarsityCoursesStatement;
    }

    // App varsity
    public static String selectAppVarsity(String idApp) {

        String selectVarsityCoursesStatement = "";
        if (idApp.equalsIgnoreCase("all")) {
            selectVarsityCoursesStatement = " SELECT "
                    + " AC.APP_COURSE_ID, "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_NAME, "
                    + " VAR.VARSITY_STATUS "
                    + " FROM "
                    + " applicant_courses AC, "
                    + " varsity_info VAR "
                    + " WHERE "
                    + " AC.VARCITY_ID = VAR.VARSITY_ID "
                    + " GROUP BY "
                    + " VAR.VARSITY_ID ";
        } else {
            selectVarsityCoursesStatement = " SELECT "
                    + " AC.APP_COURSE_ID, "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_NAME, "
                    + " VAR.VARSITY_STATUS "
                    + " FROM "
                    + " applicant_courses AC, "
                    + " varsity_info VAR "
                    + " WHERE "
                    + " AC.VARCITY_ID = VAR.VARSITY_ID "
                    + " AND "
                    + " AC.APPLICANT_ID = '" + idApp + "' "
                    + " GROUP BY "
                    + " VAR.VARSITY_ID ";
        }

        return selectVarsityCoursesStatement;
    }

    // App courses
    public static String selectVarCourseInfo(String idApp) {

        String selectVarsityCoursesStatement = "";
        if (idApp.equalsIgnoreCase("all")) {
            selectVarsityCoursesStatement = " SELECT "
                    + " AC.APP_COURSE_ID, "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_NAME, "
                    + " VAR.VARSITY_STATUS, "
                    + " VAR.COURSE_ID, "
                    + " VAR.COURSE_NAME, "
                    + " VAR.COURSE_STATUS "
                    + " FROM "
                    + " applicant_courses AC, "
                    + " varsity_info VAR "
                    + " WHERE "
                    + " AC.VARCITY_ID = VAR.VARSITY_ID "
                    + " AND "
                    + " AC.COURSE_ID = VAR.COURSE_ID "
                    + " GROUP BY "
                    + " VAR.COURSE_ID ";
        } else {
            selectVarsityCoursesStatement = " SELECT "
                    + " AC.APP_COURSE_ID, "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_NAME, "
                    + " VAR.VARSITY_STATUS, "
                    + " VAR.COURSE_ID, "
                    + " VAR.COURSE_NAME, "
                    + " VAR.COURSE_STATUS "
                    + " FROM "
                    + " applicant_courses AC, "
                    + " varsity_info VAR "
                    + " WHERE "
                    + " AC.VARCITY_ID = VAR.VARSITY_ID "
                    + " AND "
                    + " AC.COURSE_ID = VAR.COURSE_ID "
                    + " AND "
                    + " AC.APPLICANT_ID = '" + idApp + "' "
                    + " GROUP BY "
                    + " VAR.COURSE_ID ";
        }

        return selectVarsityCoursesStatement;
    }

    public static String selectVarsityInfo(String type) {
        if (type.equalsIgnoreCase("active")) {
            return " SELECT DISTINCT(VARSITY_NAME), VARSITY_ID, VARSITY_STATUS FROM varsity_info WHERE VARSITY_STATUS = 'Y' ";
        } else {
            return " SELECT DISTINCT(VARSITY_NAME), VARSITY_ID, VARSITY_STATUS FROM varsity_info ";
        }
    }

    public static String selectVarsityCourseInfo(String varID) {

        String selectVarsityStatement = "";

        if (varID.equalsIgnoreCase("all")) {
            selectVarsityStatement = " SELECT "
                    + " DISTINCT(VAR.VARSITY_NAME), "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_CONTACT, "
                    + " VAR.VARSITY_EMAIL, "
                    + " VAR.VARSITY_ADDRESS, "
                    + " VAR.VARSITY_NOTES, "
                    + " VAR.VARSITY_STATUS, "
                    + " CNT.COUNTRY_ID, "
                    + " CNT.COUNTRY_NAME "
                    + " FROM "
                    + " varsity_info VAR, "
                    + " country_info CNT "
                    + " WHERE "
                    + " VAR.VARSITY_COUNTRY = CNT.COUNTRY_ID ";
        } else {
            selectVarsityStatement = " SELECT "
                    + " DISTINCT(VAR.VARSITY_NAME), "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_CONTACT, "
                    + " VAR.VARSITY_EMAIL, "
                    + " VAR.VARSITY_ADDRESS, "
                    + " VAR.VARSITY_NOTES, "
                    + " VAR.VARSITY_STATUS, "
                    + " CNT.COUNTRY_ID, "
                    + " CNT.COUNTRY_NAME "
                    + " FROM "
                    + " varsity_info VAR, "
                    + " country_info CNT "
                    + " WHERE "
                    + " VAR.VARSITY_COUNTRY = CNT.COUNTRY_ID "
                    + " AND "
                    + " VAR.VARSITY_ID = '" + varID + "' ";

        }

        return selectVarsityStatement;
    }

    public static String selectSearchVarsityCourseInfo(String fName, String fValue) {

        String selectVarsityStatement = "";

        if (fName.equals("varName")) {
            selectVarsityStatement = " SELECT "
                    + " DISTINCT(VAR.VARSITY_NAME), "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_CONTACT, "
                    + " VAR.VARSITY_EMAIL, "
                    + " VAR.VARSITY_ADDRESS, "
                    + " VAR.VARSITY_NOTES, "
                    + " VAR.VARSITY_STATUS, "
                    + " CNT.COUNTRY_ID, "
                    + " CNT.COUNTRY_NAME "
                    + " FROM "
                    + " varsity_info VAR, "
                    + " country_info CNT "
                    + " WHERE "
                    + " VAR.VARSITY_COUNTRY = CNT.COUNTRY_ID "
                    + " AND "
                    + " VAR.VARSITY_NAME = '" + fValue + "' ";
        } else if (fName.equals("varCountry")) {
            selectVarsityStatement = " SELECT "
                    + " DISTINCT(VAR.VARSITY_NAME), "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_CONTACT, "
                    + " VAR.VARSITY_EMAIL, "
                    + " VAR.VARSITY_ADDRESS, "
                    + " VAR.VARSITY_NOTES, "
                    + " VAR.VARSITY_STATUS, "
                    + " CNT.COUNTRY_ID, "
                    + " CNT.COUNTRY_NAME "
                    + " FROM "
                    + " varsity_info VAR, "
                    + " country_info CNT "
                    + " WHERE "
                    + " VAR.VARSITY_COUNTRY = CNT.COUNTRY_ID "
                    + " AND "
                    + " CNT.COUNTRY_NAME = '" + fValue + "' ";

        } else if (fName.equals("varStatus")) {
            selectVarsityStatement = " SELECT "
                    + " DISTINCT(VAR.VARSITY_NAME), "
                    + " VAR.VARSITY_ID, "
                    + " VAR.VARSITY_CONTACT, "
                    + " VAR.VARSITY_EMAIL, "
                    + " VAR.VARSITY_ADDRESS, "
                    + " VAR.VARSITY_NOTES, "
                    + " VAR.VARSITY_STATUS, "
                    + " CNT.COUNTRY_ID, "
                    + " CNT.COUNTRY_NAME "
                    + " FROM "
                    + " varsity_info VAR, "
                    + " country_info CNT "
                    + " WHERE "
                    + " VAR.VARSITY_COUNTRY = CNT.COUNTRY_ID "
                    + " AND "
                    + " VAR.VARSITY_STATUS = '" + fValue + "' ";

        }

        return selectVarsityStatement;
    }

    public static String selectCourses(String type, String varid) {

        String selectVarsityStatement = "";

        if (type.equalsIgnoreCase("all")) {

            selectVarsityStatement = " SELECT "
                    + " COURSE_ID, "
                    + " COURSE_NAME, "
                    + " COURSE_STATUS "
                    + " FROM "
                    + " varsity_info "
                    + " WHERE "
                    + " VARSITY_ID = '" + varid + "' ";
        } else {
            selectVarsityStatement = " SELECT "
                    + " COURSE_ID, "
                    + " COURSE_NAME, "
                    + " COURSE_STATUS "
                    + " FROM "
                    + " varsity_info "
                    + " WHERE "
                    + " VARSITY_ID = '" + varid + "' "
                    + " AND "
                    + " COURSE_STATUS = 'Y' ";
        }

        return selectVarsityStatement;
    }

    public static String selectServInfo(String type) {

        String selectVarsityStatement = "";
        if (type.equalsIgnoreCase("all")) {
            selectVarsityStatement = " SELECT "
                    + " SERVICE_ID, "
                    + " SERVICE_NAME, "
                    + " SERVICE_AMOUNT, "
                    + " SERVICE_STATUS "
                    + " FROM "
                    + " service_info ";
        } else if (type.equalsIgnoreCase("active")) {
            selectVarsityStatement = " SELECT "
                    + " SERVICE_ID, "
                    + " SERVICE_NAME, "
                    + " SERVICE_AMOUNT, "
                    + " SERVICE_STATUS "
                    + " FROM "
                    + " service_info "
                    + " WHERE "
                    + " SERVICE_STATUS = 'Y' ";
        } else {
            selectVarsityStatement = " SELECT "
                    + " SERVICE_ID, "
                    + " SERVICE_NAME, "
                    + " SERVICE_AMOUNT, "
                    + " SERVICE_STATUS "
                    + " FROM "
                    + " service_info "
                    + " WHERE "
                    + " SERVICE_ID = '" + type + "' ";
        }

        return selectVarsityStatement;
    }

    public static String selectSearchServiceInfo(String fName, String fValue) {

        String selectVarsityStatement = "";
        if (fName.equals("serviceName")) {
            selectVarsityStatement = " SELECT "
                    + " SERVICE_ID, "
                    + " SERVICE_NAME, "
                    + " SERVICE_AMOUNT, "
                    + " SERVICE_STATUS "
                    + " FROM "
                    + " service_info "
                    + " WHERE "
                    + " SERVICE_NAME = '" + fValue + "' ";
        } else if (fName.equals("serviceStatus")) {
            selectVarsityStatement = " SELECT "
                    + " SERVICE_ID, "
                    + " SERVICE_NAME, "
                    + " SERVICE_AMOUNT, "
                    + " SERVICE_STATUS "
                    + " FROM "
                    + " service_info "
                    + " WHERE "
                    + " SERVICE_STATUS = '" + fValue + "' ";
        }

        return selectVarsityStatement;
    }

    public static String selectCurrentPayment(String appid) {

        String selectPaymentStatement = "";

        selectPaymentStatement = " SELECT "
                + " PAY.PAYMENT_ID, "
                + " PAY.PAYMENT_MODE, "
                + " PAY.PAYMENT_DATE, "
                + " PAY.TUITION_FEE, "
                + " PAY.OTHERS_AMOUNT, "
                + " PAY.WORKER_AMOUNT, "
                + " PAY.CONVERSION_RATE, "
                + " PAY.BANK_NAME, "
                + " PAY.CHEQUE_REFERENCE, "
                + " PAY.CHEQUE_DATE, "
                + " PAY.CURRENCY, "
                + " PAY.STATUS, "
                + " SERV.SERVICE_ID, "
                + " SERV.SERVICE_NAME, "
                + " SERV.SERVICE_AMOUNT, "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " APP.APP_TYPE, "
                + " UI.USER_ID, "
                + " UI.USER_FULL_NAME "
                + " FROM "
                + " payment_info PAY, "
                + " service_info SERV, "
                + " applicant_info APP, "
                + " user_info UI "
                + " WHERE "
                + " PAY.SERVICE_ID = SERV.SERVICE_ID "
                + " AND "
                + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                + " AND "
                + " PAY.USER_ID = UI.USER_ID "
                + " AND "
                + " PAY.APPLICANT_ID = '" + appid + "' ";

        return selectPaymentStatement;
    }

    public static String selectPaymentHistory(Integer gid, String uid) {

        String selectPaymentStatement = "";
        if (gid == 2) {
            selectPaymentStatement = " SELECT "
                    + " PH.PAY_HIS_ID, "
                    + " PH.PAYMENT_MODE, "
                    + " PH.PAYMENT_DATE, "
                    + " PH.TUITION_FEE, "
                    + " PH.OTHERS_AMOUNT, "
                    + " PH.WORKER_AMOUNT, "
                    + " PH.CONVERSION_RATE, "
                    + " PH.BANK_NAME, "
                    + " PH.CHEQUE_REFERENCE, "
                    + " PH.CHEQUE_DATE, "
                    + " PH.CURRENCY, "
                    + " PH.STATUS, "
                    + " SERV.SERVICE_ID, "
                    + " SERV.SERVICE_NAME, "
                    + " SERV.SERVICE_AMOUNT, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_history PH, "
                    + " service_info SERV, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PH.SERVICE_ID = SERV.SERVICE_ID "
                    + " AND "
                    + " PH.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PH.USER_ID = UI.USER_ID "
                    + " AND "
                    + " UI.USER_ID = '" + uid + "' ";
        } else {
            selectPaymentStatement = " SELECT "
                    + " PH.PAY_HIS_ID, "
                    + " PH.PAYMENT_MODE, "
                    + " PH.PAYMENT_DATE, "
                    + " PH.TUITION_FEE, "
                    + " PH.OTHERS_AMOUNT, "
                    + " PH.WORKER_AMOUNT, "
                    + " PH.CONVERSION_RATE, "
                    + " PH.BANK_NAME, "
                    + " PH.CHEQUE_REFERENCE, "
                    + " PH.CHEQUE_DATE, "
                    + " PH.CURRENCY, "
                    + " PH.STATUS, "
                    + " SERV.SERVICE_ID, "
                    + " SERV.SERVICE_NAME, "
                    + " SERV.SERVICE_AMOUNT, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_history PH, "
                    + " service_info SERV, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PH.SERVICE_ID = SERV.SERVICE_ID "
                    + " AND "
                    + " PH.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PH.USER_ID = UI.USER_ID ";
        }

        return selectPaymentStatement;
    }

    public static String selectSearchPaymentHistory(String fName, String fValue) {

        String selectPaymentStatement = "";
        if (fName.equals("applicantName")) {
            selectPaymentStatement = " SELECT "
                    + " PH.PAY_HIS_ID, "
                    + " PH.PAYMENT_MODE, "
                    + " PH.PAYMENT_DATE, "
                    + " PH.TUITION_FEE, "
                    + " PH.OTHERS_AMOUNT, "
                    + " PH.CONVERSION_RATE, "
                    + " PH.BANK_NAME, "
                    + " PH.CHEQUE_REFERENCE, "
                    + " PH.CHEQUE_DATE, "
                    + " PH.CURRENCY, "
                    + " PH.STATUS, "
                    + " SERV.SERVICE_ID, "
                    + " SERV.SERVICE_NAME, "
                    + " SERV.SERVICE_AMOUNT, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_history PH, "
                    + " service_info SERV, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PH.SERVICE_ID = SERV.SERVICE_ID "
                    + " AND "
                    + " PH.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PH.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_NAME = '" + fValue + "' ";
        } else if (fName.equals("agentName")) {
            selectPaymentStatement = " SELECT "
                    + " PH.PAY_HIS_ID, "
                    + " PH.PAYMENT_MODE, "
                    + " PH.PAYMENT_DATE, "
                    + " PH.TUITION_FEE, "
                    + " PH.OTHERS_AMOUNT, "
                    + " PH.CONVERSION_RATE, "
                    + " PH.BANK_NAME, "
                    + " PH.CHEQUE_REFERENCE, "
                    + " PH.CHEQUE_DATE, "
                    + " PH.CURRENCY, "
                    + " PH.STATUS, "
                    + " SERV.SERVICE_ID, "
                    + " SERV.SERVICE_NAME, "
                    + " SERV.SERVICE_AMOUNT, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_history PH, "
                    + " service_info SERV, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PH.SERVICE_ID = SERV.SERVICE_ID "
                    + " AND "
                    + " PH.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PH.USER_ID = UI.USER_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        }

        return selectPaymentStatement;
    }

    public static String selectAppServHistoryInfo(String type) {

        String selectPaymentStatement = "";
        if (type.equalsIgnoreCase("all")) {
            selectPaymentStatement = " SELECT "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " A.TUITION_BASIC, "
                    + " A.TUITION_AGREE, "
                    + " A.TSTATUS, "
                    + " B.OTHERS_BASIC, "
                    + " B.OTHERS_AGREE, "
                    + " B.OSTATUS "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " (SELECT APPLICANT_ID, STATUS AS TSTATUS, BASIC_AMOUNT AS TUITION_BASIC, AGREE_AMOUNT AS TUITION_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 1)A, "
                    + " (SELECT APPLICANT_ID, STATUS AS OSTATUS, BASIC_AMOUNT AS OTHERS_BASIC, AGREE_AMOUNT AS OTHERS_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 2)B "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = A.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = B.APPLICANT_ID ";
        } else {
            selectPaymentStatement = " SELECT "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " A.TUITION_BASIC, "
                    + " A.TUITION_AGREE, "
                    + " A.TSTATUS, "
                    + " B.OTHERS_BASIC, "
                    + " B.OTHERS_AGREE, "
                    + " B.OSTATUS "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " (SELECT APPLICANT_ID, STATUS AS TSTATUS, BASIC_AMOUNT AS TUITION_BASIC, AGREE_AMOUNT AS TUITION_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 1)A, "
                    + " (SELECT APPLICANT_ID, STATUS AS OSTATUS, BASIC_AMOUNT AS OTHERS_BASIC, AGREE_AMOUNT AS OTHERS_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 2)B "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = A.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = B.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = '" + type + "' ";
        }

        return selectPaymentStatement;
    }

    public static String selectSearchAppServHistoryInfo(String fName, String fValue) {

        String selectPaymentStatement = "";
        if (fName.equals("applicantName")) {
            selectPaymentStatement = " SELECT "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " A.TUITION_BASIC, "
                    + " A.TUITION_AGREE, "
                    + " A.TSTATUS, "
                    + " B.OTHERS_BASIC, "
                    + " B.OTHERS_AGREE, "
                    + " B.OSTATUS "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " (SELECT APPLICANT_ID, STATUS AS TSTATUS, BASIC_AMOUNT AS TUITION_BASIC, AGREE_AMOUNT AS TUITION_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 1)A, "
                    + " (SELECT APPLICANT_ID, STATUS AS OSTATUS, BASIC_AMOUNT AS OTHERS_BASIC, AGREE_AMOUNT AS OTHERS_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 2)B "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = A.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = B.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_NAME = '" + fValue + "' ";
        } else if (fName.equals("agentName")) {
            selectPaymentStatement = " SELECT "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME, "
                    + " A.TUITION_BASIC, "
                    + " A.TUITION_AGREE, "
                    + " A.TSTATUS, "
                    + " B.OTHERS_BASIC, "
                    + " B.OTHERS_AGREE, "
                    + " B.OSTATUS "
                    + " FROM "
                    + " applicant_info APP, "
                    + " user_info UI, "
                    + " (SELECT APPLICANT_ID, STATUS AS TSTATUS, BASIC_AMOUNT AS TUITION_BASIC, AGREE_AMOUNT AS TUITION_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 1)A, "
                    + " (SELECT APPLICANT_ID, STATUS AS OSTATUS, BASIC_AMOUNT AS OTHERS_BASIC, AGREE_AMOUNT AS OTHERS_AGREE "
                    + " FROM applicant_service WHERE SERVICE_ID = 2)B "
                    + " WHERE "
                    + " APP.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = A.APPLICANT_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = B.APPLICANT_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        }

        return selectPaymentStatement;
    }

    public static String selectDuePaymentHistory(Integer gid, String uid, String appid) {

        String selectPaymentStatement = "";
        if (gid == 2 && appid.equals("")) {
            selectPaymentStatement = " SELECT "
                    + " PAY.PAYMENT_ID, "
                    + " PAY.PAYMENT_MODE, "
                    + " PAY.PAYMENT_DATE, "
                    + " PAY.TUITION_FEE, "
                    + " PAY.OTHERS_AMOUNT, "
                    + " PAY.WORKER_AMOUNT, "
                    + " PAY.CONVERSION_RATE, "
                    + " PAY.BANK_NAME, "
                    + " PAY.CHEQUE_REFERENCE, "
                    + " PAY.CHEQUE_DATE, "
                    + " PAY.CURRENCY, "
                    + " PAY.STATUS, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_TYPE, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_info PAY, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PAY.USER_ID = UI.USER_ID "
                    + " AND "
                    + " UI.USER_ID = '" + uid + "' ";
        } else if (!appid.equals("")) {
            selectPaymentStatement = " SELECT "
                    + " PAY.PAYMENT_ID, "
                    + " PAY.PAYMENT_MODE, "
                    + " PAY.PAYMENT_DATE, "
                    + " PAY.TUITION_FEE, "
                    + " PAY.OTHERS_AMOUNT, "
                    + " PAY.WORKER_AMOUNT, "
                    + " PAY.CONVERSION_RATE, "
                    + " PAY.BANK_NAME, "
                    + " PAY.CHEQUE_REFERENCE, "
                    + " PAY.CHEQUE_DATE, "
                    + " PAY.CURRENCY, "
                    + " PAY.STATUS, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_TYPE, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_info PAY, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PAY.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_ID = '" + appid + "' ";
        } else {
            selectPaymentStatement = " SELECT "
                    + " PAY.PAYMENT_ID, "
                    + " PAY.PAYMENT_MODE, "
                    + " PAY.PAYMENT_DATE, "
                    + " PAY.TUITION_FEE, "
                    + " PAY.OTHERS_AMOUNT, "
                    + " PAY.WORKER_AMOUNT, "
                    + " PAY.CONVERSION_RATE, "
                    + " PAY.BANK_NAME, "
                    + " PAY.CHEQUE_REFERENCE, "
                    + " PAY.CHEQUE_DATE, "
                    + " PAY.CURRENCY, "
                    + " PAY.STATUS, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_TYPE, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_info PAY, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PAY.USER_ID = UI.USER_ID ";
        }

        return selectPaymentStatement;
    }

    public static String selectSearchDuePaymentHistory(String fName, String fValue) {

        String selectPaymentStatement = "";
        if (fName.equals("applicantName")) {
            selectPaymentStatement = " SELECT "
                    + " PAY.PAYMENT_ID, "
                    + " PAY.PAYMENT_MODE, "
                    + " PAY.PAYMENT_DATE, "
                    + " PAY.TUITION_FEE, "
                    + " PAY.OTHERS_AMOUNT, "
                    + " PAY.CONVERSION_RATE, "
                    + " PAY.BANK_NAME, "
                    + " PAY.CHEQUE_REFERENCE, "
                    + " PAY.CHEQUE_DATE, "
                    + " PAY.CURRENCY, "
                    + " PAY.STATUS, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_TYPE, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_info PAY, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PAY.USER_ID = UI.USER_ID "
                    + " AND "
                    + " APP.APPLICANT_NAME = '" + fValue + "' ";
        } else if (fName.equals("agentName")) {
            selectPaymentStatement = " SELECT "
                    + " PAY.PAYMENT_ID, "
                    + " PAY.PAYMENT_MODE, "
                    + " PAY.PAYMENT_DATE, "
                    + " PAY.TUITION_FEE, "
                    + " PAY.OTHERS_AMOUNT, "
                    + " PAY.CONVERSION_RATE, "
                    + " PAY.BANK_NAME, "
                    + " PAY.CHEQUE_REFERENCE, "
                    + " PAY.CHEQUE_DATE, "
                    + " PAY.CURRENCY, "
                    + " PAY.STATUS, "
                    + " APP.APPLICANT_ID, "
                    + " APP.APPLICANT_NAME, "
                    + " APP.APP_TYPE, "
                    + " UI.USER_ID, "
                    + " UI.USER_FULL_NAME "
                    + " FROM "
                    + " payment_info PAY, "
                    + " applicant_info APP, "
                    + " user_info UI "
                    + " WHERE "
                    + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                    + " AND "
                    + " PAY.USER_ID = UI.USER_ID "
                    + " AND "
                    + " UI.USER_FULL_NAME = '" + fValue + "' ";
        }

        return selectPaymentStatement;
    }

    public static String selectDue(String appid, String apptype) {

        String selectPaymentStatement = "";

        if (apptype.equals("1")) {
            selectPaymentStatement = " SELECT A.TUITION_AGREE, B.OTHERS_AGREE FROM "
                    + " (SELECT AGREE_AMOUNT AS TUITION_AGREE FROM applicant_service "
                    + " WHERE SERVICE_ID = 1 AND APPLICANT_ID = '" + appid + "' )A, "
                    + " (SELECT AGREE_AMOUNT AS OTHERS_AGREE FROM applicant_service "
                    + " WHERE SERVICE_ID = 2 AND APPLICANT_ID = '" + appid + "' )B ";
        } else if (apptype.equals("2")) {
            selectPaymentStatement = " SELECT C.WORKER_AGREE FROM "
                    + " (SELECT AGREE_AMOUNT AS WORKER_AGREE FROM applicant_service "
                    + " WHERE SERVICE_ID = 3 AND APPLICANT_ID = '" + appid + "' )C ";
        }

        return selectPaymentStatement;
    }

    public static String selectIssuesInfo(String appid, String agentid) {

        String selectIssuesStatement = "";

        selectIssuesStatement = " SELECT "
                + " ISI.ISSUE_ID, "
                + " ISI.ISSUE_TITLE, "
                + " ISI.ISSUE_DETAILS, "
                + " ISI.COMMENT_DETAILS, "
                + " ISI.ISSUE_STATUS, "
                + " ISI.COMMENT_STATUS, "
                + " ISI.ISSUE_INSERT_BY, "
                + " ISI.ISSUE_INSERT_DATE, "
                + " ISI.COMMENT_BY, "
                + " ISI.COMMENT_DATE, "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " UI.USER_ID, "
                + " UI.USER_FULL_NAME "
                + " FROM "
                + " issues_info ISI, "
                + " applicant_info APP, "
                + " user_info UI "
                + " WHERE "
                + " ISI.APP_ID = APP.APPLICANT_ID "
                + " AND "
                + " ISI.ISSUE_INSERT_BY = UI.USER_ID "
                + " AND "
                + " APP.APPLICANT_ID = '" + appid + "' ";

        return selectIssuesStatement;
    }

    public static String selectAppService(String appid) {

        String selectPaymentStatement = "";

        selectPaymentStatement = " SELECT "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " S.SERVICE_ID, "
                + " S.SERVICE_NAME "
                + " FROM "
                + " applicant_info APP, "
                + " applicant_service APPS, "
                + " service_info S "
                + " WHERE "
                + " APP.APPLICANT_ID = APPS.APPLICANT_ID "
                + " AND "
                + " APPS.SERVICE_ID = S.SERVICE_ID "
                + " AND "
                + " APP.APPLICANT_ID = '" + appid + "' ";

        return selectPaymentStatement;
    }

    public static String selectDuePaymentByApplicant(String servid, String appid) {

        String selectPaymentStatement = "";

        selectPaymentStatement = " SELECT "
                + " PAY.PAYMENT_ID, "
                + " PAY.PAYMENT_MODE, "
                + " PAY.PAYMENT_DATE, "
                + " PAY.TUITION_FEE, "
                + " PAY.OTHERS_AMOUNT, "
                + " PAY.CONVERSION_RATE, "
                + " PAY.BANK_NAME, "
                + " PAY.CHEQUE_REFERENCE, "
                + " PAY.CHEQUE_DATE, "
                + " PAY.CURRENCY, "
                + " PAY.STATUS, "
                + " APP.APPLICANT_ID, "
                + " APP.APPLICANT_NAME, "
                + " UI.USER_ID, "
                + " UI.USER_FULL_NAME "
                + " FROM "
                + " payment_info PAY, "
                + " applicant_info APP, "
                + " user_info UI "
                + " WHERE "
                + " PAY.APPLICANT_ID = APP.APPLICANT_ID "
                + " AND "
                + " PAY.USER_ID = UI.USER_ID "
                + " AND "
                + " APP.APPLICANT_ID = '" + appid + "' ";

        return selectPaymentStatement;
    }
}
